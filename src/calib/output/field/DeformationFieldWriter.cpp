///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DeformationFieldWriter.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

static void outputCellVolume(CAFloat& v, const Field& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = field.tessellation().cellVolume(field.localCells()[cellIndex]);
}

static void outputCellAspectRatio(CAFloat& v, const Field& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = field.tessellation().cellAspectRatio(field.localCells()[cellIndex]);
}

static void outputDeformationGradientComponent(int i, int j, CAFloat& v, const DeformationField& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = field.deformationGradients()[cellIndex](i,j);
}

static void outputPlasticDeformationGradientComponent(int i, int j, CAFloat& v, const DeformationField& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = field.plasticDeformationGradients()[cellIndex](i,j);
}

static void outputHasPlasticDeformationGradient(int& v, const DeformationField& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = (field.plasticDeformationGradients()[cellIndex] != Matrix3::Zero());
}

static void outputPlasticShearStrain(CAFloat& v, const DeformationField& field, int cellIndex, const FieldWriter& fieldWriter) {
	const Matrix3& F = field.slipDeformationGradients()[cellIndex];
	Matrix3 strain = (F.transpose() * F - Matrix3::Identity()) * 0.5;
	v = sqrt(square(strain(0,1)) + square(strain(1,2)) + square(strain(0,2)) +
			(square(strain(1,1) - strain(2,2)) + square(strain(0,0) - strain(2,2)) + square(strain(0,0) - strain(1,1))) / 6.0);
}

/******************************************************************************
* Outputs the given components of the deformation field to a VTK file.
******************************************************************************/
void DeformationFieldWriter::writeVTKFile(ostream& stream, const DeformationField& field, const std::bitset<NUM_COMPONENTS>& components)
{
	// Write tessellation vertices and cell information to output file.
	FieldWriter::writeVTKFile(stream, field, "Deformation field");

	if(components.test(CELL_VOLUME))
		writeVTKCellFloat(stream, "volume", field, &outputCellVolume);
	if(components.test(CELL_ASPECT_RATIO))
		writeVTKCellFloat(stream, "aspect_ratio", field, &outputCellAspectRatio);

	// Write total deformation gradient field.
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			if(components.test(F_11 + i*3 + j)) {
				ostringstream fieldName;
				fieldName << "F_" << (i+1) << (j+1);
				writeVTKCellFloat(stream, fieldName.str(), field, boost::bind(&outputDeformationGradientComponent, i, j, _1, _2, _3, _4));
			}
		}
	}

	if(components.test(CELL_HAS_PLASTIC_F))
		writeVTKCellInt(stream, "has_F_plastic", field, &outputHasPlasticDeformationGradient);

	// Write plastic deformation gradient field.
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			if(components.test(F_plastic_11 + i*3 + j)) {
				ostringstream fieldName;
				fieldName << "F_plastic_" << (i+1) << (j+1);
				writeVTKCellFloat(stream, fieldName.str(), field, boost::bind(&outputPlasticDeformationGradientComponent, i, j, _1, _2, _3, _4));
			}
		}
	}

	if(components.test(PLASTIC_SHEAR_STRAIN))
		writeVTKCellFloat(stream, "plastic_shear", field, &outputPlasticShearStrain);
}


}; // End of namespace
