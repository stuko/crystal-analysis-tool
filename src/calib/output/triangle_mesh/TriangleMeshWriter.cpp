///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "TriangleMeshWriter.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Outputs a triangle mesh to a VTK file.
******************************************************************************/
void TriangleMeshWriter::writeVTKFile(ostream& stream, const TriangleMesh& mesh)
{
	if(!parallel().isMaster())
		return;

	stream << "# vtk DataFile Version 3.0\n";
	stream << "# Triangle mesh (CALib version " << CA_LIB_VERSION_STRING << ")\n";
	stream << "ASCII\n";
	stream << "DATASET UNSTRUCTURED_GRID\n";
	stream << "POINTS " << mesh.vertices().size() << " double\n";
	BOOST_FOREACH(TriangleMeshVertex* v, mesh.vertices())
		stream << v->pos.x() << " " << v->pos.y() << " " << v->pos.z() << "\n";
	stream << "\nCELLS " << mesh.facets().size() << " " << (mesh.facets().size()*4) << "\n";
	BOOST_FOREACH(TriangleMeshFacet* f, mesh.facets()) {
		stream << "3";
		for(int i = 0; i < 3; i++)
			stream << " " << f->edges[i]->vertex2->index;
		stream << "\n";
	}
	stream << "\nCELL_TYPES " << mesh.facets().size() << "\n";
	for(size_t i = 0; i < mesh.facets().size(); i++)
		stream << "5\n";	// Triangle

	stream << "POINT_DATA " << mesh.vertices().size() << "\n";
	stream << "NORMALS point_normals double\n";
	BOOST_FOREACH(TriangleMeshVertex* v, mesh.vertices())
		stream << v->normal.x() << " " << v->normal.y() << " " << v->normal.z() << "\n";
}

}; // End of namespace
