///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructureParser.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"
#include "../../util/Timer.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Reads the atomic coordinates from the input file in ddcMD format.
******************************************************************************/
void AtomicStructureParser::readDDCMDAtomsFile(TextParserStream& stream)
{
	context().msgLogger() << "Parsing ddcMD file." << endl;
	Timer loadTimer;

	Vector3 cellVectors[3];
	int columnPosx = -1;
	int columnPosy = -1;
	int columnPosz = -1;
	int columnId = -1;
	int columnType = -1;
	int numberOfColumns = 0;
	int timestep = 0;
	boost::array<bool,3> pbc;
	pbc.assign(false);
	AffineTransformation simulationCell(AffineTransformation::Identity());

	if(parallel().isMaster()) {

		size_t pos;
		if(stream.line().compare(0, 19, "particle FILEHEADER") != 0)
			context().raiseErrorOne("Invalid header in ddcMD file (line %i).", stream.lineNumber());
		stream.readline();
		stream.readline();
		if(stream.line().compare(0, 5, "loop=") != 0)
			context().raiseErrorOne("Invalid header in ddcMD file (line %i).", stream.lineNumber());
		if(sscanf(stream.line().c_str() + 5, "%i", &timestep) != 1)
			context().raiseErrorOne("File parsing error. Invalid timestep number (line %d): %s", stream.lineNumber(), stream.line().c_str());
		stream.readline();
		if((pos = stream.line().find("nfiles=")) == string::npos)
			context().raiseErrorOne("Invalid header in ddcMD file (line %i). Missing files entry.", stream.lineNumber());
		int nfiles;
		if(sscanf(stream.line().c_str() + pos + 7, "%i", &nfiles) != 1)
			context().raiseErrorOne("File parsing error. Invalid file count (line %d): %s", stream.lineNumber(), stream.line().c_str());
		if((pos = stream.line().find("nrecord=")) == string::npos)
			context().raiseErrorOne("Invalid header in ddcMD file (line %i). Missing nrecord entry.", stream.lineNumber());
		unsigned long int nrecord;
		if(sscanf(stream.line().c_str() + pos + 8, "%lu", &nrecord) != 1)
			context().raiseErrorOne("File parsing error. Invalid record count (line %d): %s", stream.lineNumber(), stream.line().c_str());
		_numInputAtoms = (AtomInteger)nrecord;
		if((pos = stream.line().find("nfields=")) == string::npos)
			context().raiseErrorOne("Invalid header in ddcMD file (line %i). Missing field count entry.", stream.lineNumber());
		int nfields;
		if(sscanf(stream.line().c_str() + pos + 8, "%i", &nfields) != 1 || nfields < 3)
			context().raiseErrorOne("File parsing error. Invalid field count (line %d): %s", stream.lineNumber(), stream.line().c_str());
		stream.readline();
		if(stream.line().compare(0, 12, "field_names=") != 0)
			context().raiseErrorOne("Invalid header in ddcMD file (line %i). Expected field_names entry.", stream.lineNumber());

		// Parse column names.
		string::const_iterator tokenBegin = stream.line().begin() + 12;
		while(tokenBegin != stream.line().end()) {
			while(tokenBegin != stream.line().end() && (*tokenBegin == ' ' || *tokenBegin == '\t' || *tokenBegin == ';')) ++tokenBegin;
			if(tokenBegin == stream.line().end()) break;
			string::const_iterator tokenEnd = tokenBegin + 1;
			while(tokenEnd != stream.line().end() && *tokenEnd != ' ' && *tokenEnd != '\t' && *tokenEnd != ';') ++tokenEnd;
			string columnName(tokenBegin, tokenEnd);
			if(columnName == "rx") columnPosx = numberOfColumns;
			else if(columnName == "ry") columnPosy = numberOfColumns;
			else if(columnName == "rz") columnPosz = numberOfColumns;
			else if(columnName == "id") columnId = numberOfColumns;
			else if(columnName == "type") columnType = numberOfColumns;
			tokenBegin = tokenEnd;
			numberOfColumns++;
		}
		if(columnPosx == -1) context().raiseErrorOne("File parsing error. Input file does not contain X coordinate column.");
		if(columnPosy == -1) context().raiseErrorOne("File parsing error. Input file does not contain Y coordinate column.");
		if(columnPosz == -1) context().raiseErrorOne("File parsing error. Input file does not contain Z coordinate column.");

		stream.readline();
		if(stream.readline().compare(0, 2, "h=") != 0)
			context().raiseErrorOne("Invalid header in ddcMD file (line %i). Expected cell matrix.", stream.lineNumber());

		Vector3 cellVectors[3];
		if(sscanf(stream.line().c_str()+2, CAFLOAT_SCANF_STRING_3, &cellVectors[0].x(), &cellVectors[1].x(), &cellVectors[2].x()) != 3)
			context().raiseErrorOne("Invalid simulation cell matrix in line %i of ddcMD file: %s", stream.lineNumber(), stream.line().c_str());
		if(sscanf(stream.readline().c_str(), CAFLOAT_SCANF_STRING_3, &cellVectors[0].y(), &cellVectors[1].y(), &cellVectors[2].y()) != 3)
			context().raiseErrorOne("Invalid simulation cell matrix in line %i of ddcMD file: %s", stream.lineNumber(), stream.line().c_str());
		if(sscanf(stream.readline().c_str(), CAFLOAT_SCANF_STRING_3, &cellVectors[0].z(), &cellVectors[1].z(), &cellVectors[2].z()) != 3)
			context().raiseErrorOne("Invalid simulation cell matrix in line %i of ddcMD file: %s", stream.lineNumber(), stream.line().c_str());
		simulationCell.linear().col(0) = cellVectors[0];
		simulationCell.linear().col(1) = cellVectors[1];
		simulationCell.linear().col(2) = cellVectors[2];

		// Skip to end of header.
		while(stream.readline().compare(0, 1, "}") != 0 && stream.eof() == false);

		// Skip to first atom line.
		while(stream.readline().find_first_not_of(" \t") == string::npos && stream.eof() == false);
	}

	parallel().broadcast(_numInputAtoms);
	parallel().broadcast(timestep);
	_structure.setTimestep(timestep);

	// Setup simulation cell geometry.
	setupSimulationCell(simulationCell, pbc);

	context().msgLogger() << "Reading " << _numInputAtoms << " atoms (frame " << timestep << ") from input file." << endl;

	auto_ptr<TextParserStream> secondaryParserStream;
	TextParserStream* currentParserStream = &stream;
	ifstream currentInputStream;
	int currentInputFileIndex = 0;

	beginDistributeAtoms();

	for(AtomInteger a = 0; a < _numInputAtoms; a++) {
		AtomInfo atomInfo;
		if(parallel().isMaster()) {
			if(currentParserStream->eof()) context().raiseErrorOne("Unexpected end of file at line %i.", stream.lineNumber());
			for(;;) {
				if(a != 0) currentParserStream->readline();
				if(currentParserStream->line().find_first_not_of(" \t") == string::npos) {
					currentInputStream.close();

					size_t pos = stream.filename().rfind('#');
					if(pos == string::npos) context().raiseErrorOne("Unexpected end of file at line %i of file %s. There seem to be no more input files.", currentParserStream->lineNumber(), currentParserStream->filename().c_str());
					currentInputFileIndex++;
					ostringstream secondaryFilenameS;
					secondaryFilenameS.fill('0');
					secondaryFilenameS.width(stream.filename().size() - pos - 1);
					secondaryFilenameS << right << currentInputFileIndex;
					string secondaryFilename = stream.filename().substr(0, pos + 1) + secondaryFilenameS.str();
					context().msgLogger() << "Opening secondary input file " << secondaryFilename << " (natoms=" << _numAtomsRead << ")" << endl;

					currentInputStream.open(secondaryFilename.c_str());
					if(!currentInputStream.is_open())
						context().raiseErrorOne("Failed to open input file for reading after %i atoms. Filename was '%s'.", _numAtomsRead, secondaryFilename.c_str());
					secondaryParserStream.reset(new TextParserStream(currentInputStream, secondaryFilename));
					currentParserStream = secondaryParserStream.get();
				}
				else break;
			}
			atomInfo.tag = a + 1;
			atomInfo.species = 1;
			const char* tokenBegin = currentParserStream->line().c_str();
			int columnIndex = 0;
			while(*tokenBegin != '\0') {
				while(*tokenBegin != '\0' && (*tokenBegin == ' ' || *tokenBegin == '\t')) ++tokenBegin;
				if(*tokenBegin == '\0') break;
				const char* tokenEnd = tokenBegin + 1;
				while(*tokenEnd != '\0' && *tokenEnd != ' ' && *tokenEnd != '\t') ++tokenEnd;
				if(columnPosx == columnIndex) {
					if(sscanf(tokenBegin, CAFLOAT_SCANF_STRING_1, &atomInfo.pos.x()) != 1)
						context().raiseErrorOne("File parsing error. Invalid X coordinate (line %d, column %d): %s", currentParserStream->lineNumber(), columnIndex, currentParserStream->line().c_str());
				}
				else if(columnPosy == columnIndex) {
					if(sscanf(tokenBegin, CAFLOAT_SCANF_STRING_1, &atomInfo.pos.y()) != 1)
						context().raiseErrorOne("File parsing error. Invalid Y coordinate (line %d, column %d): %s", currentParserStream->lineNumber(), columnIndex, currentParserStream->line().c_str());
				}
				else if(columnPosz == columnIndex) {
					if(sscanf(tokenBegin, CAFLOAT_SCANF_STRING_1, &atomInfo.pos.z()) != 1)
						context().raiseErrorOne("File parsing error. Invalid Z coordinate (line %d, column %d): %s", currentParserStream->lineNumber(), columnIndex, currentParserStream->line().c_str());
				}
				else if(columnId == columnType) {
					if(sscanf(tokenBegin, "%i", &atomInfo.species) != 1)
						context().raiseErrorOne("File parsing error. Invalid atom type (line %d, column %d): %s", currentParserStream->lineNumber(), columnIndex, currentParserStream->line().c_str());
				}
				else if(columnId == columnIndex) {
					long int t;
					if(sscanf(tokenBegin, "%li", &t) != 1 || t < 0)
						context().raiseErrorOne("File parsing error. Invalid atom tag (line %d, column %d): %s", currentParserStream->lineNumber(), columnIndex, currentParserStream->line().c_str());
					atomInfo.tag = (AtomInteger)t + 1;
				}
				tokenBegin = tokenEnd;
				columnIndex++;
			}
			if(columnIndex < numberOfColumns)
				context().raiseErrorOne("File parsing error. Unexpected end of line (line %i). Expected %i columns, but found only %i.", currentParserStream->lineNumber(), numberOfColumns, columnIndex);
		}
		distributeAtom(atomInfo);
	}

	endDistributeAtoms();

	context().msgLogger() << "Load time: " << loadTimer.elapsedTime() << " sec." << endl;
}

}; // End of namespace
