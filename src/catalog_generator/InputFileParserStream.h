///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __INPUT_FILE_PARSER_STREAM_H
#define __INPUT_FILE_PARSER_STREAM_H

#include <calib/input/TextParserStream.h>
#include <calib/context/CAContext.h>
#include <boost/tokenizer.hpp>

using namespace CALib;
using namespace std;

/******************************************************************************
* This class is used to read text lines from the input file.
* Comments and empty lines are automatically skipped.
******************************************************************************/
class InputFileParserStream : public TextParserStream
{
public:

	/// Constructor.
	InputFileParserStream(istream& stream, const string& filename = std::string()) :
		TextParserStream(stream, filename) {}

	/// Reads the next non-empty line.
	/// Returns an empty string if end of file is reached.
	const string& readline() {
		for(;;) {
			if(eof()) {
				_line.clear();
				return _line;
			}
			TextParserStream::readline();

			// Skip preceding whitespace.
			size_t startIndex = _line.find_first_not_of(" \t\n");
			if(startIndex == string::npos) continue;	// Skip empty lines

			size_t endIndex = _line.find_first_of("\n#");
			if(endIndex == startIndex) continue;		// Skip comment lines without content.

			if(endIndex == string::npos) endIndex = _line.size();

			// Extract line contents.
			_line = _line.substr(startIndex, endIndex - startIndex);
			return _line;
		}
	}

	/// Read a line from the input file and splits it into tokens.
	size_t readtokens(vector<string>& tokenList) {
		boost::tokenizer< boost::escaped_list_separator<char> > tokenizer(readline(), boost::escaped_list_separator<char>("\\", " \t", "\""));
		for(boost::tokenizer< boost::escaped_list_separator<char> >::iterator token = tokenizer.begin(); token != tokenizer.end(); ++token){
			if(token->empty()) continue;
			tokenList.push_back(*token);
		}
		return tokenList.size();
	}

	/// Reports a syntax error in the current line.
	void raiseSyntaxError(CAContext& context, const char* msg) {
		context.raiseErrorAll("Syntax error in line %i of file '%s': %s", lineNumber(), filename().c_str(), msg);
	}
};

#endif // __INPUT_FILE_PARSER_STREAM_H

