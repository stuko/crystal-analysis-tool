///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2010, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "TriangleMesh.h"
#include "../atomic_structure/SimulationCell.h"
#include "../context/CAContext.h"

#include "polytess/glu.h"

using namespace std;

namespace CALib {

static const Vector3 cubeCorners[8] = {
		Vector3(0,0,0),
		Vector3(0,0,1),
		Vector3(0,1,0),
		Vector3(0,1,1),
		Vector3(1,0,0),
		Vector3(1,0,1),
		Vector3(1,1,0),
		Vector3(1,1,1),
};

/******************************************************************************
* Splits up facets which are wrapped at the periodic boundaries.
******************************************************************************/
void TriangleMesh::wrapMesh(const SimulationCell& cell, TriangleMesh* capMesh)
{
	if(cell.hasPeriodicBoundaries() == false) return;

	// Convert all positions to reduced coordinates.
	BOOST_FOREACH(TriangleMeshVertex* vertex, vertices())
		vertex->pos = cell.absoluteToReducedPoint(vertex->pos);

	bool isInside[8];
	if(capMesh) {
		// Determine which corners of the simulation cell are inside the defect region.
		// Obey periodic boundary conditions.
		int cornerIndex = 0;
		if(!cell.pbc(0) || !cell.pbc(1) || !cell.pbc(2)) {
			for(cornerIndex = 0; cornerIndex < 8; cornerIndex++)
				isInside[cornerIndex] = true;
		}
		else {
			for(int x = 0; x <= 1; x++) {
				for(int y = 0; y <= 1; y++) {
					for(int z = 0; z <= 1; z++) {
						int copyFrom = cornerIndex;
						if(cell.pbc(0)) copyFrom &= ~4;
						if(cell.pbc(1)) copyFrom &= ~2;
						if(cell.pbc(2)) copyFrom &= ~1;
						CALIB_ASSERT(copyFrom <= cornerIndex);
						if(copyFrom == cornerIndex)
							isInside[cornerIndex] = pointInPolyhedron(cubeCorners[cornerIndex], cell);
						else
							isInside[cornerIndex] = isInside[copyFrom];
						cornerIndex++;
					}
				}
			}
		}
	}

	// Create corner vertices.
	TriangleMeshVertex* cornerVertices[8];
	for(int c = 0; c < 8; c++) {
		if(capMesh == NULL || isInside[c]) {
			cornerVertices[c] = createVertex(cubeCorners[c]);
			cornerVertices[c]->setClipVertex(0);
			cornerVertices[c]->setClipVertex(1);
			cornerVertices[c]->setClipVertex(2);
			cornerVertices[c]->setCornerVertex();
		}
		else {
			cornerVertices[c] = NULL;
		}
	}

	for(int dim = 0; dim < 3; dim++) {
		if(cell.pbc(dim) == false) continue;

		// Make sure all vertices are located inside the periodic box.
		BOOST_FOREACH(TriangleMeshVertex* vertex, vertices()) {
			CAFloat& p = vertex->pos[dim];
			while(p < 0.0) p += 1.0;
			while(p > 1.0) p -= 1.0;
			CALIB_ASSERT(p >= 0.0 && p <= 1.0);
		}

		// Clip edges.
		size_t oldVertexCount = vertices().size();
		for(size_t i = 0; i < oldVertexCount; i++) {
			TriangleMeshEdge* edge = vertices()[i]->edges;
			while(edge) {
				// Split edge if it crosses a periodic boundary.
				splitEdge(edge, cell, dim);
				edge = edge->nextEdge;
			}
		}

#ifdef DEBUG_CRYSTAL_ANALYSIS
		// Check topology of created facets and edges.
		for(int i = 0; i < facets().size(); i++) {
			for(int v = 0; v < 3; v++) {
				TriangleMeshEdge* edge1 = facets()[i]->edges[v];
				TriangleMeshEdge* edge2 = facets()[i]->edges[(v+1)%3];
				CALIB_ASSERT(edge1->facet == facets()[i]);
				CALIB_ASSERT(edge2->facet == facets()[i]);
				TriangleMeshEdge* edge = edge1->vertex2->edges;
				while(edge != NULL) {
					if(edge == edge2) break;
					edge = edge->nextEdge;
				}
				CALIB_ASSERT(edge == edge2);
				CALIB_ASSERT(fabs(edge1->vertex2->pos[dim] - edge2->vertex2->pos[dim]) < 0.5);
				CALIB_ASSERT(edge2->oppositeEdge->vertex2 == edge1->vertex2);
			}
		}
#endif
	}

	// Create the cap facets to close holes at periodic boundaries.
	if(capMesh != NULL)
		createCaps(cell, *capMesh, cornerVertices);

	// Convert all vertex positions back to absolute coordinates.
	BOOST_FOREACH(TriangleMeshVertex* vertex, vertices())
		vertex->pos = cell.reducedToAbsolutePoint(vertex->pos);
}

/******************************************************************************
* Splits an edge to clip it at the periodic boundaries of the simulation cell.
******************************************************************************/
void TriangleMesh::splitEdge(TriangleMeshEdge* edge, const SimulationCell& cell, int dim)
{
	if(edge->vertex2->pos[dim] > edge->vertex1()->pos[dim])
		edge = edge->oppositeEdge;

	TriangleMeshVertex* vertex1 = edge->vertex1();
	TriangleMeshVertex* vertex2 = edge->vertex2;
	CAFloat rv1 = vertex1->pos[dim];
	CAFloat delta = vertex2->pos[dim] - rv1;
	CALIB_ASSERT(delta <= 0.0);

	// Use minimum image convention to determine if edge crosses periodic boundary.
	if(fabs(delta) <= 0.5) return;

	delta += 1.0;
	CALIB_ASSERT(delta >= 0.0 && delta <= 0.5);

	CAFloat t;
	if(delta != 0)
		t = (1.0 - rv1) / delta;
	else
		t = 0.5;

	clamp(t, 0.0, 1.0);
	Vector3 reducedDelta = cell.wrapReducedVector(vertex2->pos - vertex1->pos);
	TriangleMeshVertex* intersectionPoint1 = createVertex(vertex1->pos + t * reducedDelta);
	TriangleMeshVertex* intersectionPoint2 = createVertex(vertex2->pos - (1.0 - t) * reducedDelta);
	intersectionPoint1->pos[dim] = 1.0;
	intersectionPoint2->pos[dim] = 0.0;
	for(int d = 0; d < dim; d++) {
		if(vertex1->isClipVertex(d) && vertex2->isClipVertex(d)) {
			CALIB_ASSERT(vertex1->pos[d] == vertex2->pos[d]);
			CALIB_ASSERT(intersectionPoint1->pos[d] == intersectionPoint2->pos[d]);
			intersectionPoint1->setClipVertex(d);
			intersectionPoint2->setClipVertex(d);
		}
	}
	intersectionPoint1->setClipVertex(dim);
	intersectionPoint2->setClipVertex(dim);

	intersectionPoint1->normal = t * vertex2->normal + (1.0 - t) * vertex1->normal;
	if(intersectionPoint1->normal != Vector3::Zero()) intersectionPoint1->normal.normalize();
	intersectionPoint2->normal = intersectionPoint1->normal;

	TriangleMeshEdge* edge1 = edge;
	TriangleMeshEdge* edge1op = _edgePool.construct();
	TriangleMeshEdge* edge2 = _edgePool.construct();
	TriangleMeshEdge* edge2op = edge->oppositeEdge;

	edge1->oppositeEdge = edge1op;
	edge1op->oppositeEdge = edge1;
	edge2->oppositeEdge = edge2op;
	edge2op->oppositeEdge = edge2;

	edge1->vertex2 = intersectionPoint1;
	edge2->vertex2 = vertex2;
	edge1op->vertex2 = vertex1;
	edge2op->vertex2 = intersectionPoint2;

	intersectionPoint1->edges = edge1op;
	edge1op->nextEdge = NULL;
	edge1op->facet = NULL;

	intersectionPoint2->edges = edge2;
	edge2->nextEdge = NULL;
	edge2->facet = NULL;

	CALIB_ASSERT(fabs(vertex1->pos[dim] - intersectionPoint1->pos[dim]) <= 0.5);
	CALIB_ASSERT(fabs(vertex2->pos[dim] - intersectionPoint2->pos[dim]) <= 0.5);

	if(edge1->facet)
		splitFacet(edge1->facet, edge1, edge2, intersectionPoint1, intersectionPoint2, dim);
	if(edge2->oppositeEdge->facet)
		splitFacet(edge2->oppositeEdge->facet, edge2op, edge1op, intersectionPoint2, intersectionPoint1, dim);
}

/******************************************************************************
* Helper routine used by TriangleMesh::splitEdge().
******************************************************************************/
void TriangleMesh::splitFacet(TriangleMeshFacet* facet1, TriangleMeshEdge* edge1, TriangleMeshEdge* edge2, TriangleMeshVertex* intersectionPoint1, TriangleMeshVertex* intersectionPoint2, int dim)
{
	CALIB_ASSERT(facet1 != NULL);
	TriangleMeshEdge* firstEdge = facet1->edges[(facet1->edgeIndex(edge1) + 2) % 3];
	TriangleMeshEdge* thirdEdge = facet1->edges[(facet1->edgeIndex(edge1) + 1) % 3];
	TriangleMeshVertex* thirdVertex1 = firstEdge->oppositeEdge->vertex2;
	TriangleMeshVertex* thirdVertex2 = thirdEdge->vertex2;
	CALIB_ASSERT(thirdEdge->oppositeEdge->vertex2 == edge2->vertex2);
	CALIB_ASSERT(thirdVertex1->isClipVertex(dim) == thirdVertex2->isClipVertex(dim));

	TriangleMeshEdge* splitEdge = _edgePool.construct();
	TriangleMeshEdge* splitEdgeOpp = _edgePool.construct();

	if(thirdVertex1->isClipVertex(dim) == false) {
		CALIB_ASSERT(thirdVertex1 == thirdVertex2);
		splitEdge->oppositeEdge = splitEdgeOpp;
		splitEdgeOpp->oppositeEdge = splitEdge;
	}
	else {
		CALIB_ASSERT(thirdVertex1 != thirdVertex2);
		TriangleMeshEdge* capEdge = _edgePool.construct();
		TriangleMeshEdge* capEdgeOpp = _edgePool.construct();
		capEdge->facet = NULL;
		capEdgeOpp->facet = NULL;
		capEdge->vertex2 = thirdVertex2;
		capEdgeOpp->vertex2 = intersectionPoint1;
		capEdge->nextEdge = intersectionPoint2->edges;
		intersectionPoint2->edges = capEdge;
		capEdgeOpp->nextEdge = thirdVertex1->edges;
		thirdVertex1->edges = capEdgeOpp;
		CALIB_ASSERT(fabs(thirdVertex1->pos[dim] - intersectionPoint1->pos[dim]) <= 0.5);
		CALIB_ASSERT(fabs(thirdVertex2->pos[dim] - intersectionPoint2->pos[dim]) <= 0.5);
		splitEdge->oppositeEdge = capEdgeOpp;
		capEdgeOpp->oppositeEdge = splitEdge;
		splitEdgeOpp->oppositeEdge = capEdge;
		capEdge->oppositeEdge = splitEdgeOpp;
	}

	splitEdge->vertex2 = thirdVertex1;

	splitEdgeOpp->nextEdge = thirdVertex2->edges;
	thirdVertex2->edges = splitEdgeOpp;

	if(fabs(thirdVertex1->pos[dim] - intersectionPoint1->pos[dim]) <= 0.5) {
		splitEdge->nextEdge = intersectionPoint1->edges;
		intersectionPoint1->edges = splitEdge;
	}
	else {
		CALIB_ASSERT(fabs(thirdVertex1->pos[dim] - intersectionPoint2->pos[dim]) <= 0.5);
		splitEdge->nextEdge = intersectionPoint2->edges;
		intersectionPoint2->edges = splitEdge;
	}

	if(fabs(thirdVertex2->pos[dim] - intersectionPoint1->pos[dim]) <= 0.5) {
		splitEdgeOpp->vertex2 = intersectionPoint1;
	}
	else {
		CALIB_ASSERT(fabs(thirdVertex2->pos[dim] - intersectionPoint2->pos[dim]) <= 0.5);
		splitEdgeOpp->vertex2 = intersectionPoint2;
	}

	TriangleMeshFacet* facet2 = _facetPool.construct();
	_facets.push_back(facet2);
	facet2->edges[0] = edge2;
	facet2->edges[1] = thirdEdge;
	facet2->edges[2] = splitEdgeOpp;
	edge2->facet = facet2;
	thirdEdge->facet = facet2;
	splitEdgeOpp->facet = facet2;
	facet1->edges[facet1->edgeIndex(thirdEdge)] = splitEdge;
	splitEdge->facet = facet1;

#ifdef DEBUG_CRYSTAL_ANALYSIS
	if(thirdVertex1->isClipVertex(dim)) {
		CALIB_ASSERT(fabs(splitEdgeOpp->vertex2->pos[dim] - edge2->vertex2->pos[dim]) <= 0.5);
		CALIB_ASSERT(fabs(edge2->vertex2->pos[dim] - thirdEdge->vertex2->pos[dim]) <= 0.5);
		CALIB_ASSERT(fabs(thirdEdge->vertex2->pos[dim] - splitEdgeOpp->vertex2->pos[dim]) <= 0.5);
		CALIB_ASSERT(splitEdgeOpp->oppositeEdge->vertex2 == thirdEdge->vertex2);
		CALIB_ASSERT(thirdEdge->oppositeEdge->vertex2 == edge2->vertex2);
		CALIB_ASSERT(edge2->oppositeEdge->vertex2 == splitEdgeOpp->vertex2);
	}
	else {
		if(fabs(thirdVertex1->pos[dim] - intersectionPoint1->pos[dim]) <= 0.5) {
			CALIB_ASSERT(fabs(thirdEdge->vertex2->pos[dim] - edge2->vertex2->pos[dim]) >= 0.5);
		}
		else {
			CALIB_ASSERT(fabs(thirdVertex1->pos[dim] - intersectionPoint2->pos[dim]) <= 0.5);
			CALIB_ASSERT(fabs(thirdEdge->vertex2->pos[dim] - edge1->oppositeEdge->vertex2->pos[dim]) >= 0.5);
		}
	}
#endif
}

class CapTessellator
{
public:
	CapTessellator(TriangleMesh& _mesh) : mesh(_mesh) {
		tess = gluNewTess();
#if defined(__APPLE__) and (__MAC_OS_X_VERSION_MAX_ALLOWED < 1050)		// If old OS X version (pre 10.5)
		gluTessCallback(tess, GLU_TESS_ERROR_DATA, (void (*)(...))errorData);
		gluTessCallback(tess, GLU_TESS_BEGIN_DATA, (void (*)(...))beginData);
		gluTessCallback(tess, GLU_TESS_END_DATA, (void (*)(...))endData);
		gluTessCallback(tess, GLU_TESS_VERTEX_DATA, (void (*)(...))vertexData);
		gluTessCallback(tess, GLU_TESS_COMBINE_DATA, (void (*)(...))combineData);
#else
		gluTessCallback(tess, GLU_TESS_ERROR_DATA, (void (*)())errorData);
		gluTessCallback(tess, GLU_TESS_BEGIN_DATA, (void (*)())beginData);
		gluTessCallback(tess, GLU_TESS_END_DATA, (void (*)())endData);
		gluTessCallback(tess, GLU_TESS_VERTEX_DATA, (void (*)())vertexData);
		gluTessCallback(tess, GLU_TESS_COMBINE_DATA, (void (*)())combineData);
#endif
	}

	~CapTessellator() {
		// Cleanup.
		gluDeleteTess(tess);
	}

	void writeToFile(ostream& stream, const SimulationCell& cell) {
		size_t numPoints = 0;
		for(vector< vector<Vector3> >::const_iterator n = contours.begin(); n != contours.end(); ++n)
			numPoints += n->size();

		stream << "# vtk DataFile Version 3.0" << endl;
		stream << "# Interface mesh" << endl;
		stream << "ASCII" << endl;
		stream << "DATASET UNSTRUCTURED_GRID" << endl;
		stream << "POINTS " << numPoints << " float" << endl;
		for(vector< vector<Vector3> >::const_iterator n = contours.begin(); n != contours.end(); ++n) {
			for(vector<Vector3>::const_iterator p = n->begin(); p != n->end(); ++p) {
				Vector3 wp = cell.simulationCellMatrix() * (*p);
				stream << wp.x() << " " << wp.y() << " " << wp.z() << endl;
			}
		}
		stream << endl << "CELLS " << contours.size() << " " << (contours.size()+numPoints) << endl;
		size_t counter = 0;
		for(vector< vector<Vector3> >::const_iterator n = contours.begin(); n != contours.end(); ++n) {
			stream << n->size();
			for(size_t i = 0; i < n->size(); i++)
				stream << " " << counter++;
			stream << endl;
		}

		stream << endl << "CELL_TYPES " << contours.size() << endl;
		for(size_t i = 0; i < contours.size(); i++)
			stream << "7" << endl;
	}


	void beginPolygon(const Vector3& facetNormal, const Vector3& planeNormal) {
		this->facetNormal = facetNormal;
		this->planeNormal = planeNormal;
		gluTessNormal(tess, -planeNormal.x(), -planeNormal.y(), -planeNormal.z());
		gluTessBeginPolygon(tess, this);
	}

	void endPolygon() {
		gluTessEndPolygon(tess);
	}

	void beginContour() { gluTessBeginContour(tess); contours.resize(contours.size()+1); }
	void endContour() { gluTessEndContour(tess); }

	void vertex(const Vector3& pos) {
		vertex(mesh.createVertex(pos, facetNormal));
	}

	void vertex(TriangleMeshVertex* outputVertex) {
		double vertexCoord[3];
		vertexCoord[0] = outputVertex->pos.x();
		vertexCoord[1] = outputVertex->pos.y();
		vertexCoord[2] = outputVertex->pos.z();
		gluTessVertex(tess, vertexCoord, outputVertex);

		contours.back().push_back(outputVertex->pos);
	}

	static void beginData(int type, void* polygon_data) {
		CapTessellator* tessellator = (CapTessellator*)polygon_data;
		tessellator->primitiveType = type;
		tessellator->vertices.clear();
	}

	static void endData(void* polygon_data) {
		CapTessellator* tessellator = (CapTessellator*)polygon_data;

		if(tessellator->primitiveType == GL_TRIANGLE_FAN) {
			CALIB_ASSERT(tessellator->vertices.size() >= 4);
			TriangleMeshVertex* facetVertices[3];
			facetVertices[0] = tessellator->vertices[0];
			facetVertices[1] = tessellator->vertices[1];
			for(vector<TriangleMeshVertex*>::iterator v = tessellator->vertices.begin() + 2; v != tessellator->vertices.end(); ++v) {
				facetVertices[2] = *v;
				tessellator->mesh.createFacetAndEdges(facetVertices);
				facetVertices[1] = facetVertices[2];
			}
		}
		else if(tessellator->primitiveType == GL_TRIANGLE_STRIP) {
			CALIB_ASSERT(tessellator->vertices.size() >= 3);
			TriangleMeshVertex* facetVertices[3];
			facetVertices[0] = tessellator->vertices[0];
			facetVertices[1] = tessellator->vertices[1];
			bool even = true;
			for(vector<TriangleMeshVertex*>::iterator v = tessellator->vertices.begin() + 2; v != tessellator->vertices.end(); ++v) {
				facetVertices[2] = *v;
				tessellator->mesh.createFacetAndEdges(facetVertices);
				if(even)
					facetVertices[0] = facetVertices[2];
				else
					facetVertices[1] = facetVertices[2];
				even = !even;
			}
		}
		else if(tessellator->primitiveType == GL_TRIANGLES) {
			for(vector<TriangleMeshVertex*>::iterator v = tessellator->vertices.begin(); v != tessellator->vertices.end(); v += 3) {
				tessellator->mesh.createFacetAndEdges(&*v);
			}
		}
	}

	static void vertexData(void* vertex_data, void* polygon_data) {
		CapTessellator* tessellator = (CapTessellator*)polygon_data;
		tessellator->vertices.push_back((TriangleMeshVertex*)vertex_data);
	}

	static void combineData(double coords[3], void* vertex_data[4], float weight[4], void** outDatab, void* polygon_data) {
		CapTessellator* tessellator = (CapTessellator*)polygon_data;
		TriangleMeshVertex* outputVertex = tessellator->mesh.createVertex(Vector3(coords[0], coords[1], coords[2]), tessellator->facetNormal);
		*outDatab = outputVertex;
	}

	static void errorData(int errno, void* polygon_data) {
		if(errno == GLU_TESS_NEED_COMBINE_CALLBACK)
			cerr << "ERROR: Could not tessellate cap polygon. It contains overlapping contours." << endl;
		else
			cerr << "ERROR: Could not tessellate cap polygon. Error code: " << errno << endl;
		CALIB_ASSERT(false);
	}

private:
	GLUtesselator* tess;
	TriangleMesh& mesh;
	int primitiveType;
	Vector3 facetNormal;
	Vector3 planeNormal;
	vector<TriangleMeshVertex*> vertices;

	vector< vector<Vector3> > contours;
};

/******************************************************************************
* Creates the cap facets to close holes at periodic boundaries.
******************************************************************************/
void TriangleMesh::createCaps(const SimulationCell& cell, TriangleMesh& capMesh, TriangleMeshVertex* cornerVertices[8])
{
	CapTessellator tessellator(capMesh);

	for(int dim1 = 0; dim1 < 3; dim1++) {
		int dim2, dim3;
		if(dim1 == 0) { dim2 = 2; dim3 = 1; }
		else if(dim1 == 1) { dim2 = 0; dim3 = 2; }
		else { dim2 = 1; dim3 = 0; }

		Vector3 planeNormal = Vector3::Unit(dim1);
		Vector3 facetNormal = cell.cellVector(dim2).cross(cell.cellVector(dim3)).normalized();

		size_t oldVertexCount = capMesh.vertices().size();
		size_t oldFacetCount = capMesh.facets().size();

		if(cell.pbcFlags()[dim1] == false) {
			TriangleMeshVertex* facetVertices[3];
			facetVertices[0] = capMesh.createVertex(Vector3::Zero(), facetNormal);
			facetVertices[1] = capMesh.createVertex(Vector3::Unit(dim2) + Vector3::Unit(dim3), facetNormal);
			facetVertices[2] = capMesh.createVertex(Vector3::Unit(dim3), facetNormal);
			capMesh.createFacetAndEdges(facetVertices);
			facetVertices[2] = facetVertices[1];
			facetVertices[1] = capMesh.createVertex(Vector3::Unit(dim2), facetNormal);
			capMesh.createFacetAndEdges(facetVertices);
		}
		else {
			// Find vertices on the edges of the simulation cell and sort them according to their position along the edge.
			multimap<CAFloat, TriangleMeshVertex*> corners[2][2];
			BOOST_FOREACH(TriangleMeshVertex* vertex, vertices()) {
				if(vertex->isClipVertex(dim1) && vertex->pos[dim1] == 0.0 && !vertex->isCornerVertex()) {
					vertex->setVisited();
					if(vertex->isClipVertex(dim2)) {
						CALIB_ASSERT(vertex->isClipVertex(dim3) == false);
						CALIB_ASSERT(vertex->pos[dim2] == 0.0 || vertex->pos[dim2] == 1.0);
						if(vertex->pos[dim2] == 0)
							corners[0][0].insert(make_pair(vertex->pos[dim3], vertex));
						else
							corners[0][1].insert(make_pair(vertex->pos[dim3], vertex));
					}
					else if(vertex->isClipVertex(dim3)) {
						CALIB_ASSERT(vertex->isClipVertex(dim2) == false);
						CALIB_ASSERT(vertex->pos[dim3] == 0.0 || vertex->pos[dim3] == 1.0);
						if(vertex->pos[dim3] == 0)
							corners[1][0].insert(make_pair(vertex->pos[dim2], vertex));
						else
							corners[1][1].insert(make_pair(vertex->pos[dim2], vertex));
					}
				}
			}

			TriangleMeshVertex* capCorners[4];
			if(dim1 == 0) {
				capCorners[0] = cornerVertices[2];
				capCorners[1] = cornerVertices[3];
				capCorners[2] = cornerVertices[1];
				capCorners[3] = cornerVertices[0];
			}
			else if(dim1 == 1) {
				capCorners[0] = cornerVertices[1];
				capCorners[1] = cornerVertices[5];
				capCorners[2] = cornerVertices[4];
				capCorners[3] = cornerVertices[0];
			}
			else {
				capCorners[0] = cornerVertices[4];
				capCorners[1] = cornerVertices[6];
				capCorners[2] = cornerVertices[2];
				capCorners[3] = cornerVertices[0];
			}
			for(int c = 0; c < 4; c++)
				if(capCorners[c]) capCorners[c]->setVisited();

			vector<TriangleMeshVertex*> borderVertices;
			for(multimap<CAFloat, TriangleMeshVertex*>::const_iterator iter = corners[0][0].begin(); iter != corners[0][0].end(); ++iter)
				borderVertices.push_back(iter->second);
			if(capCorners[0]) borderVertices.push_back(capCorners[0]);
			for(multimap<CAFloat, TriangleMeshVertex*>::const_iterator iter = corners[1][1].begin(); iter != corners[1][1].end(); ++iter)
				borderVertices.push_back(iter->second);
			if(capCorners[1]) borderVertices.push_back(capCorners[1]);
			for(multimap<CAFloat, TriangleMeshVertex*>::const_iterator iter = corners[0][1].end(); iter != corners[0][1].begin(); ) {
				--iter;
				borderVertices.push_back(iter->second);
			}
			if(capCorners[2]) borderVertices.push_back(capCorners[2]);
			for(multimap<CAFloat, TriangleMeshVertex*>::const_iterator iter = corners[1][0].end(); iter != corners[1][0].begin(); ) {
				--iter;
				borderVertices.push_back(iter->second);
			}
			if(capCorners[3]) borderVertices.push_back(capCorners[3]);

			// Generate contours.
			tessellator.beginPolygon(facetNormal, planeNormal);

			BOOST_FOREACH(TriangleMeshVertex* vertex, vertices()) {
				if(vertex->isClipVertex(dim1) && vertex->pos[dim1] == 0 && vertex->wasVisited() && !vertex->isCornerVertex()) {
					tessellator.beginContour();
					for(;;) {
						if(vertex->wasVisited() == false) break;
						tessellator.vertex(vertex->pos);
						vertex->clearVisited();

						TriangleMeshEdge* edge = vertex->edges;
						while(edge) {
							if(edge->facet != NULL && edge->vertex2->isClipVertex(dim1) && edge->vertex2->pos[dim1] == 0)
								break;
							edge = edge->nextEdge;
						}
						if(edge != NULL) {
							vertex = edge->vertex2;
						}
						else {
							vector<TriangleMeshVertex*>::const_iterator iter = find(borderVertices.begin(), borderVertices.end(), vertex);
							CALIB_ASSERT(iter != borderVertices.end());
							for(;;) {
								++iter;
								if(iter == borderVertices.end()) iter = borderVertices.begin();
								vertex = *iter;
								if(vertex->isCornerVertex()) {
									CALIB_ASSERT(vertex->wasVisited());
									tessellator.vertex(vertex->pos);
									vertex->clearVisited();
								}
								else break;
							}
						}
					}
					tessellator.endContour();
				}
			}

			if(capCorners[0] && capCorners[0]->wasVisited()) {
				CALIB_ASSERT(capCorners[1] && capCorners[2] && capCorners[3]);
				CALIB_ASSERT(capCorners[1]->wasVisited());
				CALIB_ASSERT(capCorners[2]->wasVisited());
				CALIB_ASSERT(capCorners[3]->wasVisited());
				tessellator.beginContour();
				for(int c = 0; c < 4; c++)
					tessellator.vertex(capCorners[c]->pos);
				tessellator.endContour();
			}
			tessellator.endPolygon();
		}

		// Copy facets to generate cap for opposite box side.
		size_t newVertexCount = capMesh.vertices().size();
		size_t newFacetCount = capMesh.facets().size();
		for(size_t i = oldVertexCount; i < newVertexCount; i++) {
			capMesh.createVertex(capMesh.vertices()[i]->pos + planeNormal, -facetNormal);
		}
		for(size_t i = oldFacetCount; i < newFacetCount; i++) {
			TriangleMeshVertex* facetVertices[3];
			facetVertices[2] = capMesh.vertices()[capMesh.facets()[i]->edges[2]->vertex2->index - oldVertexCount + newVertexCount];
			facetVertices[1] = capMesh.vertices()[capMesh.facets()[i]->edges[0]->vertex2->index - oldVertexCount + newVertexCount];
			facetVertices[0] = capMesh.vertices()[capMesh.facets()[i]->edges[1]->vertex2->index - oldVertexCount + newVertexCount];
			capMesh.createFacetAndEdges(facetVertices);
		}
	}

	// Convert vertex positions into absolute coordinates.
	BOOST_FOREACH(TriangleMeshVertex* vertex, capMesh.vertices())
		vertex->pos = cell.reducedToAbsolutePoint(vertex->pos);
}

}; // End of namespace
