///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_SUPER_PATTERN_H
#define __CA_SUPER_PATTERN_H

#include "../../CALib.h"

namespace CALib {

class CoordinationPattern;
struct SuperPattern;

/**
 * An edge between two nodes of a pattern graph.
 */
struct SuperPatternBond
{
	/// The neighbor node's index in the super pattern's list of nodes.
	int neighborNodeIndex;

	/// The vector from the central node to one of its neighbor node in the reference frame of the super pattern.
	/// This is an unrelaxed vector that defines the stress-free state.
	Vector3 referenceVector;

	/// The index of the reverse edge, which is found in the target node's list of edges.
	int reverseNodeNeighborIndex;

	/// The number of common neighbors of the two nodes connected by this edge.
	/// Can be up to two common neighbors and must be non-coplanar.
	int numCommonNeighbors;

	/// Lists two common neighbors of the origin and the destination node connected by this bond.
	/// The array contains the bond list indices for origin and the destination node for both common
	/// neighbors.
	int commonNeighborNodeIndices[CA_MAX_PATTERN_NEIGHBORS][2];
};

/**
 * Describes one element of the space symmetry group of a super pattern.
 */
struct SpaceGroupElement
{
	/// The node to which the current node is mapped to by this symmetry transformation.
	int targetNodeIndex;

	/// The permutation from the node's coordination pattern that describes the point symmetry transformation.
	int permutationIndex;

	/// The symmetry transformation matrix.
	Matrix3 tm;
};

/**
 * A node of a super pattern graph.
 */
struct SuperPatternNode
{
	/// The coordination pattern of the node.
	/// This also determines the number of neighbor bonds of the node.
	CoordinationPattern const* coordinationPattern;

	/// The array of bonds (edges) to the neighboring nodes.
	SuperPatternBond neighbors[CA_MAX_PATTERN_NEIGHBORS];

	/// If this is an overlapping node then this stores the super pattern
	/// it overlaps with.
	SuperPattern const* overlapPattern;

	/// If this is an overlapping node then this stores the node in the
	/// adjacent super pattern it overlaps with.
	int overlapPatternNode;

	/// Graph distances of this node to the other nodes in the super pattern.
	/// Contains one entry for each node in the super pattern.
	/// This is used to check the vicinity criterion during pattern matching.
	std::vector<int> nodeNodeDistances;

	/// The species label of the graph node.
	/// A special value of 0 indicates that atom type information is ignored during matching.
	int species;

	/// Lists of symmetry transformations of the pattern that make up the space group.
	std::vector<SpaceGroupElement> spaceGroupEntries;
};

/**
 * Describes a family of Burgers vectors in a certain lattice.
 */
struct BurgersVectorFamily
{
	/// The prototype Burgers vector of this family.
	/// The XYZ components must be positive and in increasing order.
	Vector3 burgersVector;

	/// The user-defined identifier of the family.
	int id;

	/// The user-defined name of the Burgers vector family.
	std::string name;

	/// The user-defined color of the Burgers vector family.
	Vector3 color;

	/// Checks if the given Burgers vector is a member of this family.
	bool isMember(const Vector3& v) const {
		// Take absolute value of components and sort them.
		Vector3 sc(fabs(v.x()), fabs(v.y()), fabs(v.z()));
		std::sort(sc.data(), sc.data() + 3);
		return (sc - burgersVector).isZero(CA_LATTICE_VECTOR_EPSILON);
	}
};

/**
 * A super pattern is a graph-based description of an atomic arrangement.
 */
struct SuperPattern
{
	/// Vicinity criteria for pattern graph matching.
	enum VicinityCriterion {
		VICINITY_CRITERION_LATTICE,
		VICINITY_CRITERION_INTERFACE,
		VICINITY_CRITERION_POINTDEFECT,
		VICINITY_CRITERION_SHORTESTPATH
	};

	/// The user-assigned name of the pattern.
	std::string name;

	/// The long, human-readable name of the pattern.
	std::string fullName;

	/// The index of this pattern in the list of super patterns in the PatternCatalog.
	int index;

	/// The number of nodes (=atoms) this pattern consists of.
	int numNodes;

	/// The number of nodes minus the overlapping nodes at the edges of the pattern graph.
	int numCoreNodes;

	/// The graph nodes of the pattern.
	std::vector<SuperPatternNode> nodes;

	/// The number of dimensions in which the pattern is periodic (0,1,2,3).
	int periodicity;

	/// The type of vicinity criterion to use during pattern matching.
	VicinityCriterion vicinityCriterion;

	/// This this a pattern describing a planar interface, then this is the
	/// misorientation matrix that transforms vectors from grain 1 to grain 2.
	Matrix3 misorientationMatrix;

	/// The color assigned to this pattern. It is used to color matching atoms in the visualization program.
	Vector3 color;

	/// If this is a pattern having overlap nodes, then this lists the surrounding structures.
	std::set<SuperPattern const*> parentLatticePatterns;

	/// List of Burgers vector families in this lattice type (defined by the user).
	std::vector<BurgersVectorFamily> burgersVectorFamilies;

	/// Contains the periodicity lengths of the pattern.
	Matrix3 periodicCell;

	/// Returns the ID of the Burgers vector family the given vector is a member of.
	/// Return 0 if the vector does not belong to a known family.
	int burgersVectorFamilyId(const Vector3& burgersVector) const {
		BOOST_FOREACH(const BurgersVectorFamily& family, burgersVectorFamilies) {
			if(family.isMember(burgersVector))
				return family.id;
		}
		return 0;
	}

	/// Returns whether this pattern describes a three-dimensional lattice structure.
	/// A lattice pattern consists only of core nodes and is periodic in three dimensions.
	bool isLattice() const { return numNodes == numCoreNodes && periodicity == 3; }
};

}; // End of namespace

#endif // __CA_SUPER_PATTERN_H
