///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_LAMMPS_ATOMIC_STRUCTURE_H
#define __CA_LAMMPS_ATOMIC_STRUCTURE_H

#include <calib/atomic_structure/AtomicStructure.h>
#include <calib/output/atoms/AtomicStructureWriter.h>

namespace CALib {

/**
 * Interface between LAMMPS and the AtomicStructure class of the CA lib.
 */
class LAMMPSAtomicStructure : public AtomicStructure, Pointers
{
public:

	/// Constructor.
	LAMMPSAtomicStructure(class LAMMPS* lmp, CAContext& context) : AtomicStructure(context, AtomicStructure::GHOST_ATOMS, "LAMMPS"), Pointers(lmp) {}

	/// Transfers the atoms from LAMMPS to the internal memory.
	void transferLAMMPSAtoms() {
		using namespace std;

		// Make sure we always start with an empty data structure.
		CALIB_ASSERT(numLocalAtoms() == 0);

		// Store timestep number from LAMMPS.
		// This information will show up in the header of output files written by the CA lib.
		setTimestep(update->ntimestep);

		// Take current simulation cell geometry from LAMMPS.
		setupSimulationCell();

		context().msgLogger() << "Transferring " << atom->natoms << " atoms from LAMMPS to crystal analysis engine." << endl;

		// Copy local atoms from LAMMPS array to internal storage.
		beginAddingAtoms(atom->nlocal);
		for(int i = 0; i < atom->nlocal; i++) {
			Vector3 pos(atom->x[i][0], atom->x[i][1], atom->x[i][2]);

			// Determine the processor domain the atom is located in.
			CALIB_ASSERT(absoluteToProcessorGrid(pos, false) == parallel().processorLocationPoint());

			addAtom(pos, this->atom->tag[i], this->atom->type[i], parallel().processorLocationPoint());
		}
		endAddingAtoms();

		// Double check that every atom has been assigned to a processor.
		if(numTotalAtoms() != atom->natoms)
			context().raiseErrorAll("Data transfer error. Not all input atoms could be assigned to a processor.");

		// Determine bounding box of atoms.
		computeBoundingBox();
	}

protected:

	void setupSimulationCell() {

		boost::array<bool,3> pbc;
		pbc[0] = domain->periodicity[0];
		pbc[1] = domain->periodicity[1];
		pbc[2] = domain->periodicity[2];
		AffineTransformation simulationCell;
		simulationCell(0,0) = domain->h[0];
		simulationCell(1,1) = domain->h[1];
		simulationCell(2,2) = domain->h[2];
		simulationCell(1,2) = domain->h[3];
		simulationCell(0,2) = domain->h[4];
		simulationCell(0,1) = domain->h[5];
		simulationCell(1,0) = simulationCell(2,0) = simulationCell(2,1) = 0;
		simulationCell.translation() = Vector3(domain->boxlo[0], domain->boxlo[1], domain->boxlo[2]);

		setupSimulationCellAndDecomposition(simulationCell, pbc, false);
	}
};

}; // End of namespace

#endif // __CA_LAMMPS_ATOMIC_STRUCTURE_H
