///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_LIBRARY_SETTINGS_H
#define __CRYSTAL_ANALYSIS_LIBRARY_SETTINGS_H

#include "CALib.h"

namespace CALib {

/// The code's version number
#define CA_LIB_VERSION_STRING	 					"0.9.20"

/// The current file format version number for pattern catalog files.
#define CA_PATTERN_CATALOG_FILEFORMAT_VERSION	 	6

/// The current file format version number for analysis output files.
#define CA_OUTPUT_FILEFORMAT_VERSION	 			4

/// The maximum number of neighbors an atom may have.
#define CA_MAX_ATOM_NEIGHBORS						32

/// The maximum number of neighbors in a coordination pattern.
#define CA_MAX_PATTERN_NEIGHBORS					18

/// The maximum number of edges in a Burgers circuit used during search of primary dislocation segments.
#define CA_DEFAULT_MAX_BURGERS_CIRCUIT_SIZE			9

/// The maximum number of edges in a Burgers circuit when the primary dislocation segments are being extended towards the nodal points.
#define CA_DEFAULT_MAX_EXTENDED_BURGERS_CIRCUIT_SIZE	13

/// The default number of iterations performed for the dislocation line smoothing algorithm.
#define CA_DEFAULT_LINE_SMOOTHING_LEVEL				4

/// The default distance between sampling points for dislocation lines prior to smoothing.
#define CA_DEFAULT_LINE_POINT_INTERVAL				2.0

/// The default number of iterations performed for the mesh smoothing algorithm.
#define CA_DEFAULT_SURFACE_SMOOTHING_LEVEL			8

/// Controls the distance between tessellation helper points (multiples of the neighbor cutoff).
#define CA_DEFAULT_HELPER_POINT_DISTANCE			1.2

/// Controls the ghost layer thickness (multiples of the neighbor cutoff).
#define CA_DEFAULT_GHOST_ATOM_LAYER_THICKNESS		CALib::CAFloat(1.25*sqrt(6.0))

/// Two lattice space vectors are considered equal if they don't differ by more than this value.
#define CA_LATTICE_VECTOR_EPSILON					CALib::CAFloat(1e-3)

/// Two world-space vectors are considered equal if they don't differ by more than this value.
#define CA_ATOM_VECTOR_EPSILON						CALib::CAFloat(1e-4)

/// Two transitions matrices are considered equal if their elements don't differ by more than this value.
#define CA_TRANSITION_MATRIX_EPSILON				CALib::CAFloat(1e-4)

/// Define a data type that is used to store atom counts and IDs.
/// This can be a 64-bit integer type if you want to support more than 2 billion atoms.
typedef int AtomInteger;

/// The image number of an atom is stored in the upper bits of a atom tag number, i.e., it is shifted to the left by this bit count.
#define CA_ATOM_IMAGE_SHIFT							29

/// The MPI data type corresponding to an AtomInteger.
#define MPI_ATOMINT MPI_INT

// Some checks:

#if CA_MAX_ATOM_NEIGHBORS > 32
	#error "CA_MAX_ATOM_NEIGHBORS is set too high."
#endif

#if CA_MAX_PATTERN_NEIGHBORS > CA_MAX_ATOM_NEIGHBORS
	#error "CA_MAX_PATTERN_NEIGHBORS must not exceed CA_MAX_ATOM_NEIGHBORS."
#endif

};

#endif // __CRYSTAL_ANALYSIS_LIBRARY_SETTINGS_H

