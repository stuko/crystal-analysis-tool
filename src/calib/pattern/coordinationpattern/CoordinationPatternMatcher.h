///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_COORDINATION_PATTERN_MATCHER_H
#define __CA_COORDINATION_PATTERN_MATCHER_H

#include "../../CALib.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../atomic_structure/neighbor_list/NeighborList.h"
#include "CoordinationPattern.h"

namespace CALib {

/// Fast sorting function for an array of (bounded) integers.
/// Sorts values in descending order.
template<typename iterator>
void bitmapSort(iterator begin, iterator end, int max)
{
	CALIB_ASSERT(max < 32);
	CALIB_ASSERT(end >= begin);
	int bitarray = 0;
	for(iterator pin = begin; pin != end; ++pin) {
		CALIB_ASSERT(*pin >= 0 && *pin < max);
		bitarray |= 1 << (*pin);
	}
	iterator pout = begin;
	for(int i = max - 1; i >= 0; i--)
		if(bitarray & (1 << i))
			*pout++ = i;
	CALIB_ASSERT(pout == end);
}

class CoordinationPatternMatcher
{
protected:

	/// Constructor.
	CoordinationPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern);

public:

	/// Virtual destructor.
	virtual ~CoordinationPatternMatcher() {}

	/// Creates the right pattern matcher for the given pattern.
	static std::auto_ptr<CoordinationPatternMatcher> createPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern);

	/// Finds the next occurrence of the pattern at the given atom.
	/// Returns false if no more matches could be found.
	bool findNextPermutation(int atomIndex);

	/// Returns the total number of matches found so far.
	AtomInteger numberOfMatches() const { return _numMatches; }

	/// Returns the number of atoms at which a match was found.
	AtomInteger numberOfAtomsMatched() const { return _numAtomsMatched; }

	/// Returns the total number of trial permutations that have been tested so far.
	AtomInteger numberOfTrialPermutations() const { return _numTrialPermutations; }

	/// After a match has been found, this method returns the neighbor index from the
	/// input structure which matches the given pattern neighbor.
	int atomNeighborIndex(int patternNeighborIndex) const {
		CALIB_ASSERT(patternNeighborIndex < _pattern.numNeighbors());
		return _atomNeighborIndices[patternNeighborIndex];
	}

	/// After a match has been found, this method returns the mapping from pattern neighbors to atom neighbors.
	const CoordinationPermutation& permutation() const { return _atomNeighborIndices; }

	/// After a match has been found, this method returns the mapping from atom neighbors to pattern neighbors.
	CoordinationPermutation inversePermutation() const {
		CoordinationPermutation patternNeighborIndices(_pattern.numNeighbors());
		for(int i = 0; i < _pattern.numNeighbors(); i++)
			patternNeighborIndices[_atomNeighborIndices[i]] = i;
		return patternNeighborIndices;
	}

	/// Returns the pattern that this matcher object searches for.
	const CoordinationPattern& pattern() const { return _pattern; }

	/// Returns the structure in which this matcher object searches for the pattern.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns a const-reference to the neighbor list used for pattern matching.
	const NeighborList& neighborList() const { return _neighborList; }

	/// Returns the index of the central atom after a match has been found.
	int atomIndex() const { return _atomIndex; }

	/// Returns the cutoff radius being used for the pattern matching.
	CAFloat cutoff() const { return _cutoff; }

	/// Changes the cutoff radius to use for the pattern matching.
	/// The cutoff must not be larger than the cutoff used to generate the neighbor lists.
	void setCutoff(CAFloat r) {
		CALIB_ASSERT(r <= neighborList().cutoff());
		_cutoff = r;
	}


protected:

	// Virtual methods that must be implemented by specific subclasses.
	virtual bool initializeNeighborPermutations() = 0;
	virtual bool nextNeighborPermutation() = 0;

protected:

	/// The structure in which the pattern is searched.
	const AtomicStructure& _structure;

	/// The neighbor list for the atomic structure.
	const NeighborList& _neighborList;

	/// The pattern that is being searched for.
	const CoordinationPattern& _pattern;

	/// The current center atom.
	int _atomIndex;

	/// The cutoff radius to use for the pattern matching.
	CAFloat _cutoff;

	/// Total number of matches found so far.
	AtomInteger _numMatches;

	/// Number of atoms at which a match was found.
	AtomInteger _numAtomsMatched;

	/// The total number of trial permutations that have been tested so far.
	AtomInteger _numTrialPermutations;

	CoordinationPermutation _atomNeighborIndices;
	CoordinationPermutation _previousIndices;
	int _validUpTo;
};

}; // End of namespace

#endif // __CA_COORDINATION_PATTERN_MATCHER_H

