///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_NULLSTREAM_H
#define __CRYSTAL_ANALYSIS_NULLSTREAM_H

#include "../CALib.h"

namespace CALib {

/**
 *  A STL stream class that sends its data to nowhere.
 */
class NullStream : public std::ostream {
public:
	NullStream() : std::ostream(NULL) {}
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_NULLSTREAM_H

