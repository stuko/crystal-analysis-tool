///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_FIELD_H
#define __CA_FIELD_H

#include "../CALib.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../tessellation/DelaunayTessellation.h"
#include "../context/CAContext.h"

namespace CALib {

/**
 * Base class for a field defined on the Delaunay tessellation.
 */
class Field : public ContextReference
{
public:

	/// Constructor.
	Field(CAContext& context, AtomicStructure& structure, DelaunayTessellation& tessellation);

	/// Returns the tessellation.
	const DelaunayTessellation& tessellation() const { return _tessellation; }

	/// Returns the atomic structure to which the tessellation belongs.
	const AtomicStructure& structure() const { return _structure; }

	/// Generates the list of cells owned by the local processor.
	void buildLocalCellList();

	/// Determines whether the given tetrahedral cell of the tessellation is owned by the
	/// local processor and connects four physical atoms.
	bool isValidAndLocalCell(DelaunayTessellation::CellHandle cell) const {
		return tessellation().isValidAndLocalCell(cell);
	}

	/// Returns the list of tessellation cells owned by the local processor.
	const std::vector<DelaunayTessellation::CellHandle>& localCells() const { return _localCells; }

private:

	/// The atomic structure.
	const AtomicStructure& _structure;

	/// The Delaunay triangulation.
	const DelaunayTessellation& _tessellation;

	/// List of tessellation cells owned by the local processor.
	std::vector<DelaunayTessellation::CellHandle> _localCells;
};

}; // End of namespace

#endif // __CA_FIELD_H

