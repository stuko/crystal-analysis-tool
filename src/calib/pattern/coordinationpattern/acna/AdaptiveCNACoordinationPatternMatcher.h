///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __ADAPTIVE_CNA_COORDINATION_PATTERN_MATCHER_H
#define __ADAPTIVE_CNA_COORDINATION_PATTERN_MATCHER_H

#include "../cna/CNACoordinationPatternMatcher.h"

namespace CALib {

class AdaptiveCNACoordinationPatternMatcher : public CNACoordinationPatternMatcher
{
public:

	/// Constructor.
	AdaptiveCNACoordinationPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern) :
		CNACoordinationPatternMatcher(structure, neighborList, pattern) {}

protected:

	virtual bool initializeNeighborPermutations();
};

}; // End of namespace

#endif // __ADAPTIVE_CNA_COORDINATION_PATTERN_MATCHER_H

