///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_TIMER_H
#define __CRYSTAL_ANALYSIS_TIMER_H

#include "../CALib.h"
#include <CGAL/Timer.h>

namespace CALib {

/**
 * This helper class is used to measure the performance of the program.
 */
class Timer
{
public:

	/// Constructs the timer and starts it if 'running' is true.
	Timer(bool running = true) { if(running) _timer.start(); }

	/// Puts the timer into the running state.
	void start() { _timer.start(); }

	/// Puts the timer into the stopped state.
	void stop() { _timer.stop(); }

	/// Returns the time (in seconds) elapsed while the timer was running.
	double elapsedTime() const { return _timer.time(); }

private:

	/// The internal timer object.
	CGAL::Timer _timer;
};

/// Outputs the elapsed time of a timer to a log stream.
inline std::ostream& operator<<(std::ostream& stream, const Timer& timer) {
	using namespace std;
	streamsize oldprecision = stream.precision(3);
	stream << fixed << timer.elapsedTime() << " sec";
	stream.unsetf(ios_base::floatfield);
	stream.precision(oldprecision);
	return stream;
}

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_TIMER_H

