///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "ElasticFieldWriter.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

static void outputCellVolume(CAFloat& v, const Field& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = field.tessellation().cellVolume(field.localCells()[cellIndex]);
}

static void outputCellAspectRatio(CAFloat& v, const Field& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = field.tessellation().cellAspectRatio(field.localCells()[cellIndex]);
}

static void outputElasticDeformationGradientComponent(int i, int j, CAFloat& v, const ElasticField& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = field.elasticDeformationGradients()[cellIndex](i,j);
}

static void outputHasElasticDeformationGradient(int& v, const ElasticField& field, int cellIndex, const FieldWriter& fieldWriter) {
	v = (field.elasticDeformationGradients()[cellIndex] != Matrix3::Zero());
}

static void outputVolumetricElasticStrain(CAFloat& v, const ElasticField& field, int cellIndex, const FieldWriter& fieldWriter) {
	const Matrix3& F = field.elasticDeformationGradients()[cellIndex];
	v = ((F.transpose() * F - Matrix3::Identity()) * 0.5).trace() / 3.0;
}

static void outputShearElasticStrain(CAFloat& v, const ElasticField& field, int cellIndex, const FieldWriter& fieldWriter) {
	const Matrix3& F = field.elasticDeformationGradients()[cellIndex];
	Matrix3 strain = (F.transpose() * F - Matrix3::Identity()) * 0.5;
	v = sqrt(square(strain(0,1)) + square(strain(1,2)) + square(strain(0,2)) +
			(square(strain(1,1) - strain(2,2)) + square(strain(0,0) - strain(2,2)) + square(strain(0,0) - strain(1,1))) / 6.0);
}

/******************************************************************************
* Outputs the given components of the deformation field to a VTK file.
******************************************************************************/
void ElasticFieldWriter::writeVTKFile(ostream& stream, const ElasticField& field, const std::bitset<NUM_COMPONENTS>& components)
{
	// Write tessellation vertices and cell information to output file.
	FieldWriter::writeVTKFile(stream, field, "Elastic field");

	if(components.test(CELL_VOLUME))
		writeVTKCellFloat(stream, "volume", field, &outputCellVolume);
	if(components.test(CELL_ASPECT_RATIO))
		writeVTKCellFloat(stream, "aspect_ratio", field, &outputCellAspectRatio);

	// Write elastic deformation gradient field.
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 3; j++) {
			if(components.test(F_11 + i*3 + j)) {
				ostringstream fieldName;
				fieldName << "F_" << (i+1) << (j+1);
				writeVTKCellFloat(stream, fieldName.str(), field, boost::bind(&outputElasticDeformationGradientComponent, i, j, _1, _2, _3, _4));
			}
		}
	}

	if(components.test(CELL_HAS_ELASTIC_F))
		writeVTKCellInt(stream, "has_F_elastic", field, &outputHasElasticDeformationGradient);

	if(components.test(VOLUMETRIC_STRAIN))
		writeVTKCellFloat(stream, "volumetric_strain", field, &outputVolumetricElasticStrain);

	if(components.test(SHEAR_STRAIN))
		writeVTKCellFloat(stream, "shear_strain", field, &outputShearElasticStrain);
}


}; // End of namespace
