///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CNA_COORDINATION_PATTERN_MATCHER_H
#define __CNA_COORDINATION_PATTERN_MATCHER_H

#include "../CoordinationPatternMatcher.h"
#include "CNACoordinationPattern.h"

namespace CALib {

class CNACoordinationPatternMatcher : public CoordinationPatternMatcher
{
public:

	/// Constructor.
	CNACoordinationPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern) :
		CoordinationPatternMatcher(structure, neighborList, pattern) {}

protected:

	virtual bool initializeNeighborPermutations();
	virtual bool nextNeighborPermutation();

	bool validatePermutation();

protected:

	/// Two-dimensional bit array that stores the bonds between neighbors.
	NeighborBondArray _neighborArray;

	/// The CNA signatures for the neighbors of the central atom.
	CNABond _cnaSignatures[CA_MAX_PATTERN_NEIGHBORS];
};

}; // End of namespace

#endif // __CNA_COORDINATION_PATTERN_MATCHER_H
