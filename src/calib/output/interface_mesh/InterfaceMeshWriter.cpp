///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "InterfaceMeshWriter.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Outputs the interface mesh to a VTK file.
******************************************************************************/
void InterfaceMeshWriter::writeVTKFile(ostream& stream, const InterfaceMesh& mesh)
{
	if(!parallel().isMaster())
		return;

	// Count facets which are not crossing the periodic boundaries.
	size_t numFacets = 0;
	BOOST_FOREACH(const InterfaceMesh::Facet* f, mesh.facets()) {
		if(isWrappedFacet(mesh.structure(), f) == false)
			numFacets++;
	}

	stream << "# vtk DataFile Version 3.0\n";
	stream << "# Interface mesh\n";
	stream << "ASCII\n";
	stream << "DATASET UNSTRUCTURED_GRID\n";
	stream << "POINTS " << mesh.nodes().size() << " float\n";
	BOOST_FOREACH(const InterfaceMesh::Node* n, mesh.nodes()) {
		const Vector3& pos = n->pos;
		stream << pos.x() << " " << pos.y() << " " << pos.z() << "\n";
	}
	stream << "\nCELLS " << numFacets << " " << (numFacets*4) << "\n";
	BOOST_FOREACH(const InterfaceMesh::Facet* f, mesh.facets()) {
		if(isWrappedFacet(mesh.structure(), f) == false) {
			stream << "3";
			for(int i = 0; i < 3; i++)
				stream << " " << f->vertex(2-i)->index;
			stream << "\n";
		}
	}

	stream << "\nCELL_TYPES " << numFacets << "\n";
	for(size_t i = 0; i < numFacets; i++)
		stream << "5\n";	// Triangle

#if 0

	stream << "\nCELL_DATA " << numFacets << "\n";

	stream << "\nSCALARS dislocation_segment int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	for(vector<InterfaceMeshFacet*>::const_iterator f = facets.begin(); f != facets.end(); ++f) {
		if(isWrappedFacet(*f) == false) {
			if((*f)->circuit != NULL && ((*f)->circuit->isDangling == false || (*f)->testFlag(InterfaceMeshFacet::IS_PRIMARY_SEGMENT))) {
				DislocationSegment* segment = (*f)->circuit->segment;
				while(segment->replacedWith != NULL) segment = segment->replacedWith;
				stream << segment->id << "\n";
			}
			else
				stream << "-1\n";
		}
	}

	stream << "\nSCALARS swept_by_segment int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	for(vector<InterfaceMeshFacet*>::const_iterator f = facets.begin(); f != facets.end(); ++f) {
		if(isWrappedFacet(*f) == false) {
			if((*f)->circuit != NULL)
				stream << (*f)->circuit->segment->id << "\n";
			else
				stream << "-1\n";
		}
	}

	stream << "\nSCALARS is_primary_segment int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	for(vector<InterfaceMeshFacet*>::const_iterator f = facets.begin(); f != facets.end(); ++f)
		if(isWrappedFacet(*f) == false)
			stream << (*f)->testFlag(InterfaceMeshFacet::IS_PRIMARY_SEGMENT) << "\n";

	stream << "\nSCALARS is_ghost_facet int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	for(vector<InterfaceMeshFacet*>::const_iterator f = facets.begin(); f != facets.end(); ++f)
		if(isWrappedFacet(*f) == false)
			stream << (*f)->testFlag(InterfaceMeshFacet::IS_GHOST_FACET) << "\n";

#endif
}

/******************************************************************************
* Determines whether a mesh triangle facet crosses a periodic boundary.
******************************************************************************/
bool InterfaceMeshWriter::isWrappedFacet(const AtomicStructure& structure, const InterfaceMesh::Facet* facet) const
{
	if(structure.isWrappedVector(facet->vertex(1)->pos - facet->vertex(0)->pos))
		return true;

	if(structure.isWrappedVector(facet->vertex(2)->pos - facet->vertex(0)->pos))
		return true;

	return false;
}

}; // End of namespace
