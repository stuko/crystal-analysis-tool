///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructureParser.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"
#include "../../util/Timer.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Reads the atomic coordinates from the input file in LAMMPS dump format.
******************************************************************************/
void AtomicStructureParser::readLAMMPSAtomsFile(TextParserStream& stream)
{
	context().msgLogger() << "Parsing LAMMPS dump file." << endl;
	Timer loadTimer;

	int columnPosx = -1;
	int columnPosy = -1;
	int columnPosz = -1;
	int columnId = -1;
	int columnType = -1;
	bool reducedCoordinates = false;
	int numberOfColumns = 0;
	int timestep = 0;
	boost::array<bool,3> pbc;
	pbc.assign(false);
	AffineTransformation simulationCell;

	if(parallel().isMaster()) {
		bool parseAtoms = false;
		do {
			do {
				if(stream.line().find("ITEM: NUMBER OF ATOMS") != string::npos) {
					// Parse number of atoms.
					unsigned long int na;
					if(sscanf(stream.readline().c_str(), "%lu", &na) != 1 || na > 1e9)
						context().raiseErrorOne("LAMMPS dump file parsing error. Invalid number of atoms (line %d): %s", stream.lineNumber(), stream.line().c_str());
					_numInputAtoms = (AtomInteger)na;
					break;
				}
				else if(stream.line().find("ITEM: ATOMS") != string::npos) {

					// Parse column names.
					string::const_iterator tokenBegin = stream.line().begin() + 11;
					while(tokenBegin != stream.line().end()) {
						while(tokenBegin != stream.line().end() && (*tokenBegin == ' ' || *tokenBegin == '\t')) ++tokenBegin;
						if(tokenBegin == stream.line().end()) break;
						string::const_iterator tokenEnd = tokenBegin + 1;
						while(tokenEnd != stream.line().end() && *tokenEnd != ' ' && *tokenEnd != '\t') ++tokenEnd;
						string columnName(tokenBegin, tokenEnd);
						if(!_coordinateColumnNameX.empty() && columnName == _coordinateColumnNameX) columnPosx = numberOfColumns;
						else if(!_coordinateColumnNameY.empty() && columnName == _coordinateColumnNameY) columnPosy = numberOfColumns;
						else if(!_coordinateColumnNameZ.empty() && columnName == _coordinateColumnNameZ) columnPosz = numberOfColumns;
						else if(columnName == "x" || columnName == "xu") columnPosx = numberOfColumns;
						else if(columnName == "y" || columnName == "yu") columnPosy = numberOfColumns;
						else if(columnName == "z" || columnName == "zu") columnPosz = numberOfColumns;
						else if(columnName == "id") columnId = numberOfColumns;
						else if(columnName == "type") columnType = numberOfColumns;
						else if(columnName == "xs") { columnPosx = numberOfColumns; reducedCoordinates = true; }
						else if(columnName == "ys") { columnPosy = numberOfColumns; reducedCoordinates = true; }
						else if(columnName == "zs") { columnPosz = numberOfColumns; reducedCoordinates = true; }
						tokenBegin = tokenEnd;
						numberOfColumns++;
					}
					if(numberOfColumns == 0) context().raiseErrorOne("LAMMPS dump file parsing error. File does not contain column identifiers. File format is too old.");
					if(columnPosx == -1) context().raiseErrorOne("LAMMPS dump file parsing error. Input file does not contain X coordinate column.");
					if(columnPosy == -1) context().raiseErrorOne("LAMMPS dump file parsing error. Input file does not contain Y coordinate column.");
					if(columnPosz == -1) context().raiseErrorOne("LAMMPS dump file parsing error. Input file does not contain Z coordinate column.");

					// Stop parsing header and continue with atoms.
					parseAtoms = true;
					break;
				}
				if(stream.line().find("ITEM: TIMESTEP") != string::npos) {
					if(sscanf(stream.readline().c_str(), "%i", &timestep) != 1)
						context().raiseErrorOne("LAMMPS dump file parsing error. Invalid timestep number (line %d): %s", stream.lineNumber(), stream.line().c_str());
					break;
				}
				else if(stream.line().find("ITEM: BOX BOUNDS xy xz yz") == 0) {
					// Parse optional boundary condition flags.
					istringstream ss(stream.line().substr(strlen("ITEM: BOX BOUNDS xy xz yz")));
					string pbcx, pbcy, pbcz;
					ss >> pbcx >> pbcy >> pbcz;
					if(pbcx.length() == 2 && pbcy.length() == 2 && pbcz.length() == 2) {
						pbc[0] = (pbcx == "pp");
						pbc[1] = (pbcy == "pp");
						pbc[2] = (pbcz == "pp");
					}

					// Parse triclinic simulation box.
					CAFloat tiltFactors[3];
					CAFloat simBox[2][3];
					for(size_t k=0; k<3; k++) {
						if(sscanf(stream.readline().c_str(), CAFLOAT_SCANF_STRING_3, &simBox[0][k], &simBox[1][k], &tiltFactors[k]) != 3)
							context().raiseErrorOne("LAMMPS dump file parsing error. Invalid box size in line %d of dump file: %s", stream.lineNumber(), stream.line().c_str());
					}
					// LAMMPS only stores the outer bounding box of the simulation cell in the dump file.
					// We have to determine the size of the actual triclinic cell.
					simBox[0][0] -= min(min(min(tiltFactors[0], tiltFactors[1]), tiltFactors[0]+tiltFactors[1]), (CAFloat)0.0);
					simBox[1][0] -= max(max(max(tiltFactors[0], tiltFactors[1]), tiltFactors[0]+tiltFactors[1]), (CAFloat)0.0);
					simBox[0][1] -= min(tiltFactors[2], (CAFloat)0.0);
					simBox[1][1] -= max(tiltFactors[2], (CAFloat)0.0);
					simulationCell.translation() = Vector3(simBox[0][0], simBox[0][1], simBox[0][2]);
					simulationCell.linear().col(0) = Vector3(simBox[1][0] - simBox[0][0], 0, 0);
					simulationCell.linear().col(1) = Vector3(tiltFactors[0], simBox[1][1] - simBox[0][1], 0);
					simulationCell.linear().col(2) = Vector3(tiltFactors[1], tiltFactors[2], simBox[1][2] - simBox[0][2]);
					break;
				}
				else if(stream.line().find("ITEM: BOX BOUNDS") == 0) {
					// Parse optional boundary condition flags.
					istringstream ss(stream.line().substr(strlen("ITEM: BOX BOUNDS")));
					string pbcx, pbcy, pbcz;
					ss >> pbcx >> pbcy >> pbcz;
					if(pbcx.length() == 2 && pbcy.length() == 2 && pbcz.length() == 2) {
						pbc[0] = (pbcx == "pp");
						pbc[1] = (pbcy == "pp");
						pbc[2] = (pbcz == "pp");
					}

					// Parse orthogonal simulation box size.
					CAFloat simBox[2][3];
					for(size_t k=0; k<3; k++) {
						if(sscanf(stream.readline().c_str(), CAFLOAT_SCANF_STRING_2, &simBox[0][k], &simBox[1][k]) != 2)
							context().raiseErrorOne("LAMMPS dump file parsing error. Invalid box size in line %d of dump file: %s", stream.lineNumber(), stream.line().c_str());
					}
					simulationCell.translation() = Vector3(simBox[0][0], simBox[0][1], simBox[0][2]);
					simulationCell.linear().col(0) = Vector3(simBox[1][0] - simBox[0][0], 0, 0);
					simulationCell.linear().col(1) = Vector3(0, simBox[1][1] - simBox[0][1], 0);
					simulationCell.linear().col(2) = Vector3(0, 0, simBox[1][2] - simBox[0][2]);
					break;
				}
				else if(stream.line().find("ITEM:") != string::npos) {
					// Skip lines up to next ITEM.
					while(!stream.eof()) {
						stream.readline();
						if(stream.line().find("ITEM:") != string::npos) break;
					}
				}
				else if(stream.line().find_first_not_of(" \t\n\r") == string::npos) {
					// Skip empty lines
					break;
				}
				else {
					context().raiseErrorOne("LAMMPS dump file parsing error. Invalid line %d in file: %s", stream.lineNumber(), stream.line().c_str());
				}
			}
			while(!stream.eof() && !parseAtoms);
			if(parseAtoms) break;

			// Parse next line.
			stream.readline();
		}
		while(!stream.eof());
	}
	parallel().broadcast(_numInputAtoms);
	parallel().broadcast(timestep);
	_structure.setTimestep(timestep);

	setupSimulationCell(simulationCell, pbc);

	context().msgLogger() << "Reading " << _numInputAtoms << " atoms (frame " << timestep << ") from input file." << endl;
	if(reducedCoordinates)
		context().msgLogger() << "LAMMPS file contains reduced atom coordinates." << endl;

	beginDistributeAtoms();

	for(AtomInteger a = 0; a < _numInputAtoms; a++) {
		AtomInfo atomInfo;
		if(parallel().isMaster()) {
			if(stream.eof())
				context().raiseErrorOne("LAMMPS dump file parsing error. Unexpected end of file at line %i.", stream.lineNumber());
			stream.readline();
			atomInfo.tag = a + 1;
			atomInfo.species = 1;
			const char* tokenBegin = stream.line().c_str();
			int columnIndex = 0;
			while(*tokenBegin != '\0') {
				while(*tokenBegin != '\0' && (*tokenBegin == ' ' || *tokenBegin == '\t')) ++tokenBegin;
				if(*tokenBegin == '\0') break;
				const char* tokenEnd = tokenBegin + 1;
				while(*tokenEnd != '\0' && *tokenEnd != ' ' && *tokenEnd != '\t') ++tokenEnd;
				if(columnPosx == columnIndex) {
					if(sscanf(tokenBegin, CAFLOAT_SCANF_STRING_1, &atomInfo.pos.x()) != 1)
						context().raiseErrorOne("LAMMPS dump file parsing error. Invalid X coordinate (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
				}
				else if(columnPosy == columnIndex) {
					if(sscanf(tokenBegin, CAFLOAT_SCANF_STRING_1, &atomInfo.pos.y()) != 1)
						context().raiseErrorOne("LAMMPS dump file parsing error. Invalid Y coordinate (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
				}
				else if(columnPosz == columnIndex) {
					if(sscanf(tokenBegin, CAFLOAT_SCANF_STRING_1, &atomInfo.pos.z()) != 1)
						context().raiseErrorOne("LAMMPS dump file parsing error. Invalid Z coordinate (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
				}
				else if(columnId == columnIndex) {
					long int t;
					if(sscanf(tokenBegin, "%li", &t) != 1 || t <= 0)
						context().raiseErrorOne("LAMMPS dump file parsing error. Invalid atom tag (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
					atomInfo.tag = (AtomInteger)t;
				}
				else if(columnType == columnIndex) {
					if(sscanf(tokenBegin, "%u", &atomInfo.species) != 1)
						context().raiseErrorOne("LAMMPS dump file parsing error. Invalid atom type (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
				}
				tokenBegin = tokenEnd;
				columnIndex++;
			}
			if(columnIndex != numberOfColumns)
				context().raiseErrorOne("LAMMPS dump file parsing error. Unexpected end of line (line %i).", stream.lineNumber());

			if(reducedCoordinates)
				atomInfo.pos = simulationCell * atomInfo.pos;
		}

		distributeAtom(atomInfo);
	}

	endDistributeAtoms();

	context().msgLogger() << "File loading took " << loadTimer << "." << endl;
}

}; // End of namespace
