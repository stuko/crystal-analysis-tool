///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2010, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "TriangleMesh.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Clears the mesh and release all memory.
******************************************************************************/
void TriangleMesh::clear()
{
	_vertices.clear();
	_vertexPool.clear();
	_facets.clear();
	_facetPool.clear();
	_edgePool.clear();
	_numLocalVertices = _numLocalFacets = 0;
}

/******************************************************************************
* Creates a new vertex in the mesh.
******************************************************************************/
TriangleMeshVertex* TriangleMesh::createVertex(const Vector3& pos, bool isGhostVertex, AtomInteger tag)
{
	TriangleMeshVertex* vertex = _vertexPool.construct();
	vertex->pos = pos;
	vertex->tag = tag;
	vertex->index = _vertices.size();
	vertex->numFacets = 0;
	vertex->normal.setZero();
	vertex->edges = NULL;
	if(isGhostVertex)
		vertex->flags.set(TriangleMeshVertex::IS_GHOST_VERTEX);
	else
		_numLocalVertices++;
	_vertices.push_back(vertex);
	return vertex;
}

/******************************************************************************
* Create a new edge (consisting of a pair of halfedges).
******************************************************************************/
TriangleMeshEdge* TriangleMesh::createEdge(TriangleMeshVertex* vertex1, TriangleMeshVertex* vertex2)
{
	TriangleMeshEdge* edge = _edgePool.construct();
	TriangleMeshEdge* oppositeEdge = _edgePool.construct();
	edge->oppositeEdge = oppositeEdge;
	oppositeEdge->oppositeEdge = edge;
	edge->vertex2 = vertex2;
	oppositeEdge->vertex2 = vertex1;
	edge->facet = oppositeEdge->facet = NULL;
	edge->nextEdge = vertex1->edges;
	vertex1->edges = edge;
	oppositeEdge->nextEdge = vertex2->edges;
	vertex2->edges = oppositeEdge;
	return edge;
}

/******************************************************************************
* Create a new half edge.
******************************************************************************/
TriangleMeshEdge* TriangleMesh::createHalfEdge(TriangleMeshVertex* vertex1, TriangleMeshVertex* vertex2)
{
	TriangleMeshEdge* edge = _edgePool.construct();
	edge->oppositeEdge = NULL;
	edge->vertex2 = vertex2;
	edge->facet = NULL;
	edge->nextEdge = vertex1->edges;
	vertex1->edges = edge;
	return edge;
}

/******************************************************************************
* Create a new triangle facet.
******************************************************************************/
TriangleMeshFacet* TriangleMesh::createFacet(TriangleMeshEdge* edges[3], bool isGhostFacet)
{
	TriangleMeshFacet* facet = _facetPool.construct();
	if(isGhostFacet)
		facet->flags.set(TriangleMeshFacet::IS_GHOST_FACET);
	else
		_numLocalFacets++;
	_facets.push_back(facet);
	facet->edges[0] = edges[0];
	facet->edges[1] = edges[1];
	facet->edges[2] = edges[2];
	edges[0]->vertex2->numFacets++;
	edges[1]->vertex2->numFacets++;
	edges[2]->vertex2->numFacets++;
	CALIB_ASSERT(edges[0]->facet == NULL);
	CALIB_ASSERT(edges[1]->facet == NULL);
	CALIB_ASSERT(edges[2]->facet == NULL);
	edges[0]->facet = facet;
	edges[1]->facet = facet;
	edges[2]->facet = facet;
	return facet;
}

/******************************************************************************
* Create a new triangle facet and creates its edges if necessary.
******************************************************************************/
TriangleMeshFacet* TriangleMesh::createFacetAndEdges(TriangleMeshVertex* vertices[3])
{
	TriangleMeshEdge* edges[3];
	for(int v = 0; v < 3; v++) {
		TriangleMeshVertex* v2 = vertices[(v+1)%3];
		TriangleMeshEdge* edge = vertices[v]->edges;
		while(edge != NULL) {
			if(edge->vertex2 == v2) break;
			edge = edge->nextEdge;
		}
		if(edge == NULL)
			edge = createEdge(vertices[v], v2);
		CALIB_ASSERT(edge->facet == NULL);
		edges[v] = edge;
	}
	return createFacet(edges);
}

/******************************************************************************
* Calculates the facet and vertex normals.
******************************************************************************/
void TriangleMesh::calculateNormals(const SimulationCell& cell)
{
	BOOST_FOREACH(TriangleMeshVertex* v, vertices())
		v->normal.setZero();

	BOOST_FOREACH(TriangleMeshFacet* f, facets()) {
		Vector3 normal = cell.wrapVector(f->edges[1]->vertex2->pos - f->edges[0]->vertex2->pos).cross(
									  cell.wrapVector(f->edges[2]->vertex2->pos - f->edges[0]->vertex2->pos));
		if(normal != Vector3::Zero()) {
			normal.normalize();
			for(size_t v = 0; v < 3; v++)
				f->edges[v]->vertex2->normal += normal;
		}
	}

	BOOST_FOREACH(TriangleMeshVertex* v, vertices()) {
		if(v->normal != Vector3::Zero())
			v->normal.normalize();
	}
}

/******************************************************************************
* Tests whether the given point is inside the closed polyhedron described by this mesh.
******************************************************************************/
bool TriangleMesh::pointInPolyhedron(const Vector3& p, const SimulationCell& cell) const
{
	// Implementation of the point in polyhedron test described in:
	//
	// J. Andreas Baerentzen and Henrik Aanaes
	// Signed Distance Computation Using the Angle Weighted Pseudonormal
	// IEEE Transactions on Visualization and Computer Graphics, Volume 11, Issue 3 (May 2005), Pages: 243 - 253

	// Determine which vertex is closest to the test point.
	TriangleMeshVertex* closestVertex = NULL;
	CAFloat closestDistance2 = CAFLOAT_MAX;
	Vector3 closestNormal(Vector3::Zero());
	Vector3 closestVector(Vector3::Zero());
	BOOST_FOREACH(TriangleMeshVertex* v, vertices()) {
		Vector3 r = cell.wrapReducedVector(v->pos - p);
		CAFloat dist2 = r.squaredNorm();
		if(dist2 < closestDistance2) {
			closestDistance2 = dist2;
			closestVertex = v;
			closestVector = r;
		}
	}

	// Check if any edge or any facet is closer to the test point than the closest vertex.
	TriangleMeshEdge* closestEdge = NULL;
	BOOST_FOREACH(TriangleMeshFacet* f, facets()) {
		Vector3 edgeVectors[3];
		Vector3 vertexVectors[3];
		Vector3 baseCorner = f->edges[0]->vertex2->pos;
		Vector3 baseVector = cell.wrapReducedVector(baseCorner - p);

		for(int v = 0; v < 3; v++) {
			TriangleMeshEdge* e = f->edges[v];
			Vector3 lineDir = cell.wrapReducedVector(e->vertex2->pos - e->vertex1()->pos);
			edgeVectors[v] = lineDir;
			Vector3 r = cell.wrapReducedVector(e->vertex1()->pos - baseCorner) + baseVector;
			vertexVectors[v] = r;
			CAFloat edgeLength = lineDir.norm();
			if(edgeLength <= 1e-8) continue;
			lineDir /= edgeLength;
			CAFloat d = lineDir.dot(r);
			if(d >= edgeLength || d <= 0.0) continue;
			Vector3 c = e->vertex2->pos - lineDir * d;
			Vector3 r2 = cell.wrapReducedVector(c - p);
			CAFloat dist2 = r2.squaredNorm();
			if(dist2 < closestDistance2) {
				closestDistance2 = dist2;
				closestVertex = NULL;
				closestEdge = e;
				closestVector = r2;
			}
		}

		Vector3 normal = edgeVectors[0].cross(edgeVectors[1]);
		CAFloat normalLengthSq = normal.squaredNorm();
		if(fabs(normalLengthSq) <= CAFLOAT_EPSILON) continue;
		bool isInsideTriangle = true;
		for(int v = 0; v < 3; v++) {
			if(vertexVectors[v].dot(normal.cross(edgeVectors[v])) >= 0.0) {
				isInsideTriangle = false;
				break;
			}
		}
		if(isInsideTriangle) {
			normal /= sqrt(normalLengthSq);
			CAFloat planeDist = normal.dot(vertexVectors[0]);
			if(planeDist * planeDist < closestDistance2) {
				closestDistance2 = planeDist * planeDist;
				closestVector = normal * planeDist;
				closestVertex = NULL;
				closestEdge = NULL;
				closestNormal = normal;
			}
		}
	}

	if(closestEdge != NULL) {
		cell.context().msgLogger() << "POINT IN POLYHEDRON TEST: Edge is closest. WARNING: This is untested code! You may get wrong surface cap output." << endl;

		// Calculate pseudo-normal at edge.
		TriangleMeshFacet* facets[2] = { closestEdge->facet, closestEdge->oppositeEdge->facet };
		closestNormal.setZero();
		for(int f = 0; f < 2; f++) {
			Vector3 edge1 = cell.wrapReducedVector(facets[f]->edges[0]->vertex2->pos - facets[f]->edges[0]->vertex1()->pos);
			Vector3 edge2 = cell.wrapReducedVector(facets[f]->edges[1]->vertex2->pos - facets[f]->edges[1]->vertex1()->pos);
			Vector3 normal = edge1.cross(edge2);
			if(normal != Vector3::Zero())
				closestNormal += normal.normalized();
		}
	}
	else if(closestVertex != NULL) {
		// Calculate pseudo-normal at vertex.
		TriangleMeshEdge* edge = closestVertex->edges;
		CALIB_ASSERT(edge != NULL);
		TriangleMeshFacet* facet = edge->facet;
		CALIB_ASSERT(facet != NULL);
		closestNormal.setZero();
		Vector3 edge1v = cell.wrapReducedVector(edge->vertex2->pos - closestVertex->pos);
		if(edge1v != Vector3::Zero()) edge1v.normalize();
		do {
			TriangleMeshEdge* nextEdge = facet->edges[(facet->edgeIndex(edge)+2)%3]->oppositeEdge;
			CALIB_ASSERT(nextEdge->vertex1() == closestVertex);
			Vector3 edge2v = cell.wrapReducedVector(nextEdge->vertex2->pos - closestVertex->pos);
			if(edge2v != Vector3::Zero()) edge2v.normalize();
			CAFloat angle = acos(edge1v.dot(edge2v));
			Vector3 normal = edge1v.cross(edge2v);
			if(normal != Vector3::Zero())
				closestNormal += normal.normalized() * angle;
			facet = nextEdge->facet;
			edge = nextEdge;
			edge1v = edge2v;
		}
		while(edge != closestVertex->edges);
	}

	return closestNormal.dot(closestVector) > 0.0;
}

/******************************************************************************
* Duplicates mesh vertices which are part of multiple manifolds.
******************************************************************************/
size_t TriangleMesh::duplicateSharedVertices()
{
	size_t numSharedVertices = 0;
	size_t oldVertexCount = vertices().size();
	for(size_t vertexIndex = 0; vertexIndex < oldVertexCount; vertexIndex++) {
		TriangleMeshVertex* vertex = vertices()[vertexIndex];

		// Count edges connected to the vertex.
		int numVertexEdges = 0;
		for(TriangleMeshEdge* edge = vertex->edges; edge != NULL; edge = edge->nextEdge)
			numVertexEdges++;
		CALIB_ASSERT(numVertexEdges >= 2);

		// Go in positive direction around vertex, facet by facet.
		TriangleMeshEdge* currentEdge = vertex->edges;
		int numManifoldEdges = 0;
		do {
			CALIB_ASSERT(currentEdge != NULL && currentEdge->facet != NULL);
			currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
			numManifoldEdges++;
		}
		while(currentEdge != vertex->edges);

		if(numManifoldEdges == numVertexEdges) {
			CALIB_ASSERT(numVertexEdges == vertex->numFacets);
			continue;		// Vertex is not part of multiple manifolds.
		}

		set<TriangleMeshEdge*> visitedEdges;
		currentEdge = vertex->edges;
		vertex->numFacets = 0;
		do {
			visitedEdges.insert(currentEdge);
			vertex->numFacets++;
			currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
		}
		while(currentEdge != vertex->edges);

		while(visitedEdges.size() < numVertexEdges) {

			// Create a second vertex that takes the edges not visited yet.
			TriangleMeshVertex* secondVertex = createVertex(vertex->pos);

			TriangleMeshEdge* startEdge;
			for(startEdge = vertex->edges; startEdge != NULL; startEdge = startEdge->nextEdge) {
				if(visitedEdges.find(startEdge) == visitedEdges.end())
					break;
			}
			CALIB_ASSERT(startEdge != NULL);

			currentEdge = startEdge;
			do {
				CALIB_ASSERT(visitedEdges.find(currentEdge) == visitedEdges.end());
				visitedEdges.insert(currentEdge);
				for(TriangleMeshEdge* previousEdge = vertex->edges; previousEdge != NULL; previousEdge = previousEdge->nextEdge) {
					if(previousEdge->nextEdge == currentEdge) {
						previousEdge->nextEdge = currentEdge->nextEdge;
						break;
					}
				}
				currentEdge->nextEdge = secondVertex->edges;
				secondVertex->edges = currentEdge;
				secondVertex->numFacets++;
				TriangleMeshEdge* oppositeEdge = currentEdge->oppositeEdge;
				oppositeEdge->vertex2 = secondVertex;
				currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
			}
			while(currentEdge != startEdge);
		}

		numSharedVertices++;
	}

	return numSharedVertices;
}

/******************************************************************************
* Smoothes the output mesh for better visualization results.
******************************************************************************/
void TriangleMesh::smoothMesh(int smoothingLevel, const SimulationCell& cell)
{
	if(smoothingLevel <= 0) return;

	// This is the implementation of the mesh smoothing algorithm:
	//
	// Gabriel Taubin
	// A Signal Processing Approach To Fair Surface Design
	// In SIGGRAPH 95 Conference Proceedings, pages 351-358 (1995)

	CAFloat k_PB = 0.1;
	CAFloat lambda = 0.5;
	CAFloat mu = 1.0 / (k_PB - 1.0/lambda);
	const CAFloat prefactors[2] = { lambda, mu };

	for(int iteration = 0; iteration < smoothingLevel; iteration++) {
		for(int pass = 0; pass <= 1; pass++) {
			smoothMesh(prefactors[pass], cell, false);
		}
	}
}

/******************************************************************************
* Smoothes the output mesh for better visualization results.
******************************************************************************/
void TriangleMesh::smoothMesh(CAFloat prefactor, const SimulationCell& cell, bool projectToNormals)
{
	// Reset Laplacians
	BOOST_FOREACH(TriangleMeshVertex* v, vertices())
		v->laplacian.setZero();

	// Compute discrete Laplacian for each vertex.
	BOOST_FOREACH(TriangleMeshFacet* facet, facets()) {
		Vector3 delta1 = cell.wrapVector(facet->edges[0]->vertex2->pos - facet->edges[2]->vertex2->pos);
		Vector3 delta2 = cell.wrapVector(facet->edges[1]->vertex2->pos - facet->edges[0]->vertex2->pos);
		Vector3 delta3 = cell.wrapVector(facet->edges[2]->vertex2->pos - facet->edges[1]->vertex2->pos);
		facet->edges[2]->vertex2->laplacian += delta1;
		facet->edges[0]->vertex2->laplacian += delta2;
		facet->edges[1]->vertex2->laplacian += delta3;
		facet->edges[0]->vertex2->laplacian -= delta1;
		facet->edges[1]->vertex2->laplacian -= delta2;
		facet->edges[2]->vertex2->laplacian -= delta3;
	}

	// Do smoothing.
	BOOST_FOREACH(TriangleMeshVertex* vertex, vertices()) {
		if(!vertex->flags.test(TriangleMeshVertex::VERTEX_IS_FIXED) && vertex->numFacets != 0) {
			Vector3 d = (prefactor / (vertex->numFacets * 2)) * vertex->laplacian;
			if(projectToNormals) d = vertex->normal * d.dot(vertex->normal);
			vertex->pos += d;
		}
	}
}

/// This structure is used to transmit mesh vertices across processors.
struct TriangleMeshVertexComm
{
	Vector3 pos;		// The world-space position of the vertex.
	AtomInteger tag;	// The unique tag of the vertex.
};

/// This structure is used to transmit mesh facets across processors.
struct TriangleMeshFacetComm
{
	AtomInteger vertices[3];	// The three vertices of the triangle facet.
};

/// This structure is used to transmit mesh edge information across processors.
struct TriangleMeshEdgeComm
{
	AtomInteger vertex1;
	AtomInteger vertex2;
	AtomInteger facet1vertex;
	AtomInteger facet2vertex;
};

/******************************************************************************
* Merges partial meshes from all processors into a single mesh.
******************************************************************************/
void TriangleMesh::mergeParallel(TriangleMesh& outputMesh)
{
	CALIB_ASSERT(outputMesh.vertices().empty() && outputMesh.facets().empty());

	// Pack local mesh nodes into buffer.
	vector<TriangleMeshVertexComm> vertexBuffer(_numLocalVertices);
	vector<TriangleMeshVertexComm>::iterator vertexItem = vertexBuffer.begin();
	for(vector<TriangleMeshVertex*>::const_iterator vertex = vertices().begin(); vertex != vertices().end(); ++vertex) {
		if(!(*vertex)->flags.test(TriangleMeshVertex::IS_GHOST_VERTEX)) {
			vertexItem->pos = (*vertex)->pos;
			vertexItem->tag = (*vertex)->tag;
			++vertexItem;
		}
	}
	CALIB_ASSERT(vertexItem == vertexBuffer.end());

	// Determine the total number of vertices.
	size_t totalVertexCount = parallel().reduce_sum(vertexBuffer.size());
	// Determine the maximum number of vertices per processor.
	size_t maxVertexCount = parallel().reduce_max(vertexBuffer.size());

	// Tag to vertex map.
	map<AtomInteger, TriangleMeshVertex*> tagToVertexMap;

	if(parallel().isMaster() == false) {
		// Send buffer to master processor.
		parallel().send(vertexBuffer, 0);
	}
	else {
		// As the master processor, receive data from other processors.
		size_t nread = vertexBuffer.size();
		outputMesh._vertices.reserve(totalVertexCount);

		for(int proc = 0; proc < parallel().processorCount(); proc++) {
			if(proc != 0) {
				vertexBuffer.resize(maxVertexCount);
				nread = parallel().receive(vertexBuffer, proc);
			}

			// Re-create nodes in the output mesh.
			for(vector<TriangleMeshVertexComm>::const_iterator vertexItem = vertexBuffer.begin(); vertexItem != vertexBuffer.begin() + nread; ++vertexItem) {
				TriangleMeshVertex* meshVertex = outputMesh.createVertex(vertexItem->pos, false, vertexItem->tag);
				bool newlyInserted = tagToVertexMap.insert(make_pair(vertexItem->tag, meshVertex)).second;

				// Tag must be unique:
				CALIB_ASSERT(newlyInserted);
			}
		}

		CALIB_ASSERT(outputMesh._numLocalVertices == outputMesh.vertices().size());
	}

	// Pack local mesh facets into buffer.
	vector<TriangleMeshFacetComm> facetBuffer(_numLocalFacets);
	vector<TriangleMeshFacetComm>::iterator facetItem = facetBuffer.begin();
	for(vector<TriangleMeshFacet*>::const_iterator facet = facets().begin(); facet != facets().end(); ++facet) {
		if(!(*facet)->flags.test(TriangleMeshFacet::IS_GHOST_FACET)) {
			for(int v = 0; v < 3; v++) {
				TriangleMeshEdge* e = (*facet)->edges[v];
				facetItem->vertices[v] = e->vertex1()->tag;
			}
			++facetItem;
		}
	}
	CALIB_ASSERT(facetItem == facetBuffer.end());

	// Determine the total number of facets.
	size_t totalFacetCount = parallel().reduce_sum(facetBuffer.size());
	// Determine the maximum number of facets per processor.
	size_t maxFacetCount = parallel().reduce_max(facetBuffer.size());

	if(parallel().isMaster() == false) {
		// Send buffer to master processor.
		parallel().send(facetBuffer, 0);
	}
	else {
		// As the master processor, receive data from other processors.
		size_t nread = facetBuffer.size();
		outputMesh._facets.reserve(totalFacetCount);

		for(int proc = 0; proc < parallel().processorCount(); proc++) {
			if(proc != 0) {
				facetBuffer.resize(maxFacetCount);
				nread = parallel().receive(facetBuffer, proc);
			}

			// Re-create facets in the output mesh.
			for(vector<TriangleMeshFacetComm>::const_iterator facetItem = facetBuffer.begin(); facetItem != facetBuffer.begin() + nread; ++facetItem) {
				TriangleMeshVertex* facetVertices[3];
				for(int v = 0; v < 3; v++) {
					facetVertices[v] = tagToVertexMap[facetItem->vertices[v]];
					CALIB_ASSERT(facetVertices[v] != NULL);
				}
				TriangleMeshEdge* facetEdges[3];
				for(int v = 0; v < 3; v++) {
					facetEdges[v] = outputMesh.createHalfEdge(facetVertices[v], facetVertices[(v+1)%3]);
				}
				outputMesh.createFacet(facetEdges);
			}
		}

		CALIB_ASSERT(outputMesh._numLocalFacets == outputMesh.facets().size());
	}

	// Pack local mesh edges into buffer.
	vector<TriangleMeshEdgeComm> edgeBuffer;
	for(vector<TriangleMeshVertex*>::const_iterator vertex = vertices().begin(); vertex != vertices().end(); ++vertex) {
		if((*vertex)->flags.test(TriangleMeshVertex::IS_GHOST_VERTEX))
			continue;

		for(TriangleMeshEdge* edge = (*vertex)->edges; edge != NULL; edge = edge->nextEdge) {
			TriangleMeshEdgeComm edgeItem;
			edgeItem.vertex1 = (*vertex)->tag;
			edgeItem.vertex2 = edge->vertex2->tag;
			if(edgeItem.vertex2 < edgeItem.vertex1) continue;
			edgeItem.facet1vertex = edge->facet->nextEdge(edge)->vertex2->tag;
			edgeItem.facet2vertex = edge->oppositeEdge->facet->nextEdge(edge->oppositeEdge)->vertex2->tag;
			edgeBuffer.push_back(edgeItem);
		}
	}

	// Determine the maximum number of edges per processor.
	size_t maxEdgeCount = parallel().reduce_max(edgeBuffer.size());

	// Transmit edges.
	if(parallel().isMaster() == false) {
		// Send buffer to master processor.
		parallel().send(edgeBuffer, 0);
	}
	else {
		// As the master processor, receive data from other processors.
		size_t nread = edgeBuffer.size();

		for(int proc = 0; proc < parallel().processorCount(); proc++) {
			if(proc != 0) {
				edgeBuffer.resize(maxEdgeCount);
				nread = parallel().receive(edgeBuffer, proc);
			}

			for(vector<TriangleMeshEdgeComm>::const_iterator edgeItem = edgeBuffer.begin(); edgeItem != edgeBuffer.begin() + nread; ++edgeItem) {

				TriangleMeshVertex* vertex1 = tagToVertexMap[edgeItem->vertex1];
				CALIB_ASSERT(vertex1 != NULL);

				TriangleMeshEdge* edge = vertex1->edges;
				while(edge != NULL) {
					if(edge->vertex2->tag == edgeItem->vertex2) {
						TriangleMeshVertex* thirdVertex = edge->facet->nextEdge(edge)->vertex2;
						if(thirdVertex->tag == edgeItem->facet1vertex) {
							CALIB_ASSERT(edge->oppositeEdge == NULL);

							TriangleMeshEdge* oppositeEdge = NULL;
							for(TriangleMeshEdge* otherEdge = edge->vertex2->edges; otherEdge != NULL; otherEdge = otherEdge->nextEdge) {
								if(otherEdge->vertex2 == vertex1) {
									TriangleMeshVertex* thirdVertex = otherEdge->facet->nextEdge(otherEdge)->vertex2;
									if(thirdVertex->tag == edgeItem->facet2vertex) {
										oppositeEdge = otherEdge;
										break;
									}
								}
							}
							CALIB_ASSERT(oppositeEdge != NULL);
							CALIB_ASSERT(oppositeEdge->oppositeEdge == NULL);

							edge->oppositeEdge = oppositeEdge;
							oppositeEdge->oppositeEdge = edge;
							break;
						}
					}
					edge = edge->nextEdge;
				}
				CALIB_ASSERT(edge != NULL);
			}
		}

#ifdef DEBUG_CRYSTAL_ANALYSIS
		for(vector<TriangleMeshFacet*>::const_iterator facet = outputMesh.facets().begin(); facet != outputMesh.facets().end(); ++facet) {
			for(int v = 0; v < 3; v++) {
				TriangleMeshEdge* edge = (*facet)->edges[v];
				CALIB_ASSERT(edge->oppositeEdge != NULL);
				CALIB_ASSERT(edge->oppositeEdge->oppositeEdge == edge);
			}
		}
#endif
	}
}

/******************************************************************************
* Computes the total surface area of the mesh.
******************************************************************************/
CAFloat TriangleMesh::computeTotalArea(const SimulationCell& cell) const
{
	CALIB_ASSERT(parallel().isMaster());

	CAFloat totalArea = 0;
	BOOST_FOREACH(const TriangleMeshFacet* facet, facets()) {
		Vector3 e1 = cell.wrapVector(facet->vertex(1)->pos - facet->vertex(0)->pos);
		Vector3 e2 = cell.wrapVector(facet->vertex(2)->pos - facet->vertex(0)->pos);
		totalArea += e1.cross(e2).norm();
	}
	return 0.5 * totalArea;
}

/******************************************************************************
* Creates a copy of this output mesh.
******************************************************************************/
TriangleMesh* TriangleMesh::clone(bool reverseOrientation)
{
	TriangleMesh* cloneMesh = new TriangleMesh(context());
	clone(*cloneMesh, reverseOrientation);
	return cloneMesh;
}

/******************************************************************************
* Creates a copy of this output mesh.
******************************************************************************/
void TriangleMesh::clone(TriangleMesh& outputMesh, bool reverseOrientation)
{
	CALIB_ASSERT(outputMesh.vertices().empty());
	CALIB_ASSERT(outputMesh.facets().empty());

	outputMesh._vertices.reserve(vertices().size());
	BOOST_FOREACH(TriangleMeshVertex* v, vertices()) {
		TriangleMeshVertex* vertex = outputMesh.createVertex(v->pos);
		CALIB_ASSERT(vertex->index == v->index);
	}

	outputMesh._facets.reserve(facets().size());

	int facetIndex = 0;
	BOOST_FOREACH(TriangleMeshFacet* f, facets()) {
		f->index = facetIndex++;
		TriangleMeshVertex* facetVertices[3];
		for(int v = 0; v < 3; v++)
			facetVertices[v] = outputMesh.vertices()[f->edges[v]->vertex1()->index];
		if(reverseOrientation)
			swap(facetVertices[0], facetVertices[2]);
		TriangleMeshEdge* facetEdges[3];
		for(int v = 0; v < 3; v++)
			facetEdges[v] = outputMesh.createHalfEdge(facetVertices[v], facetVertices[(v+1)%3]);
		outputMesh.createFacet(facetEdges);
	}

	for(vector<TriangleMeshFacet*>::const_iterator f = facets().begin(), f2 = outputMesh.facets().begin(); f != facets().end(); ++f, ++f2) {
		for(int e = 0; e < 3; e++) {
			CALIB_ASSERT((*f2)->edges[e]->oppositeEdge == NULL);
			CALIB_ASSERT((*f)->edges[e]->oppositeEdge != NULL);
			CALIB_ASSERT((*f)->edges[e]->oppositeEdge->facet != NULL);
			int e2 = reverseOrientation ? ((4-e) % 3) : e;
			int oppositeFacetIndex = (*f)->edges[e2]->oppositeEdge->facet->index;
			CALIB_ASSERT(oppositeFacetIndex >= 0 && oppositeFacetIndex <  outputMesh.facets().size());
			TriangleMeshFacet* adjacentFacet = outputMesh.facets()[oppositeFacetIndex];
			CALIB_ASSERT(adjacentFacet != *f2);
			CALIB_ASSERT(adjacentFacet->hasVertex((*f2)->vertex(e)));
			CALIB_ASSERT(adjacentFacet->hasVertex((*f2)->vertex((e+1)%3)));
			TriangleMeshEdge* oppositeEdge = adjacentFacet->edges[adjacentFacet->vertexIndex((*f2)->vertex((e+1)%3))];
			CALIB_ASSERT(oppositeEdge->vertex2 == (*f2)->vertex(e));
			(*f2)->edges[e]->oppositeEdge = oppositeEdge;
		}
	}
}

/******************************************************************************
* Performs a thorough check of the topology of the generated mesh.
* Raises an error if something wrong is found. Note that this should never happen.
******************************************************************************/
void TriangleMesh::validate(bool performManifoldCheck) const
{
	// Check if edges and facets are properly linked together.
	BOOST_FOREACH(TriangleMeshVertex* vertex, vertices()) {

		int numVertexEdges = 0;
		for(TriangleMeshEdge* edge = vertex->edges; edge != NULL; edge = edge->nextEdge) {

			TriangleMeshVertex* neighbor = edge->vertex2;
			if(edge->oppositeEdge == NULL)
				context().raiseErrorOne("Detected dangling half edge.");
			if(edge->oppositeEdge->oppositeEdge != edge)
				context().raiseErrorOne("Detected invalid reference between opposite edges.");
			if(edge->facet == NULL)
				context().raiseErrorOne("Detected open mesh.");

			TriangleMeshFacet* facet = edge->facet;
			bool found = false;
			for(int v = 0; v < 3; v++) {
				if(facet->edges[v]->oppositeEdge->vertex2 == vertex) {
					if(facet->edges[v] != edge)
						context().raiseErrorOne("Detected invalid reference from facet to edge.");
					found = true;
					break;
				}
			}
			if(!found)
				context().raiseErrorOne("Facet does not contain vertex to which it is incident.");
			numVertexEdges++;
		}

		if(performManifoldCheck) {
			// Go in positive direction around node, facet by facet.
			TriangleMeshEdge* currentEdge = vertex->edges;
			int numManifoldEdges = 0;
			do {
				CALIB_ASSERT(currentEdge->facet != NULL);
				currentEdge = currentEdge->facet->previousEdge(currentEdge)->oppositeEdge;
				numManifoldEdges++;
			}
			while(currentEdge != vertex->edges);

			if(numManifoldEdges != numVertexEdges) {
				context().raiseErrorOne("Triangle mesh node is part of multiple manifolds. Number of vertex edges: %i. Number of manifold edges: %i.", numVertexEdges, numManifoldEdges);
			}
		}
	}
}


}; // End of namespace
