#!/usr/bin/env python

# Creates a VRML file for rendering defect surfaces.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input simulation cell file and an output VRML file.")

def skipToKeyword(stream, keyword):
    while True:
        line = stream.readline()
        if line == "": raise IOError("Unexpected end of simulation cell file.")
        if line.startswith(keyword):
            return line

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")

if instream.readline() != "# vtk DataFile Version 3.0\n": raise IOError("Invalid VTK file.")
infoLine = instream.readline()
if infoLine.startswith("# Simulation cell") == False: raise IOError("Invalid simulation cell file.")    
dxaVersionString = infoLine.strip()
if instream.readline() != "ASCII\n" or instream.readline() != "DATASET UNSTRUCTURED_GRID\n": raise IOError("Invalid simulation cell file.")

# Open the output file.
outstream = stdout
if args[1] != '-': outstream = open(args[1], "w")

outstream.write("#VRML V2.0 utf8\n")
outstream.write("Group {\n")
outstream.write("  children [\n")
outstream.write("  Shape {\n")
outstream.write("appearance Appearance {\n")
outstream.write("material Material {\n")
outstream.write("ambientIntensity .075\n")
outstream.write("diffuseColor 1 1 1\n")
outstream.write("specularColor 1 1 1\n")
outstream.write("shininess .02\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("IndexedFaceSet {\n")

# Read in point coordinates
tokens = instream.readline().split()
if tokens[0] != "POINTS": raise IOError("Invalid simulation cell file.")
numPoints = int(tokens[1])

outstream.write("coord Coordinate {\n")
outstream.write("point [\n")

for i in range(numPoints):
    tokens = instream.readline().split()
    outstream.write("%s %s %s,\n" % (tokens[0], tokens[1], tokens[2]))
outstream.write("]\n")
outstream.write("}\n")

outstream.write("coordIndex [\n")

outstream.write("0 1 2 3 -1,\n")   
outstream.write("7 6 5 4 -1,\n")
outstream.write("0 4 5 1 -1,\n")
outstream.write("1 5 6 2 -1,\n")
outstream.write("2 6 7 3 -1,\n")
outstream.write("3 7 4 0 -1,\n")

outstream.write("]\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("]\n")
outstream.write("}\n")
outstream.close()
