///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __DIAMOND_COORDINATION_PATTERN_H
#define __DIAMOND_COORDINATION_PATTERN_H

#include "../cna/CNACoordinationPattern.h"

namespace CALib {

/**
 * Stores a pattern for a cubic or hexagonal diamond structure.
 */
struct DiamondCoordinationPattern : public CNACoordinationPattern
{
	/// Constructor.
	DiamondCoordinationPattern() : CNACoordinationPattern(COORDINATION_PATTERN_DIAMOND) {}

	/// Returns the maximum number of neighbors taken into account by this pattern.
	virtual int maxNeighbors() const { return 30; }

	int secondToFirstMap[4][3];
};

}; // End of namespace

#endif // __DIAMOND_COORDINATION_PATTERN_H

