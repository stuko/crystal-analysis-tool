///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructureParser.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"
#include "../../util/Timer.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Reads the atomic coordinates from the input file in compact binary format.
******************************************************************************/
void AtomicStructureParser::readCompactBinaryAtomsFile(TextParserStream& stream)
{
	context().msgLogger() << "Parsing compact binary atoms file." << endl;
	Timer loadTimer;

	if(_preprocessing.jitterMagnitude != 0)
		context().raiseErrorAll("Cannot apply random displacements to atoms when using spatially decomposed input file.");
	if(_preprocessing.atomsOffset != Vector3::Zero())
		context().raiseErrorAll("Cannot offset atomic positions when using spatially decomposed input file.");
	if(_preprocessing.replicate[0] != 1 || _preprocessing.replicate[1] != 1 || _preprocessing.replicate[2] != 1)
		context().raiseErrorAll("Cannot multiply simulation cell when using spatially decomposed input file.");

	int timestep = 0;
	boost::array<bool,3> pbc;
	AffineTransformation simulationCell(AffineTransformation::Identity());

	// Open input file in binary mode.
	ifstream fin;
	unsigned int magicCode;
	if(parallel().isMaster()) {
		fin.open(stream.filename().c_str(), ios_base::in | ios_base::binary);
		if(!fin.is_open())
			context().raiseErrorOne("Failed to open input file for reading.");

		fin.read((char*)&magicCode, sizeof(magicCode));
		if(magicCode != 0x87BD21 && magicCode != 0x87BD22)
			context().raiseErrorOne("Input file has invalid format.");

		int natoms;
		double xlo,xhi,ylo,yhi,zlo,zhi,xy,xz,yz;
		int xperiodic, yperiodic, zperiodic;

		fin.read((char*)&timestep, sizeof(timestep));
		fin.read((char*)&xlo, sizeof(xlo));
		fin.read((char*)&xhi, sizeof(xhi));
		fin.read((char*)&ylo, sizeof(ylo));
		fin.read((char*)&yhi, sizeof(yhi));
		fin.read((char*)&zlo, sizeof(zlo));
		fin.read((char*)&zhi, sizeof(zhi));
		fin.read((char*)&xy, sizeof(xy));
		fin.read((char*)&xz, sizeof(xz));
		fin.read((char*)&yz, sizeof(yz));
		fin.read((char*)&xperiodic, sizeof(xperiodic));
		fin.read((char*)&yperiodic, sizeof(yperiodic));
		fin.read((char*)&zperiodic, sizeof(zperiodic));
		fin.read((char*)&natoms, sizeof(natoms));
		_numInputAtoms = natoms;

		xlo -= min(min(min(xy, xz), xy+xz), 0.0);
		xhi -= max(max(max(xy, xz), xy+xz), 0.0);
		ylo -= min(yz, 0.0);
		yhi -= max(yz, 0.0);
		simulationCell.translation() = Vector3(xlo, ylo, zlo);
		simulationCell.linear().col(0) = Vector3(xhi - xlo, 0, 0);
		simulationCell.linear().col(1) = Vector3(xy, yhi - ylo, 0);
		simulationCell.linear().col(2) = Vector3(xz, yz, zhi - zlo);
		pbc[0] = (bool)xperiodic;
		pbc[1] = (bool)yperiodic;
		pbc[2] = (bool)zperiodic;
	}

	parallel().broadcast(_numInputAtoms);
	parallel().broadcast(timestep);
	_structure.setTimestep(timestep);

	setupSimulationCell(simulationCell, pbc);

	context().msgLogger() << "Reading " << _numInputAtoms << " atoms (frame " << timestep << ") from input file." << endl;

	beginDistributeAtoms();

	for(AtomInteger a = 0; a < _numInputAtoms; a++) {
		AtomInfo atomInfo;
		if(parallel().isMaster()) {
			if(fin.eof())
				context().raiseErrorOne("File parsing error. Unexpected end of file.");

			if(magicCode != 0x87BD22) {
				atomInfo.tag = a + 1;
				atomInfo.species = 1;
				float xyz[3];
				fin.read((char*)xyz, sizeof(xyz));
				atomInfo.pos.x() = xyz[0];
				atomInfo.pos.y() = xyz[1];
				atomInfo.pos.z() = xyz[2];
			}
			else {
				atomInfo.species = 1;
				fin.read((char*)&atomInfo.tag, sizeof(atomInfo.tag));
				fin.read((char*)&atomInfo.pos, sizeof(atomInfo.pos));
			}
		}

		distributeAtom(atomInfo);
	}

	endDistributeAtoms();

	context().msgLogger() << "File loading took " << loadTimer << "." << endl;
}

}; // End of namespace
