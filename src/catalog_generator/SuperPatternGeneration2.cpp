///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CatalogGenerator.h"
#include <calib/pattern/superpattern/SuperPatternMatcher.h>
#include <calib/pattern/superpattern/SuperPatternAnalysis.h>
#include <calib/cluster_graph/ClusterVector.h>
#include <calib/output/atoms/AtomicStructureWriter.h>

using namespace std;
using namespace CALib;

/******************************************************************************
* Create a graph pattern for a crystal defect.
******************************************************************************/
void PatternTemplate::generateDefectGraphPattern2(PatternCatalog& catalog, const CoordinationPatternAnalysis& coordPatternAnalysis)
{
	// First look for occurrences of known super patterns in the template structure.
	SuperPatternAnalysis superPatternAnalysis(coordPatternAnalysis);
	superPatternAnalysis.searchSuperPatterns(surroundingLatticePatterns);

	const NeighborList& neighborList = coordPatternAnalysis.neighborList();

	// Generate a new super pattern from all atoms which are not covered by one of the existing super patterns.
	for(int patternCount = 1; ; patternCount++) {
		vector<int> patternAtoms;
		// Find first atom that doesn't match to an existing super pattern.
		int seedAtomIndex = 0;
		for(; seedAtomIndex < structure().numTotalAtoms(); seedAtomIndex++) {

			// Skip atoms which should be ignored according to the user.
			if(isClipped(seedAtomIndex))
				continue;

			if(superPatternAnalysis.atomCluster(seedAtomIndex) == NULL) {
				context().msgLogger() << "Starting to build new pattern at atom " << structure().atomTag(seedAtomIndex) << ", which does not belong to any cluster so far." << endl;
				break;
			}
		}
		// Reached end of atoms list?
		if(seedAtomIndex == structure().numTotalAtoms()) {
			if(patternCount == 1)
				context().raiseErrorAll("Could not create a defect pattern from template structure '%s'. It contains no atoms that form a defect core.", templateName.c_str());
			break;
		}

		// Recursively gather all neighboring atoms which also don't match an existing super pattern.
		deque<int> toBeProcessed(1, seedAtomIndex);
		do {
			int currentAtom = toBeProcessed.front();
			toBeProcessed.pop_front();

			if(isClipped(currentAtom))
				context().raiseErrorAll("Candidate atom for a new pattern is in the clipped region (current atom tag %i, seed atom tag %i). Please check the clipping region.", structure().atomTag(currentAtom), structure().atomTag(seedAtomIndex));

			patternAtoms.push_back(currentAtom);

			BOOST_FOREACH(NeighborList::neighbor_info neigh, neighborList.neighbors(currentAtom)) {
				if(neigh.distsq > innerCutoff * innerCutoff)
					continue;
				if(superPatternAnalysis.atomCluster(neigh.index) != NULL)
					continue;
				if(boost::find(patternAtoms, neigh.index) == patternAtoms.end() &&
						boost::find(toBeProcessed, neigh.index) == toBeProcessed.end()) {
					toBeProcessed.push_back(neigh.index);
				}
			}
		}
		while(!toBeProcessed.empty());

		int numCoreNodes = patternAtoms.size();

		// For crystal interfaces, these matrices store the misorientations on either side of the interface.
		Matrix3 T_A(Matrix3::Identity());	// Transformation from the GB frame to grain A.
		Matrix3 T_B(Matrix3::Identity());	// Transformation from the GB frame to grain B.
		Cluster* grainA = NULL;
		Cluster* grainB = NULL;

		// Now add an extra layer of atoms to the pattern, which overlap with the surrounding perfect crystal.
		set<SuperPattern const*> parentLatticePatterns;
		for(int nodeIndex = 0; nodeIndex < numCoreNodes; nodeIndex++) {
			int currentAtom = patternAtoms[nodeIndex];

			BOOST_FOREACH(NeighborList::neighbor_info neigh, neighborList.neighbors(currentAtom)) {
				if(neigh.distsq > innerCutoff * innerCutoff)
					continue;
				if(superPatternAnalysis.atomCluster(neigh.index) == NULL)
					continue;
				if(boost::find(patternAtoms, neigh.index) != patternAtoms.end())
					continue;

				Cluster* latticeCluster = superPatternAnalysis.atomCluster(neigh.index);
				const SuperPattern& latticePattern = superPatternAnalysis.atomPattern(neigh.index);
				const SuperPatternNode& latticeNode = superPatternAnalysis.atomPatternNode(neigh.index);
				boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> neighborCoordinationPatternMapping = coordPatternAnalysis.findCoordinationPatternMatch(neigh.index, *latticeNode.coordinationPattern);
				CALIB_ASSERT(neighborCoordinationPatternMapping);

				if(latticePattern.isLattice()) {
					FrameTransformation ftm;
					for(int n2 = 0; n2 < latticeNode.coordinationPattern->numNeighbors(); n2++) {
						int atomNeighborIndex = superPatternAnalysis.nodeNeighborToAtomNeighbor(neigh.index, n2);
						const Vector3 lattice_vector = latticeNode.neighbors[n2].referenceVector;
						const Vector3 gb_vector = unrelaxedNeighborVector(neigh.index, atomNeighborIndex);
						ftm.addVector(gb_vector, lattice_vector);
					}
					if(grainA == NULL) {
						T_A = ftm.computeTransformation();
						grainA = latticeCluster;
					}
					else if(latticeCluster == grainA) {
						if((T_A - ftm.computeTransformation()).isZero(CA_TRANSITION_MATRIX_EPSILON) == false) {
							context().raiseErrorAll("Elastic field incompatibility detected at interface between the new defect pattern and the parent lattice cluster '%s' at atom %i.",
								latticePattern.name.c_str(), structure().atomTag(neigh.index));
						}
					}
					else if(grainB == NULL) {
						T_B = ftm.computeTransformation();
						grainB = latticeCluster;
					}
					else if(latticeCluster == grainB) {
						if((T_B - ftm.computeTransformation()).isZero(CA_TRANSITION_MATRIX_EPSILON) == false) {
							context().raiseErrorAll("Elastic field incompatibility detected at interface between the new defect pattern and the parent lattice cluster '%s' at atom %i.",
								latticePattern.name.c_str(), structure().atomTag(neigh.index));
						}
					}

					patternAtoms.push_back(neigh.index);
					parentLatticePatterns.insert(&latticePattern);
				}
			}
		}

		context().msgLogger() << "Gathered " << numCoreNodes << " core atoms and " << (patternAtoms.size() - numCoreNodes) << " overlap atoms." << endl;


		// Create the new super pattern.
//		SuperPattern* superPattern = createGraphPattern(patternAtoms, coordMappings, numCoreNodes, catalog, superPatternAnalysis).release();

		context().raiseErrorAll("STOPPED");

	}
}
