///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CNACoordinationPatternMatcher.h"
#include "CNACoordinationPattern.h"
#include "../../../context/CAContext.h"

using namespace std;

namespace CALib {

bool CNACoordinationPatternMatcher::initializeNeighborPermutations()
{
	CALIB_ASSERT(atomIndex() >= 0 && atomIndex() < structure().numLocalAtoms());
	CALIB_ASSERT(neighborList().isSorted());

	// Check for sufficient number of neighbors.
	int nn = pattern().numNeighbors();
	if(neighborList().neighborCount(atomIndex()) < nn)
		return false;

	// Get current cutoff radius.
	CAFloat cutoffSquared = square(cutoff());

	// Make sure the N-th atom is within the cutoff radius.
	if(neighborList().neighborDistanceSquared(atomIndex(), nn-1) > cutoffSquared)
		return false;

	// Make sure the (N+1)-th atom is beyond the cutoff radius (if it exists).
	if(neighborList().neighborCount(atomIndex()) > nn && neighborList().neighborDistanceSquared(atomIndex(), nn) <= cutoffSquared)
		return false;

	// Compute bond bit-flag array.
	NeighborList::neighbor_iterator n1 = neighborList().neighbors(atomIndex()).begin();
	for(int ni1 = 0; ni1 < nn; ni1++, ++n1) {
		_neighborArray.setNeighborBond(ni1, ni1, false);
		NeighborList::neighbor_iterator n2 = n1 + 1;
		for(int ni2 = ni1 + 1; ni2 < nn; ni2++, ++n2) {
			bool bonded = (n2->delta - n1->delta).squaredNorm() < cutoffSquared;
			_neighborArray.setNeighborBond(ni1, ni2, bonded);
		}
	}

	// Compute CNA signatures.
	CNABond cnaSignaturesSorted[CA_MAX_PATTERN_NEIGHBORS];
	for(int ni = 0; ni < nn; ni++) {
		CNACoordinationPattern::calculateCNASignature(_neighborArray, ni, _cnaSignatures[ni], nn);
		cnaSignaturesSorted[ni] = _cnaSignatures[ni];
	}
	sort(cnaSignaturesSorted, cnaSignaturesSorted + nn);

	// Check if sorted CNA signatures are identical.
	const CNACoordinationPattern& cnaPattern = static_cast<const CNACoordinationPattern&>(pattern());
	for(int ni = 0; ni < nn; ni++) {
		if(cnaSignaturesSorted[ni] != cnaPattern.cnaNeighborsSorted[ni])
			return false;
	}

	// Reset permutation.
	_previousIndices.assign(nn, -1);
	_atomNeighborIndices.resize(nn);
	for(int n = 0; n < nn; n++)
		_atomNeighborIndices[n] = n;

	// Find first matching permutation.
	for(;;) {
		if(validatePermutation())
			return true;
		bitmapSort(_atomNeighborIndices.begin() + _validUpTo + 1, _atomNeighborIndices.end(), nn);
		if(!boost::next_permutation(_atomNeighborIndices))
			break;
	}

	return false;
}

bool CNACoordinationPatternMatcher::nextNeighborPermutation()
{
	for(;;) {
		if(!boost::next_permutation(_atomNeighborIndices))
			break;
		if(validatePermutation())
			return true;
		bitmapSort(_atomNeighborIndices.begin() + _validUpTo + 1, _atomNeighborIndices.end(), _atomNeighborIndices.size());
	}
	return false;
}

bool CNACoordinationPatternMatcher::validatePermutation()
{
	// Validate interconnectivity.
	bool check = false;
	const CNACoordinationPattern& cnaPattern = static_cast<const CNACoordinationPattern&>(pattern());
	for(int ni1 = 0; ni1 < _atomNeighborIndices.size(); ni1++) {
		_validUpTo = ni1;
		int atomNeighborIndex1 = _atomNeighborIndices[ni1];
		if(atomNeighborIndex1 != _previousIndices[ni1]) check = true;
		_previousIndices[ni1] = atomNeighborIndex1;
		if(check) {
			if(_cnaSignatures[atomNeighborIndex1] != cnaPattern.cnaNeighbors[ni1])
				return false;
			for(int ni2 = 0; ni2 < ni1; ni2++) {
				int atomNeighborIndex2 = _atomNeighborIndices[ni2];
				if(_neighborArray.neighborBond(atomNeighborIndex1, atomNeighborIndex2) != cnaPattern.neighborArray.neighborBond(ni1, ni2))
					return false;
			}
		}
	}

	_validUpTo = _atomNeighborIndices.size();
	return true;
}

}; // End of namespace
