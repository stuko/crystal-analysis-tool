///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include <calib/CALib.h>
#include <calib/util/Timer.h>
#include "CrystalAnalysisTool.h"

using namespace CALib;
using namespace std;

int main(int argc, char* argv[])
{
	try {
		CrystalAnalysisTool catool;

		// Check if program has been invoked without parameter.
		// In this case just print usage instructions and quit.
		if(argc == 1) {
			catool.params().printHelp(std::cout);
			return 1;
		}

		// Set the locale to the standard "C" locale, which is independent of the user's system settings.
		// This is needed to parse numbers with a decimal dot on non-english systems.
		locale::global(locale::classic());

		// Initialize MPI etc.
		if(!catool.initialize(argc, argv))
			return 1;

		// Parse command line parameters.
		if(!catool.params().parseCommandLine(argc, argv, catool.context().msgLogger()))
			return 1;

		// Validate command line parameters.
		catool.validateParameters();

		if(catool.params().allprocOutput)
			catool.context().setAllProcOutput(true);

		// Print banner.
		if(catool.context().isMaster()) {
			std::cout << "Crystal Analysis Tool" << endl;
			std::cout << "Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)" << endl;
			std::cout << "Version: " << CA_LIB_VERSION_STRING << endl;
		}

		if(catool.params().identifyAtomicStructures) {
			// Load pattern catalog.
			catool.loadPatterns();
		}

		Timer analysisTimer;

		// Load input structure.
		catool.loadInputStructure(catool.inputConfiguration(), catool.params().inputFile, true);

		// Dump simulation cell shape to file.
		if(catool.params().cellOutputFile.empty() == false)
			catool.writeCellToFile(catool.inputConfiguration(), catool.params().cellOutputFile);

		// Generate free surface mesh.
		if(catool.params().generateFreeSurface) {
			catool.generateFreeSurfaceMesh(catool.inputConfiguration());

			if(catool.params().freeSurfaceRawOutputFile.empty() == false) {
				// Output original (not smoothed, not wrapped) surface mesh to file.
				catool.writeRawFreeSurfaceToFile(catool.params().freeSurfaceRawOutputFile);
			}

			// Dump free surface mesh to output file.
			if(catool.params().freeSurfaceOutputFile.empty() == false) {
				// Finish free surface mesh for output.
				catool.prepareFreeSurfaceForVisualization();

				catool.writeFreeSurfaceToFile(catool.params().freeSurfaceOutputFile);

				// Dump free surface cap to output file.
				if(catool.params().freeSurfaceCapOutputFile.empty() == false)
					catool.writeFreeSurfaceCapToFile(catool.params().freeSurfaceCapOutputFile);
			}
		}

		if(catool.params().identifyAtomicStructures) {

			// Perform structure analysis on input configuration.
			catool.performStructureAnalysis(catool.inputConfiguration());

			// Decompose polycrystal into individual grains.
			if(catool.params().identifyGrains) {
				catool.performGrainAnalysis(catool.inputConfiguration());
				if(catool.params().grainsOutputFile.empty() == false)
					catool.outputGrains(catool.params().grainsOutputFile);
			}

			// Dump clusters to output file.
			if(catool.params().clustersOutputFile.empty() == false)
				catool.writeClustersToFile(catool.inputConfiguration(), catool.params().clustersOutputFile);
		}

		// Load second configuration for deformation field analysis.
		if(catool.params().deformationAnalysisMode) {

			catool.context().msgLogger() << "================= Reading deformed structure  ==================" << endl;

			// When elimination of homogeneous deformation is enabled,
			// The cell of the deformed structure (including all atoms) is transformed
			// such that it exactly matches the reference cell before the deformation
			// analysis is performed.
			if(catool.params().eliminateHomogeneousDeformation) {
				catool.params().preprocessing.cellShape = catool.inputConfiguration().structure.simulationCellMatrix().linear();
			}

			// Load deformed structure.
			catool.loadInputStructure(catool.deformedConfiguration(), catool.params().deformedConfigurationFile, false);
		}

		// Dump processed atoms to output file.
		if(catool.params().atomsOutputFile.empty() == false)
			catool.writeAtomsToFile(catool.inputConfiguration(), catool.params().atomsOutputFile);

#if 0
		// For debugging purposes, dump each processor's atoms to a separate file.
		ostringstream localAtomsFilename;
		localAtomsFilename << "atoms_proc." << catool.context().processor() << ".dump";
		catool.writeLocalAtomsToFile(catool.inputConfiguration(), localAtomsFilename.str());
#endif

		if(catool.params().generateTessellation) {

			// Generate the Delaunay tessellation.
			catool.generateTessellation();

			if(catool.params().generateElasticMapping) {

				// Compute elastic mapping from physical to virtual relaxed configuration.
				bool onlyLocalTetrahedra = !catool.params().computeElasticPlasticFields;
				bool synchronizeProcessors = catool.params().generateInterfaceMesh;
				catool.computeElasticMapping(catool.inputConfiguration(), onlyLocalTetrahedra, synchronizeProcessors);

				if(catool.params().generateInterfaceMesh) {

					// Generate the interface mesh needed for dislocation extraction.
					catool.buildInterfaceMesh();

					// Dump mesh to output file.
					if(catool.params().meshOutputFile.empty() == false)
						catool.writeMeshToFile(catool.params().meshOutputFile);

					if(catool.params().identifyDislocations) {
						// Identify dislocation segments.
						catool.identifyDislocations();
					}

					if(catool.params().generateDefectSurface) {
						// Generate defect surface mesh.
						catool.generateDefectSurface();
					}

					// Write analysis results to output file.
					if(catool.params().crystalAnalysisOutputFile.empty() == false)
						catool.writeAnalysisResultsToFile(catool.params().crystalAnalysisOutputFile);

					if(catool.params().generateDefectSurface) {
						// Finish defect surface for output.
						catool.prepareDefectSurfaceForVisualization();

						// Dump defect surface to output file.
						if(catool.params().surfaceOutputFile.empty() == false)
							catool.writeDefectSurfaceToFile(catool.params().surfaceOutputFile);

						// Dump defect surface cap to output file.
						if(catool.params().surfaceCapOutputFile.empty() == false)
							catool.writeDefectSurfaceCapToFile(catool.params().surfaceCapOutputFile);
					}

					if(catool.params().identifyDislocations) {

						// Write dislocation junctions to output file.
						if(catool.params().junctionsOutputFile.empty() == false)
							catool.outputJunctions(catool.params().junctionsOutputFile);

						// Finish dislocation lines for output.
						catool.prepareDislocationsForVisualization();

						// Write dislocation lines to output file.
						if(catool.params().dislocationsOutputFile.empty() == false)
							catool.writeDislocationsToFile(catool.params().dislocationsOutputFile);

						// Calculate total dislocation line length.
						catool.outputDislocationInformation();
					}
				}

				// Perform elastic field analysis.
				if(catool.params().computeElasticField) {

					// Compute elastic deformation gradient field.
					catool.computeElasticField();

					// Write elastic field to output file.
					if(catool.params().elasticFieldOutputFile.empty() == false)
						catool.writeElasticFieldToFile(catool.params().elasticFieldOutputFile);
				}

				// Perform deformation field analysis.
				if(catool.params().computeDeformationField) {

					catool.context().msgLogger() << "=============== Processing deformed structure  ==================" << endl;

					if(catool.params().computeElasticPlasticFields) {
						// Perform structure analysis on deformed configuration.
						catool.performStructureAnalysis(catool.deformedConfiguration());
					}

					catool.context().msgLogger() << "=============== Performing deformation field analysis ==================" << endl;

					// Compute total deformation gradient field.
					catool.computeDeformationField();

					// Perform elastic-plastic decomposition.
					if(catool.params().computeElasticPlasticFields) {

						// Compute elastic mapping from physical to virtual relaxed configuration.
						catool.deformedConfiguration().elasticMapping.setVertexToAtomMapping(catool.deformationField().initialToFinalMap());
						catool.computeElasticMapping(catool.deformedConfiguration(), false, false);

						// Compute elastic and plastic deformation gradient fields.
						catool.computeElasticPlasticFields();
					}

					// Write deformation field to output file.
					if(catool.params().deformationFieldOutputFile.empty() == false)
						catool.writeDeformationFieldToFile(catool.params().deformationFieldOutputFile);

					// Output results.
					catool.outputMacroscopicDeformationGradients();
				}
			}
		}

		catool.context().msgLogger() << "Total analysis time: " << analysisTimer << "." << endl;
	}
	catch(const std::bad_alloc& ex) {
		cerr << endl << "ERROR: Out of memory" << endl << flush;
		return 1;
	}
	catch(const std::exception& ex) {
		cout << flush;
		cerr << endl << "ERROR: " << ex.what() << endl << flush;
		return 1;
	}

	return 0;
}
