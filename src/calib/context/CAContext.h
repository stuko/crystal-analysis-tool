///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_CONTEXT_H
#define __CRYSTAL_ANALYSIS_CONTEXT_H

#include "../CALib.h"
#include "../util/FixedCapacityVector.h"

namespace CALib {

/*
 * Global object that provides basic parallelization routines, error handling
 * and message logging functions.
 */
class CAContext
{
public:

	/******************************** Initialization **************************************/

	/// Default constructor.
	CAContext();

	/// Destructor.
	virtual ~CAContext();

#ifdef CALIB_USE_MPI
	/// Initializes the main object.
	virtual void initContext(int argc, char* argv[], bool parallelMode, MPI_Comm communicator = MPI_COMM_WORLD);
#else
	/// Initializes the main object.
	virtual void initContext(int argc, char* argv[], bool parallelMode);
#endif

	/******************************** Error handling **************************************/

	/// Aborts the analysis and reports an error before exiting the program.
	/// This function must be called by all processors simultaneously.
	void raiseErrorAll(const char* errorFormatString, ...);

	/// Aborts the analysis and reports an error before exiting the program.
	/// This function can be called by a single processor.
	void raiseErrorOne(const char* errorFormatString, ...);

	/************************************ Logging ****************************************/

	/// Sets the output stream to which log messages are sent.
	void setMsgLogger(std::ostream& stream) { _msgLogger = &stream; }

	/// Returns the output stream to which log messages are sent.
	std::ostream& msgLogger() const;

	/// Returns whether log messages from all processors are sent to the console.
	bool allProcOutput() const { return _allProcessorsOutput; }

	/// Controls whether log messages from all processors are sent to the console.
	void setAllProcOutput(bool enable);

	/******************************** Parallelization ************************************/

	/// Returns whether we are using more than one processor.
	bool isParallelMode() const { return _parallelMode; }

	/// Returns the number of processors in a spatial dimension.
	inline int processorGrid(unsigned int dim) const { return _processorGrid[dim]; }

	/// Returns the grid location of the current processor in a spatial dimension.
	inline int processorLocation(unsigned int dim) const { return _processorLocation[dim]; }

	/// Returns the grid location of the current processor.
	inline const Vector3I& processorLocationPoint() const { return _processorLocation; }

	/// Assigns spatial domains to the available processors.
	void setupProcessorGrid(const Matrix3& simulationCell);

#ifdef CALIB_USE_MPI

	/// Returns true if this is the master processor.
	inline bool isMaster() const { return _processorMe == 0; }

	/// Returns the processor ID.
	inline int processor() const { return _processorMe; }

	/// Returns the total number of processors in use.
	inline int processorCount() const { return _processorCount; }

	/// Returns the global MPI communicator.
	inline const MPI_Comm& communicator() const { return _mpiComm; }

	/// Returns the rank of the neighboring processor in the given dimension.
	inline int processorNeighbor(unsigned int dim, unsigned int dir) const { return _processorNeighbors[dim][dir]; }

	/// Returns the rank of a more distant processor.
	int processorStrideNeighbor(unsigned int dim, unsigned int dir, unsigned int stride) const {
		CALIB_ASSERT(stride < (unsigned int)processorGrid(dim));
		return _processorStrideNeighbors[dim][dir][stride];
	}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(int& v) const { MPI_Bcast(&v, 1, MPI_INT, 0, communicator()); }

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(size_t& v) const { MPI_Bcast(&v, 1, (sizeof(v) == 4) ? MPI_UNSIGNED : MPI_UNSIGNED_LONG, 0, communicator()); }

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(float& v) const { MPI_Bcast(&v, 1, MPI_FLOAT, 0, communicator()); }

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(double& v) const { MPI_Bcast(&v, 1, MPI_DOUBLE, 0, communicator()); }

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(bool& v) const { int vint = v; MPI_Bcast(&vint, 1, MPI_INT, 0, communicator()); v = (bool)vint; }

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(Vector3& v) const { MPI_Bcast(v.data(), 3, MPI_FLOATTYPE, 0, communicator()); }

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(Matrix3& m) const { CALIB_STATIC_ASSERT(sizeof(m) == 9*sizeof(CAFloat)); MPI_Bcast(m.data(), 9, MPI_FLOATTYPE, 0, communicator()); }

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(AffineTransformation& tm) const { CALIB_STATIC_ASSERT(sizeof(tm) == 12*sizeof(CAFloat)); MPI_Bcast(tm.data(), 12, MPI_FLOATTYPE, 0, communicator()); }

	/// Broadcasts an array of boolean values to all processors.
	template<size_t N>
	inline void broadcast(boost::array<bool,N>& a) const { MPI_Bcast(&a, sizeof(a), MPI_CHAR, 0, communicator()); }

	/// Broadcasts an STL vector to all processors.
	template<typename T>
	inline void broadcastWithSize(std::vector<T>& v) const {
		size_t count = v.size();
		broadcast(count);
		v.resize(count);
		if(count)
			broadcast(&v.front(), count * sizeof(T));
	}

	/// Broadcasts a STL string to all processors.
	inline void broadcast(std::string& s) const {
		size_t length = s.size();
		broadcast(length);
		if(!length) { s.clear(); return; }
		std::vector<char> buffer(length);
		if(isMaster())
			broadcast((void*)s.data(), length);
		else {
			broadcast(&buffer[0], length);
			s.assign(buffer.begin(), buffer.end());
		}
	}

	/// Broadcasts a data array of known size to all processors.
	inline void broadcast(void* data, unsigned int size) const { MPI_Bcast(data, size, MPI_CHAR, 0, communicator()); }

	/// Performs a sum-reduce operation over all processors.
	inline int allreduce_sum(int localValue) const { int totalValue; MPI_Allreduce(&localValue, &totalValue, 1, MPI_INT, MPI_SUM, communicator()); return totalValue; }

	/// Performs a sum-reduce operation over all processors.
	inline int reduce_sum(int localValue) const { int totalValue; MPI_Reduce(&localValue, &totalValue, 1, MPI_INT, MPI_SUM, 0, communicator()); return totalValue; }

	/// Performs a sum-reduce operation over all processors.
	inline size_t reduce_sum(size_t localValue) const {
		size_t totalValue;
		if(sizeof(size_t) == 4) MPI_Reduce(&localValue, &totalValue, 1, MPI_UNSIGNED, MPI_SUM, 0, communicator());
#ifdef MPI_UNSIGNED_LONG_LONG
		else if(sizeof(size_t) == 8) MPI_Reduce(&localValue, &totalValue, 1, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, communicator());
#else
		else if(sizeof(size_t) == 8) MPI_Reduce(&localValue, &totalValue, 1, MPI_LONG_LONG, MPI_SUM, 0, communicator());
#endif
		else CALIB_ASSERT(false);
		return totalValue;
	}

	/// Performs a sum-reduce operation over all processors.
	inline CAFloat reduce_sum(CAFloat localValue) const { CAFloat totalValue; MPI_Reduce(&localValue, &totalValue, 1, MPI_FLOATTYPE, MPI_SUM, 0, communicator()); return totalValue; }

	/// Performs a sum-reduce operation over all processors.
	inline Matrix3 reduce_sum(Matrix3& localValue) const { Matrix3 totalValue; CALIB_ASSERT(sizeof(Matrix3) == 9*sizeof(CAFloat)); MPI_Reduce(localValue.data(), totalValue.data(), 9, MPI_FLOATTYPE, MPI_SUM, 0, communicator()); return totalValue; }

	/// Performs a sum-reduce operation over all processors.
	template<size_t N>
	inline boost::array<AtomInteger,N> reduce_sum(boost::array<AtomInteger,N>& localValue) const { boost::array<AtomInteger,N> totalValue; MPI_Reduce(&localValue, &totalValue, N, MPI_ATOMINT, MPI_SUM, 0, communicator()); return totalValue; }

	/// Performs a max-reduce operation over all processors.
	inline int allreduce_max(int localValue) const { int totalValue; MPI_Allreduce(&localValue, &totalValue, 1, MPI_INT, MPI_MAX, communicator()); return totalValue; }

	/// Performs a max-reduce operation over all processors.
	inline CAFloat allreduce_max(CAFloat localValue) const { CAFloat globalValue; MPI_Allreduce(&localValue, &globalValue, 1, MPI_FLOATTYPE, MPI_MAX, communicator()); return globalValue; }

	/// Performs a max-reduce operation over all processors.
	inline void allreduce_max(const CAFloat* localValues, CAFloat* globalValues, size_t count) const { MPI_Allreduce(const_cast<CAFloat*>(localValues), globalValues, count, MPI_FLOATTYPE, MPI_MAX, communicator()); }

	/// Performs a min-reduce operation over all processors.
	inline CAFloat allreduce_min(CAFloat localValue) const { CAFloat globalValue; MPI_Allreduce(&localValue, &globalValue, 1, MPI_FLOATTYPE, MPI_MIN, communicator()); return globalValue; }

	/// Performs a min-reduce operation over all processors.
	inline void allreduce_min(const CAFloat* localValues, CAFloat* globalValues, size_t count) const { MPI_Allreduce(const_cast<CAFloat*>(localValues), globalValues, count, MPI_FLOATTYPE, MPI_MIN, communicator()); }

	/// Performs a max-reduce operation over all processors.
	inline int reduce_max(int localValue) const { int totalValue; MPI_Reduce(&localValue, &totalValue, 1, MPI_INT, MPI_MAX, 0, communicator()); return totalValue; }

	/// Waits for all processors to reach the barrier.
	inline void barrier() const { MPI_Barrier(communicator()); }

	/// Returns whether the current processor should exchange data with the neighbor in the given direction.
	inline bool shouldSendOrReceive(unsigned int dim, unsigned int dir, bool pbc) const {
		return pbc ||
			(dir == 0 && processorLocation(dim) != 0) ||
			(dir == 1 && processorLocation(dim) != processorGrid(dim)-1);
	}

	/// Performs data exchange with neighboring processors.
	void exchange(unsigned int dim, unsigned int dir, bool pbc, void* sendBuffer, int sendCount, void* receiveBuffer, int receiveCount, MPI_Datatype dataType) const;

	/// Performs data exchange with neighboring processors.
	void exchange(unsigned int dim, unsigned int dir, bool pbc, int sendValue, int& receiveValue) const {
		exchange(dim, dir, pbc, &sendValue, 1, &receiveValue, 1, MPI_INT);
	}

	/// Performs data exchange with neighboring processors.
	void exchange(unsigned int dim, unsigned int dir, bool pbc, unsigned int sendValue, unsigned int& receiveValue) const {
		exchange(dim, dir, pbc, &sendValue, 1, &receiveValue, 1, MPI_UNSIGNED);
	}

	/// Performs data exchange with neighboring processors.
	template<typename T>
	void exchange(unsigned int dim, unsigned int dir, bool pbc, std::vector<T>& sendBuffer, std::vector<T>& receiveBuffer) const {
		exchange(dim, dir, pbc, sendBuffer.empty() ? NULL : &sendBuffer.front(), sendBuffer.size() * sizeof(T), receiveBuffer.empty() ? NULL : &receiveBuffer.front(), receiveBuffer.size() * sizeof(T), MPI_CHAR);
	}

	/// Performs data exchange with neighboring processors.
	template<typename T>
	void exchangeWithSize(unsigned int dim, unsigned int dir, bool pbc, std::vector<T>& sendBuffer, std::vector<T>& receiveBuffer) const {
		// First exchange number of elements to be transmitted.
		int receiveBufferSize = 0;
		exchange(dim, dir, pbc, sendBuffer.size(), receiveBufferSize);
		// Allocate buffer to accommodate data to be received.
		receiveBuffer.resize(receiveBufferSize);
		// Transmit actual data array.
		exchange(dim, dir, pbc, sendBuffer, receiveBuffer);
	}

	/// Sends a STL vector to another processor.
	template<typename T>
	void send(const std::vector<T>& sendBuffer, int destinationProc) const {
		MPI_Send(sendBuffer.empty() ? NULL : const_cast<T*>(&sendBuffer.front()),
				sizeof(T) * sendBuffer.size(), MPI_CHAR,
				destinationProc, 0, communicator());
	}

	/// Sends a STL vector to another processor.
	template<typename T>
	void sendWithSize(const std::vector<T>& sendBuffer, int destinationProc) const {
		// First, send number of items to be transmitted.
		int numItems = sendBuffer.size();
		MPI_Send(&numItems, 1, MPI_INT, destinationProc, 0, communicator());
		// Then send actual data.
		if(numItems != 0) {
			MPI_Send(sendBuffer.empty() ? NULL : const_cast<T*>(&sendBuffer.front()),
					sizeof(T) * sendBuffer.size(), MPI_CHAR,
					destinationProc, 0, communicator());
		}
	}

	/// Receives a STL vector from another processor.
	/// Returns number of received elements.
	template<typename T>
	size_t receive(std::vector<T>& receiveBuffer, int sourceProc, size_t offset = 0) const {
		MPI_Status status;
		MPI_Recv(receiveBuffer.empty() ? NULL : &receiveBuffer[offset],
				sizeof(T) * (receiveBuffer.size() - offset), MPI_CHAR,
				sourceProc, 0, communicator(), &status);
		int receivedBytes;
		MPI_Get_count(&status, MPI_CHAR, &receivedBytes);
		return receivedBytes / sizeof(T);
	}

	/// Receives a STL vector from another processor.
	/// Returns number of received elements.
	template<typename T>
	size_t receiveWithSize(std::vector<T>& receiveBuffer, int sourceProc) const {
		// Receive number of items to be received.
		MPI_Status status;
		int numItemsToReceive;
		MPI_Recv(&numItemsToReceive, 1, MPI_INT, sourceProc, 0, communicator(), &status);
		// Allocate receive buffer.
		receiveBuffer.resize(numItemsToReceive);
		// Receive actual data.
		if(numItemsToReceive != 0) {
			MPI_Recv(&receiveBuffer.front(),
					sizeof(T) * numItemsToReceive, MPI_CHAR,
					sourceProc, 0, communicator(), &status);
		}
		return numItemsToReceive;
	}

#else

	/// Returns true if this is the master processor.
	inline bool isMaster() const { return true; }

	/// Returns the processor ID.
	inline int processor() const { return 0; }

	/// Returns the total number of processors in use.
	inline int processorCount() const { return 1; }

	/// Returns the rank of the neighboring processor in the given dimension.
	inline int processorNeighbor(unsigned int dim, unsigned int dir) const { return 0; }

	/// Returns the rank of a more distant processor.
	int processorStrideNeighbor(unsigned int dim, unsigned int dir, unsigned int stride) const {
		CALIB_ASSERT(stride < processorGrid(dim));
		return 0;
	}

	/// Sets the decomposition grid in pseudo-parallel mode.
	void setupPseudoProcessorGrid(int dimensions[3], const Vector3I& mylocation);

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(int& v) const {}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(size_t& v) const {}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(float& v) const {}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(double& v) const {}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(bool& v) const {}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(Vector3& v) const {}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(Matrix3& m) const {}

	/// Broadcasts the value of a variable to all processors.
	inline void broadcast(AffineTransformation& tm) const {}

	/// Broadcasts an array of boolean values to all processors.
	template<size_t N>
	inline void broadcast(boost::array<bool,N>& a) const {}

	/// Broadcasts a STL vector to all processors.
	template<typename T>
	inline void broadcastWithSize(std::vector<T>& v) const {}

	/// Broadcasts a STL string to all processors.
	inline void broadcast(std::string& s) const {}

	/// Broadcasts data array to all processors.
	inline void broadcast(void* data, int size) const {}

	/// Performs a sum-reduce operation over all processors.
	inline int allreduce_sum(int localValue) const { return localValue; }

	/// Performs a sum-reduce operation over all processors.
	inline int reduce_sum(int localValue) const { return localValue; }

	/// Performs a sum-reduce operation over all processors.
	inline size_t reduce_sum(size_t localValue) const { return localValue; }

	/// Performs a sum-reduce operation over all processors.
	inline CAFloat reduce_sum(CAFloat localValue) const { return localValue; }

	/// Performs a sum-reduce operation over all processors.
	inline Matrix3 reduce_sum(Matrix3& localValue) const { return localValue; }

	/// Performs a sum-reduce operation over all processors.
	template<size_t N>
	inline boost::array<AtomInteger,N> reduce_sum(boost::array<AtomInteger,N>& localValue) const { return localValue; }

	/// Performs a max-reduce operation over all processors.
	inline int allreduce_max(int localValue) const { return localValue; }

	/// Performs a min-reduce operation over all processors.
	inline CAFloat allreduce_max(CAFloat localValue) const { return localValue; }

	/// Performs a min-reduce operation over all processors.
	inline CAFloat allreduce_min(CAFloat localValue) const { return localValue; }

	/// Performs a max-reduce operation over all processors.
	inline void allreduce_max(const CAFloat* localValues, CAFloat* globalValues, size_t count) const { memcpy(globalValues, localValues, sizeof(localValues[0]) * count); }

	/// Performs a min-reduce operation over all processors.
	inline void allreduce_min(const CAFloat* localValues, CAFloat* globalValues, size_t count) const { memcpy(globalValues, localValues, sizeof(localValues[0]) * count); }

	/// Performs a max-reduce operation over all processors.
	inline int reduce_max(int localValue) const { return localValue; }

	/// Waits for all processors to reach the barrier.
	inline void barrier() const {}

	/// Returns whether the current processor should exchange data with the neighbor in the given direction.
	inline bool shouldSendOrReceive(unsigned int dim, unsigned int dir, bool pbc) const { return pbc; }

	/// Performs data exchange with neighboring processors.
	void exchange(unsigned int dim, unsigned int dir, bool pbc, void* sendBuffer, int sendCount, void* receiveBuffer, int receiveCount) const {
		// This operation is not permitted in non-MPI mode.
		CALIB_ASSERT(pbc);
	}

	/// Performs data exchange with neighboring processors.
	void exchange(unsigned int dim, unsigned int dir, bool pbc, int sendValue, int& receiveValue) const {
		CALIB_ASSERT(pbc);
		receiveValue = sendValue;
	}

	/// Performs data exchange with neighboring processors.
	void exchange(unsigned int dim, unsigned int dir, bool pbc, unsigned int sendValue, unsigned int& receiveValue) const {
		CALIB_ASSERT(pbc);
		receiveValue = sendValue;
	}

	/// Performs data exchange with neighboring processors.
	template<typename T>
	void exchange(int dim, int dir, bool pbc, std::vector<T>& sendBuffer, std::vector<T>& receiveBuffer) const {
		// This operation is not permitted in non-MPI mode.
		CALIB_ASSERT(pbc);
		receiveBuffer = sendBuffer;
	}

	/// Performs data exchange with neighboring processors.
	template<typename T>
	void exchangeWithSize(int dim, int dir, bool pbc, std::vector<T>& sendBuffer, std::vector<T>& receiveBuffer) const {
		// This operation is not permitted in non-MPI mode.
		CALIB_ASSERT(pbc);
		receiveBuffer = sendBuffer;
	}

	/// Sends a STL vector to another processor.
	template<typename T>
	void send(const std::vector<T>& sendBuffer, int destinationProc) const {
		// This operation is not permitted in non-MPI mode.
		CALIB_ASSERT(false);
	}

	/// Sends a STL vector to another processor.
	template<typename T>
	void sendWithSize(const std::vector<T>& sendBuffer, int destinationProc) const {
		// This operation is not permitted in non-MPI mode.
		CALIB_ASSERT(false);
	}

	/// Receives a STL vector from another processor.
	/// Returns number of received elements.
	template<typename T>
	size_t receive(std::vector<T>& receiveBuffer, int sourceProc, size_t offset = 0) const {
		// This operation is not permitted in non-MPI mode.
		CALIB_ASSERT(false);
		return 0;
	}

	/// Receives a STL vector from another processor.
	/// Returns number of received elements.
	template<typename T>
	size_t receiveWithSize(std::vector<T>& receiveBuffer, int sourceProc) const {
		// This operation is not permitted in non-MPI mode.
		CALIB_ASSERT(false);
		return 0;
	}

#endif

	/// Broadcasts a constant-capacity vector to all processors.
	template<typename T, std::size_t C>
	inline void broadcast(FixedCapacityVector<T,C>& v) const {
		broadcast(&v, sizeof(v));
	}

protected:

	/// Aborts the analysis and reports an error before exiting the program.
	/// This function must be called by all processors simultaneously.
	virtual void raiseErrorAllImpl(const char* errorMessage);

	/// Aborts the analysis and reports an error before exiting the program.
	/// This function can be called by a single processor.
	virtual void raiseErrorOneImpl(const char* errorMessage);

protected:

	/// Flag indicating that we are operating in parallel mode.
	bool _parallelMode;

	/// Controls whether log messages from all processors are sent to the console.
	bool _allProcessorsOutput;

	/// Number of processors in each dimension.
	int _processorGrid[3];

	/// This processor's location in the spatial decomposition grid.
	Vector3I _processorLocation;

	/// The output stream to which log messages are sent.
	std::ostream* _msgLogger;

#ifdef CALIB_USE_MPI

	/// Indicates that MPI_Init has been called.
	bool _mpiIsInitialized;

	/// The MPI communicator.
	MPI_Comm _mpiComm;

	/// The number of processors used.
	int _processorCount;

	/// The ID of the local processor.
	int _processorMe;

	/// The neighboring processors in each dimension.
	int _processorNeighbors[3][2];

	/// Contains ranks of distant processors.
	std::vector<int> _processorStrideNeighbors[3][2];

#endif

};

/*
 * Simple helper base class that stores a reference to the global context object.
 *
 * Library classes subclass this helper class to have access to the global
 * context object through an object's lifetime.
 */
class ContextReference
{
public:

	/// Constructor.
	/// Takes a reference to the global context object which will be stored in this class instance.
	ContextReference(CAContext& context) : _context(context) {}

	/// Returns the stored reference to the global context object.
	CAContext& context() const { return _context; }

	/// Returns the stored reference to the global parallelization object.
	CAContext& parallel() const { return _context; }

private:

	/// Reference to the global context object.
	CAContext& _context;
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_CONTEXT_H
