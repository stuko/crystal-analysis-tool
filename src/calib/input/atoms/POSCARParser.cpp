///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructureParser.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Reads the atomic coordinates from the input file in POSCAR format.
******************************************************************************/
void AtomicStructureParser::readPOSCARAtomsFile(TextParserStream& stream, CAFloat scaling_factor, const Vector3& firstCellVector)
{
	context().msgLogger() << "Parsing POSCAR file." << endl;

	AffineTransformation simulationCell(AffineTransformation::Identity());
	simulationCell.linear().col(0) = firstCellVector;

	if(parallel().isMaster()) {
		// Read cell matrix
		for(size_t i = 1; i < 3; i++) {
			stream.readline();
			if(sscanf(stream.line().c_str(), CAFLOAT_SCANF_STRING_3, &simulationCell.linear()(0,i), &simulationCell.linear()(1,i), &simulationCell.linear()(2,i)) != 3)
				context().raiseErrorOne("POSCAR file parsing error. Invalid cell vector (line %i): %s", stream.lineNumber(), stream.line().c_str());
		}
		simulationCell.scale(scaling_factor);
	}

	setupSimulationCell(simulationCell, _preprocessing.pbc);

	// Parse number of atoms per type.
	vector<int> atomCounts;
	_numInputAtoms = 0;

	bool isCartesian = false;
	if(parallel().isMaster()) {
		stream.readline();
		string::const_iterator tokenBegin = stream.line().begin();
		while(tokenBegin != stream.line().end()) {
			while(tokenBegin != stream.line().end() && (*tokenBegin == ' ' || *tokenBegin == '\t')) ++tokenBegin;
			if(tokenBegin == stream.line().end()) break;
			string::const_iterator tokenEnd = tokenBegin + 1;
			while(tokenEnd != stream.line().end() && *tokenEnd != ' ' && *tokenEnd != '\t') ++tokenEnd;
			string token(tokenBegin, tokenEnd);
			int n;
			if(sscanf(token.c_str(), "%i", &n) != 1 || n < 0)
				context().raiseErrorOne("POSCAR file parsing error. Invalid token in line %i: %s", stream.lineNumber(), stream.line().c_str());
			atomCounts.push_back(n);
			_numInputAtoms += n;
			tokenBegin = tokenEnd;
		}

		// Read in 'Selective dynamics' flag
		stream.readline();

		// Parse coordinate system.
		if(stream.line().length() >= 1 && (stream.line().at(0) == 'C' || stream.line().at(0) == 'c' || stream.line().at(0) == 'K' || stream.line().at(0) == 'k')) {
			isCartesian = true;
		}
	}
	parallel().broadcastWithSize(atomCounts);
	parallel().broadcast(_numInputAtoms);

	beginDistributeAtoms();

	// Read in atom coordinates.
	for(int atype = 1; atype <= atomCounts.size(); atype++) {
		for(int i = 0; i < atomCounts[atype-1]; i++) {
			AtomInfo atomInfo;
			if(parallel().isMaster()) {
				stream.readline();

				atomInfo.tag = _numAtomsRead + 1;
				atomInfo.species = atype;

				if(sscanf(stream.line().c_str(), CAFLOAT_SCANF_STRING_3, &atomInfo.pos.x(), &atomInfo.pos.y(), &atomInfo.pos.z()) != 3)
					context().raiseErrorOne("POSCAR file parsing error. Invalid atom coordinate (line %d): %s", stream.lineNumber(), stream.line().c_str());

				if(!isCartesian)
					atomInfo.pos = simulationCell * atomInfo.pos;
				else
					atomInfo.pos = atomInfo.pos * scaling_factor;
			}
			distributeAtom(atomInfo);
		}
	}

	endDistributeAtoms();
}

}; // End of namespace
