#!/usr/bin/env python

# Creates a POV-Ray file for rendering defect surfaces.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input dislocations file and an output POV-Ray file.")

def skipToKeyword(stream, keyword):
    while True:
        line = stream.readline()
        if line == "": raise IOError("Unexpected end of surface file.")
        if line.startswith(keyword):
            return line

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")

if instream.readline() != "# vtk DataFile Version 3.0\n": raise IOError("Invalid VTK file.")
infoLine = instream.readline()
if infoLine.startswith("# Defect surface") == False: raise IOError("Invalid surface file.")    
dxaVersionString = infoLine.strip()
if instream.readline() != "ASCII\n" or instream.readline() != "DATASET UNSTRUCTURED_GRID\n": raise IOError("Invalid surface file.")

# Open the output file.
outstream = stdout
if args[1] != '-': outstream = open(args[1], "w")

outstream.write("mesh2 {\n")
outstream.write("  vertex_vectors {\n")

# Read in point coordinates
tokens = instream.readline().split()
if tokens[0] != "POINTS": raise IOError("Invalid surface file.")
numPoints = int(tokens[1])

outstream.write("    %i" % numPoints)

for i in range(numPoints):
    tokens = instream.readline().split()
    outstream.write(",\n    <%s, %s, %s>" % (tokens[0], tokens[1], tokens[2]))
outstream.write("\n  }\n")

numFacets = int(skipToKeyword(instream, "CELLS").split()[1])

# Read in facets.
facetIndices = []
for i in range(numFacets):
    tokens = instream.readline().split()
    numFacetPoints = int(tokens[0])
    assert(numFacetPoints == 3)
    facetIndices.append((int(tokens[1]), int(tokens[2]), int(tokens[3])))    

skipToKeyword(instream, "POINT_DATA")
skipToKeyword(instream, "NORMALS point_normals float")

outstream.write("  normal_vectors {\n")
outstream.write("    %i" % numPoints)

for i in range(numPoints):
    tokens = instream.readline().split()
    outstream.write(",\n    <%s, %s, %s>" % (tokens[0], tokens[1], tokens[2]))
outstream.write("\n  }\n")

outstream.write("  face_indices {\n")
outstream.write("    %i" % numFacets)
for f in facetIndices:
    outstream.write(",\n    <%i, %i, %i>" % f)
outstream.write("\n  }\n")

outstream.write("}\n")
outstream.close()
