#!/usr/bin/env python

# Aligns the directions of dislocation lines such that they all point in a similar direction.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input and an output dislocations file.")

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")
network = loadDislocationNetwork(instream)

for segment in network.segments:
	lineVec = segment.points[-1] - segment.points[0]
	if abs(lineVec.x()) > abs(lineVec.y()):
		if abs(lineVec.x()) > abs(lineVec.z()):
			if lineVec.x() < 0: segment.flipOrientation()
		else:
			if lineVec.z() < 0: segment.flipOrientation()
	else:
		if abs(lineVec.y()) > abs(lineVec.z()):
			if lineVec.y() < 0: segment.flipOrientation()
		else:
			if lineVec.z() < 0: segment.flipOrientation()
 
# Write the output file.
outstream = stdout
if args[1] != '-': outstream = open(args[1], "w")
saveDislocationNetwork(outstream, network)
