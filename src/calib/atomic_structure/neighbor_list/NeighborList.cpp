///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "NeighborList.h"
#include "../AtomicStructure.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Generates the neighbor lists for the atoms of the atomic structure.
******************************************************************************/
void NeighborList::buildNeighborLists(CAFloat cutoff, bool sortNeighborLists, bool generateGhostNeighborLists, int numNearestNeighbors)
{
	CALIB_ASSERT_MSG(cutoff > 0.0, "NeighborList::buildNeighborLists()", "Neighbor list cutoff must be positive.");

	// Save cutoff radius for later use.
	this->_cutoff = cutoff;
	this->_cutoffSquared = square(cutoff);
	this->_isSorted = sortNeighborLists;

	Vector3 cellVectors[3] = {
			structure().cellVector(0),
			structure().cellVector(1),
			structure().cellVector(2)
	};

	// Check if requested neighbor cutoff radius is compatible with the atomic structure and the selected
	// neighbor mode.
	if(structure().neighborMode() == AtomicStructure::MINIMUM_IMAGE_CONVENTION) {
		// Minimum diameter of simulation cell must be at least two times the neighbor cutoff.
		for(int dim = 0; dim < 3; dim++) {
			if(structure().pbc(dim)) {
				CAFloat d = cellVectors[dim].dot(structure().cellNormalVector(dim));
				if(fabs(d) < cutoff * 2)
					context().raiseErrorAll("Simulation cell is too small for minimum image convention.");
			}
		}
	}

	// Determine the dimensions of the bin grid and the size of a single bin cell.
	Vector3I binCount;
	AffineTransformation domain = structure().processorDomainMatrix();
	for(int dim = 0; dim < 3; dim++) {

		// Compute normal vector of current cell face.
		Vector3 normal = structure().cellNormalVector(dim);

		// Extend domain to include the ghost margin.
		if(structure().neighborMode() == AtomicStructure::GHOST_ATOMS) {

			CAFloat d = domain.matrix().col(dim).dot(normal);
			CALIB_ASSERT(d >= 0.0);
			Vector3 margin = domain.matrix().col(dim) * (structure().ghostCutoff() / d);

			if(parallel().shouldSendOrReceive(dim, 0, structure().pbc(dim))) {
				domain.translation() -= margin;
				domain.matrix().col(dim) += margin;
			}
			if(parallel().shouldSendOrReceive(dim, 1, structure().pbc(dim))) {
				domain.matrix().col(dim) += margin;
			}
		}

		// Calculate number of bins.
		binCount[dim] = (int)floor(fabs(domain.matrix().col(dim).dot(normal)) / cutoff);

		// We always have at least one bin in each dimension.
		// But too many bins are not good either, because they would take too much memory.
		clamp(binCount[dim], 1, 100);
	}

	// Determine shape of a single bin cell.
	AffineTransformation binCell = domain *
			Eigen::Scaling(1.0 / binCount[0], 1.0 / binCount[1], 1.0 / binCount[2]);

	// Calculate number of bins in a stencil.
	Vector3I stencilCount;
	for(int dim = 0; dim < 3; dim++) {
		Vector3 normal = structure().cellNormalVector(dim);
		stencilCount[dim] = (int)floor(fabs(binCell.matrix().col(dim).dot(normal)) / cutoff);
		clamp(stencilCount[dim], 1, 100);
	}

	// Compute the reciprocal bin cell for fast lookup.
	AffineTransformation reciprocalBinCell = binCell.inverse();

	// Allocate 3-dimensional bin head array.
	vector<int> linkedListHeads(binCount.prod(), -1);

	// Allocate pointer array for linked list of atoms.
	int numAtoms = structure().numLocalAtoms() + structure().numGhostAtoms();
	vector<int> linkedListPointers(numAtoms);

	// Stores the bin index for each atom.
	vector<Vector3I> atomBinLocations(numAtoms);

	// Sort atoms into bins.
	for(int atomIndex = 0; atomIndex < numAtoms; atomIndex++) {

		// Determine the bin the atom is located in.
		Vector3 rp = reciprocalBinCell * structure().atomPosition(atomIndex);
		Vector3I coordi;
		for(int dim = 0; dim < 3; dim++) {

			// All atoms should be within the domain (plus a small margin).
			CALIB_ASSERT((rp[dim] >= -CAFLOAT_EPSILON && rp[dim] <= CAFloat(binCount[dim]) + CAFLOAT_EPSILON) || structure().pbc(dim) == false);

			coordi[dim] = clamped((int)floor(rp[dim]), 0, binCount[dim]-1);
		}
		int binIndex = (coordi.z() * binCount[0] * binCount[1]) + (coordi.y() * binCount[0]) + coordi.x();
		CALIB_ASSERT(binIndex >= 0 && binIndex < linkedListHeads.size());

		// Insert atom into linked list of bin.
		linkedListPointers[atomIndex] = linkedListHeads[binIndex];
		linkedListHeads[binIndex] = atomIndex;

		// Save bin coordinates for later use.
		atomBinLocations[atomIndex] = coordi;
	}

	// Build list of neighbor bins to be visited around a central bin.
	vector<Vector3I> stencil;
	for(int ix = -stencilCount[0]; ix <= +stencilCount[0]; ix++) {
		for(int iy = -stencilCount[1]; iy <= +stencilCount[1]; iy++) {
			for(int iz = -stencilCount[2]; iz <= +stencilCount[2]; iz++) {
				stencil.push_back(Vector3I(ix,iy,iz));
			}
		}
	}

	bool wrapAtPBC = (structure().neighborMode() != AtomicStructure::GHOST_ATOMS);

	// Allocate output memory.
	size_t nnlistSize = structure().numLocalAtoms();
	if(generateGhostNeighborLists) nnlistSize += structure().numGhostAtoms();
	_neighborCountArray.resize(nnlistSize);
	_neighborListHeads.resize(nnlistSize);
	_neighborLists.clear();

	// Build per-atom neighbor lists for all local atoms.
	vector<int>::iterator numNeighbors = _neighborCountArray.begin();
	for(int atomIndex = 0; atomIndex < structure().numLocalAtoms(); atomIndex++, ++numNeighbors) {
		*numNeighbors = 0;
		_neighborListHeads[atomIndex] = _neighborLists.size();
		const Vector3I centerBin = atomBinLocations[atomIndex];

		// Iterate over neighbor bins.
		for(vector<Vector3I>::const_iterator nbin = stencil.begin(); nbin != stencil.end(); ++nbin) {
			Vector3I currentBin = centerBin + (*nbin);
			Vector3 centerPos = structure().atomPosition(atomIndex);

			// Handle out-of-range case.
			bool skipBin = false;
			for(int dim = 0; dim < 3; dim++) {
				if(currentBin[dim] < 0 || currentBin[dim] >= binCount[dim]) {
					if(wrapAtPBC && structure().pbc(dim)) {
						// Wrap at periodic boundaries.
						if(currentBin[dim] < 0) {
							currentBin[dim] += binCount[dim];
							centerPos += cellVectors[dim];
						}
						else {
							currentBin[dim] -= binCount[dim];
							centerPos -= cellVectors[dim];
						}
					}
					else {
						skipBin = true;
						break;
					}
					CALIB_ASSERT(currentBin[dim] >= 0 && currentBin[dim] < binCount[dim]);
				}
			}
			if(skipBin) continue;

			int binIndex = (currentBin.z() * binCount[0] * binCount[1]) + (currentBin.y() * binCount[0]) + currentBin.x();
			CALIB_ASSERT(binIndex >= 0 && binIndex < linkedListHeads.size());

			// Iterate over all atoms in the current bin.
			for(int neighborIndex = linkedListHeads[binIndex]; neighborIndex != -1; neighborIndex = linkedListPointers[neighborIndex]) {

				neighbor_info entry;
				entry.delta = structure().atomPosition(neighborIndex) - centerPos;
				entry.distsq = entry.delta.squaredNorm();
				if(entry.distsq <= _cutoffSquared) {
					// Skip the central atom.
					if(neighborIndex == atomIndex && *nbin == Vector3I::Zero())
						continue;

					// Add neighbor atom to neighbor list of current central atom.
					entry.index = neighborIndex;
					_neighborLists.push_back(entry);
					(*numNeighbors)++;
				}
			}
		}

		if(sortNeighborLists) {

			// Sort neighbor list of current atom.
			std::sort(_neighborLists.begin() + _neighborListHeads[atomIndex], _neighborLists.end());

			// Restrict list to N nearest neighbors.
			if((*numNeighbors) > numNearestNeighbors) {
				_neighborLists.resize(_neighborListHeads[atomIndex] + numNearestNeighbors);
				*numNeighbors = numNearestNeighbors;
			}
		}
	}

	// When using ghost atoms, transfer generated neighbor lists of local atoms to neighboring processors.
	if(generateGhostNeighborLists && structure().neighborMode() == AtomicStructure::GHOST_ATOMS) {

		// We transfer the list of neighbor atom tags only, and then each neighbor tag
		// is looked up on the receiving processor. To speed this up,
		// we use the neighbor bins to confine the search to a small set of candidate atoms.
		int atomIndex = structure().numLocalAtoms();
		vector<int>::iterator ghostNeighborCount = _neighborCountArray.begin() + structure().numLocalAtoms();
		for(int dim = 0; dim < 3; dim++) {
			if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;
			for(int dir = 0; dir <= 1; dir++) {
				const vector<int>& sendIndices = structure().ghostCommunicationList(dim, dir);

				// Pack neighbor counts of local atoms into a send buffer.
				size_t totalSendNeighborCount = 0;
				vector<int> countSendBuffer(sendIndices.size());
				vector<int>::iterator sendCountItem = countSendBuffer.begin();
				BOOST_FOREACH(int index, sendIndices) {
					*sendCountItem++ = neighborCount(index);
					totalSendNeighborCount += neighborCount(index);
				}

				// Allocate receive buffer and send packed neighbor counts to neighbor processor.
				vector<int> countReceiveBuffer(structure().ghostReceiveCount(dim, dir));
				parallel().exchange(dim, dir, structure().pbc(dim), countSendBuffer, countReceiveBuffer);

				// Pack neighbor lists of local atoms into a send buffer.
				vector<AtomTag> sendBuffer(totalSendNeighborCount);
				vector<AtomTag>::iterator sendItem = sendBuffer.begin();
				BOOST_FOREACH(int index, sendIndices) {
					BOOST_FOREACH(const neighbor_info& n, neighbors(index)) {
						*sendItem++ = structure().atomTag(n.index);
					}
				}
				CALIB_ASSERT(sendItem == sendBuffer.end());

				// Allocate receive buffer and send packed neighbor lists to neighbor processor.
				int receiveCount = boost::accumulate(countReceiveBuffer, 0);
				vector<AtomTag> receiveBuffer(receiveCount);
				parallel().exchange(dim, dir, structure().pbc(dim), sendBuffer, receiveBuffer);

				// Unpack received neighbor lists of ghost atoms.
				_neighborLists.reserve(_neighborLists.size() + receiveCount);
				vector<AtomTag>::const_iterator receivedTag = receiveBuffer.begin();
				for(vector<int>::const_iterator neighborCount = countReceiveBuffer.begin(); neighborCount != countReceiveBuffer.end(); ++neighborCount, ++atomIndex, ++ghostNeighborCount) {
					*ghostNeighborCount = *neighborCount;

					// Determine the bin the current ghost atom is located in.
					_neighborListHeads[atomIndex] = _neighborLists.size();
					const Vector3I centerBin = atomBinLocations[atomIndex];
					const Vector3 centerPos = structure().atomPosition(atomIndex);

					// Iterate over all received neighbor atom tags.
					for(int i = 0; i < *neighborCount; i++, ++receivedTag) {

						// Iterate over neighbor bins.
						bool foundTag = false;
						for(vector<Vector3I>::const_iterator nbin = stencil.begin(); nbin != stencil.end() && !foundTag; ++nbin) {
							Vector3I currentBin = centerBin + (*nbin);

							// Handle out-of-range case.
							if(currentBin[0] < 0 || currentBin[0] >= binCount[0] ||
								currentBin[1] < 0 || currentBin[1] >= binCount[1] ||
								currentBin[2] < 0 || currentBin[2] >= binCount[2])
								continue;

							int binIndex = (currentBin.z() * binCount[0] * binCount[1]) + (currentBin.y() * binCount[0]) + currentBin.x();
							CALIB_ASSERT(binIndex >= 0 && binIndex < linkedListHeads.size());

							// Iterate over all atoms in the current bin.
							for(int neighborIndex = linkedListHeads[binIndex]; neighborIndex != -1; neighborIndex = linkedListPointers[neighborIndex]) {
								if(structure().atomTag(neighborIndex) == *receivedTag) {
									neighbor_info entry;
									entry.index = neighborIndex;
									entry.delta = structure().atomPosition(neighborIndex) - centerPos;
									entry.distsq = entry.delta.squaredNorm();
									CALIB_ASSERT(entry.distsq <= _cutoffSquared + CAFLOAT_EPSILON);
									_neighborLists.push_back(entry);
									foundTag = true;
									break;
								}
							}
						}
						if(!foundTag) {
							// If one of the received neighbors could not be resolved
							// (because the ghost atom is close to the edge of the ghost layer and the neighbor atom is not present on the local processor),
							// we discard the whole neighbor list of the ghost atom.
							// Then the ghost atom will only serve as a neighbor for other ghost atoms.
							*ghostNeighborCount = 0;
							_neighborLists.resize(_neighborListHeads[atomIndex]);
							receivedTag += *neighborCount - i;
							break;
						}
					}
				}
				CALIB_ASSERT(receivedTag == receiveBuffer.end());
			}
		}
		CALIB_ASSERT(ghostNeighborCount == _neighborCountArray.end());
	}
}

}; // End of namespace
