########################################################
# Lattices
########################################################

#--------------------------------------------------------
# Face-centered cubic lattice
#--------------------------------------------------------
lattice: fcc
  file: lattices/fcc/fcc.poscar 
  innercutoff: 0.8 
  outercutoff: 1.1
  coordtype: nda
  maxdisplacement: 0.35

########################################################
# Grain boundaries
########################################################

#--------------------------------------------------------
# Coherent twin boundary in fcc lattice (symmetric Sigma 3)
#--------------------------------------------------------
interface: fcc_coherent_twin
  file: lattices/fcc/fcc_coherent_twin.poscar 
  lattices: fcc
  innercutoff: 0.8 
  outercutoff: 1.1
  coordtype: nda
  maxdisplacement: 0.35
  vicinity: interface
  
########################################################
# Planar defects
########################################################

#--------------------------------------------------------
# Intrinsic stacking fault in fcc lattice
#--------------------------------------------------------
interface: fcc_isf
  file: lattices/fcc/fcc_intrinsic_stacking_fault.poscar 
  lattices: fcc
  innercutoff: 0.8 
  outercutoff: 1.1
  coordtype: acna
  vicinity: shortestpath

#--------------------------------------------------------
# Extended stacking fault in fcc lattice with three 
# layers of hcp coordinated atoms
#--------------------------------------------------------
interface: fcc_triple_sf
  file: lattices/fcc/fcc_triple_stacking_fault.poscar 
  lattices: fcc
  innercutoff: 0.8 
  outercutoff: 1.1
  coordtype: acna
  vicinity: interface

#--------------------------------------------------------
# Extended stacking fault in fcc lattice with four layers
# of hcp coordinated atoms.
#--------------------------------------------------------
interface: fcc_quad_sf
  file: lattices/fcc/fcc_quad_stacking_fault.poscar 
  lattices: fcc
  innercutoff: 0.8 
  outercutoff: 1.1
  coordtype: acna
  vicinity: interface

