///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ATOMIC_STRUCTURE_H
#define __CA_ATOMIC_STRUCTURE_H

#include "../CALib.h"
#include "SimulationCell.h"

namespace CALib {

/// Type used to store the tag of an atom.
typedef AtomInteger AtomTag;

/**
 * Stores the atoms.
 *
 * The AtomicStructure class supports distributed or spatially decomposed
 * data with only a subset of the atoms being stored on the local processor.
 *
 * Atoms from neighboring processors within a certain range are duplicated on the
 * local processor where they are called 'ghost atoms'.
 */
class AtomicStructure : public SimulationCell
{
public:

	/// Different modes supported by AtomicStructure which control
	/// how to deal with periodic boundaries and the spatial domain decomposition when
	/// generating neighbor lists.
	enum NeighborMode {

		/// In ghost-atom mode, atoms are duplicated within a certain range (the ghost cutoff) to
		/// handle periodic images of atoms and to ensure compatibility at processor domain boundaries.
		/// This mode must be used whenever spatial decomposition is used.
		GHOST_ATOMS,

		/// Mode without explicit ghost atoms, which can only be used with large simulation cell whose
		/// minimum diameter is at least two times the neighbor list cutoff.
		/// An atom can only appear once in the neighbor list of another atom.
		MINIMUM_IMAGE_CONVENTION,

		/// Mode without explicit ghost atoms, which can be used with any simulation cell size.
		/// When generating neighbor lists, periodic images of atoms are implicitly considered.
		/// An atom can appear several times in the neighbor list of another atom.
		IMPLICIT_IMAGES
	};

public:

	/// Constructor.
	AtomicStructure(CAContext& context, NeighborMode neighborMode, const std::string& name = std::string());

	/************************************ General properties ***************************************/

	/// Returns the user-assigned name of the structure.
	const std::string& name() const { return _name; }

	/// Assigns a name to the structure.
	void setName(const std::string& name) { this->_name = name; }

	/********************************** Spatial decomposition***************************************/

	/// Returns the neighbor mode set for this atomic structure. The neighbor mode
	/// described how to deal with periodic boundaries and the spatial decomposition when
	/// building neighbor lists.
	NeighborMode neighborMode() const { return _neighborMode; }

	/****************************************** Atoms ************************************************/

	/// Returns the number of atoms that are owned by this processor.
	int numLocalAtoms() const { return _numLocalAtoms; }

	/// Returns the number of ghost atoms received from neighboring processors.
	int numGhostAtoms() const { return _numGhostAtoms; }

	/// The number of tessellation helper atoms created on this processor.
	int numHelperAtoms() const { return _numHelperAtoms; }

	/// Returns the total number of atoms in the system on all processors.
	AtomInteger numTotalAtoms() const { return _numTotalAtoms; }

	/// Returns the position of the i-th atom.
	const Vector3& atomPosition(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_atomPositions.size());
		return _atomPositions[atomIndex];
	}

	/// Changes the position of an atom. This function is for internal use only.
	void setAtomPosition(int atomIndex, const Vector3& newPos) {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_atomPositions.size());
		_atomPositions[atomIndex] = newPos;
	}

	/// Returns the unique tag of the i-th atom.
	AtomTag atomTag(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_atomTags.size());
		return _atomTags[atomIndex];
	}

	/// Returns the original tag of the i-th atom (before replication of the system).
	AtomTag atomOriginalTag(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_atomTags.size());
		return _atomTags[atomIndex] & ((1 << CA_ATOM_IMAGE_SHIFT) - 1);
	}

	/// Returns the first local atom with the given tag or -1 if no such atom exists.
	int atomIndexFromTag(AtomTag tag) const {
		for(int atomIndex = 0; atomIndex < numLocalAtoms(); atomIndex++)
			if(atomTag(atomIndex) == tag) return atomIndex;
		return -1;
	}

	/// Returns the species (atom type) of the i-th atom.
	int atomSpecies(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_atomSpecies.size());
		return _atomSpecies[atomIndex];
	}

	/// Returns the (unwrapped) processor grid coordinate of an atom.
	const Vector3I& atomProcCoord(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_atomProcCoords.size());
		return _atomProcCoords[atomIndex];
	}

	/// Returns the list of atomic positions.
	const std::vector<Vector3>& atomPositions() const {
		return _atomPositions;
	}

	/// Returns the list of atomic species.
	const std::vector<int>& atomSpeciesList() const {
		return _atomSpecies;
	}

	/// Returns the list of atomic tags.
	const std::vector<AtomInteger>& atomTags() const {
		return _atomTags;
	}

	/// Returns true if the given atom is a local atom.
	bool isLocalAtom(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < numLocalAtoms() + numGhostAtoms());
		return atomIndex < numLocalAtoms();
	}

	/// Returns true if the given atom is a ghost atom.
	bool isGhostAtom(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < numLocalAtoms() + numGhostAtoms());
		return atomIndex >= numLocalAtoms();
	}

	/// Returns true if the given atom is a tessellation helper atom.
	bool isHelperAtom(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < numLocalAtoms() + numGhostAtoms() + numHelperAtoms());
		return atomIndex >= numLocalAtoms() + numGhostAtoms();
	}

	/// Must be called before adding atoms to the structure.
	void beginAddingAtoms(int estimatedNumberOfAtomsToBeAdded = 0);

	/// Adds an atom to the atoms array.
	int addAtom(const Vector3& pos, AtomInteger tag, int species, const Vector3I& procCoord,
			bool isGhostAtom = false, bool isHelperAtom = false, int pbcImageNumber = 0);

	/// Must be called after all atoms have been added.
	void endAddingAtoms();

	/// Returns the overlap radius between processor domains.
	CAFloat ghostCutoff() const { return _ghostCutoff; }

	/// Sets the overlap distance between processor domains.
	void setGhostCutoff(CAFloat cutoff) { _ghostCutoff = cutoff; }

	/// Computes the bounding box of all atoms.
	void computeBoundingBox();

	/// Creates extra helper atoms in empty regions of the simulation cell to
	/// make the Delaunay triangulation in the overlap region between processors consistent.
	AtomInteger createExtraTessellationPoints(CAFloat maxPointDistance);

	/****************************************** Communication ************************************************/

	/// Transfers local atoms to neighboring processors.
	void communicateGhostAtoms();

	/// Returns the list of indices of local atoms that are transferred to the given neighboring processor.
	const std::vector<int>& ghostCommunicationList(int dim, int dir) const {
		return _ghostCommunicationLists[dim][dir];
	}

	/// Returns the number of ghost atoms received from the given neighboring processor.
	int ghostReceiveCount(int dim, int dir) const {
		return _ghostReceiveCounts[dim][dir];
	}

protected:

	/// The size of the ghost atom layer.
	CAFloat _ghostCutoff;

	/// The number of atoms that are owned by this processor.
	int _numLocalAtoms;

	/// The number of ghost atoms received from neighboring processors.
	int _numGhostAtoms;

	/// The number of tessellation helper atoms created on this processor.
	int _numHelperAtoms;

	/// The total number of atoms in the system on all processors.
	AtomInteger _numTotalAtoms;

	/// The atomic coordinates.
	std::vector<Vector3> _atomPositions;

	/// The atomic species.
	std::vector<int> _atomSpecies;

	/// The tags of atoms.
	std::vector<AtomInteger> _atomTags;

	/// The processor locations of atoms.
	std::vector<Vector3I> _atomProcCoords;

	/// For each spatial direction, this list contains the indices of the local atoms that must be transferred
	/// to the neighboring processors.
	std::vector<int> _ghostCommunicationLists[3][2];

	/// For each spatial direction, this stores the number of ghost atoms received from the neighboring processor.
	int _ghostReceiveCounts[3][2];

	/// The name of this structure.
	std::string _name;

	/// The neighbor for this atomic structure.
	NeighborMode _neighborMode;

	template<typename T> friend class GhostAtomCommunicationHelper;
};

}; // End of namespace

#endif // __CA_ATOMIC_STRUCTURE_H
