///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifdef FIX_CLASS

FixStyle(analysis/crystal, CrystalAnalysisFix)

#else

#ifndef __CRYSTALANALYSIS_FIX_LAMMPS_H
#define __CRYSTALANALYSIS_FIX_LAMMPS_H

#include "fix.h"
#include "comm.h"
#include "modify.h"
#include "domain.h"
#include "error.h"
#include "atom.h"
#include "error.h"
#include "update.h"
#include "neighbor.h"
#include "neigh_request.h"
#include "neigh_list.h"
#include "fix_ave_atom.h"

// Enables sanity checks in the CA code.
#define DEBUG_CRYSTAL_ANALYSIS

// LAMMPS uses double precision floating-point numbers.
// The CA library should too.
#define CALIB_USE_DOUBLE_PRECISION_FP

// LAMMPS uses MPI, so should the CA library.
#define CALIB_USE_MPI

#include <calib/CALib.h>
#include <calib/util/FILEStream.h>
#include <calib/context/CAContext.h>

using namespace LAMMPS_NS;
using namespace CALib;

namespace CALib {
	class AtomicStructure;
	class PatternCatalog;
	struct SuperPattern;
	class CoordinationPattern;
};

class CrystalAnalysisFix : public Fix, public CALib::CAContext
{
public:

	/// Constructor.
	CrystalAnalysisFix(class LAMMPS*, int, char **);

	/// Destructor.
	virtual ~CrystalAnalysisFix();

	/******************** Virtual methods from Fix base class ************************/

	/// The return value of this method specifies at which points the fix is invoked during the simulation.
	virtual int setmask();

	/// This gets called by the system before the simulation starts.
	virtual void init();

	/// This is called by LAMMPS and the beginning of a run.
	virtual void setup(int);

	/// Is called by LAMMPS at the beginning of every integration step.
	virtual void initial_integrate(int vflag);

	/// Called at the end of MD time integration steps.
	virtual void end_of_step();

	/// Reports the memory usage of this fix to LAMMPS.
	virtual double memory_usage();

protected:

	/// Prepares an output file for writing during the initialization phase.
	void initOutputStream(const std::string& filename, std::ofstream& stream);

	/// Opens an output file for writing during the simulation phase.
	bool openOutputStream(const std::string& filename, std::ofstream& stream);

	/// Opens an output file for writing during the simulation phase.
	bool openOutputStream(const std::string& filename, FILE** stream);

	/// Replaces placeholder characters in a filename.
	std::string resolveFilename(const std::string& filename);

	/// Aborts the analysis and reports an error before exiting the program.
	virtual void raiseErrorAllImpl(const char* errorMessage);

	/// Aborts the analysis and reports an error before exiting the program.
	virtual void raiseErrorOneImpl(const char* errorMessage);

	/// This function aborts the program after printing an error message when an assertion error occurs.
	static void handleFatalAssertionError(const char* file, int line, const char* location, const char* msg);

protected:

	/// The output stream to which log messages are sent.
	stdio_ostream msgLoggerStream;

	/// The name of the output file to which the log messages are written.
	std::string logFilename;

	/// The output stream to which the log messages are written.
	std::ofstream logStream;

	/// The fix that computes the time-averaged atomic positions.
	FixAveAtom* averageFix;

	/// The catalog of structure patterns.
	PatternCatalog* patternCatalog;

	/// The list of super patterns to search for in the atomistic data.
	std::vector<SuperPattern const*> superPatternsToSearchFor;

	/// The list of coordination patterns to search for in the atomistic data.
	std::vector<CoordinationPattern const*> coordinationPatternsToSearchFor;

	/// The neighbor list curoff.
	CAFloat neighborCutoff;

	/// The ghost atom communcation cutoff.
	CAFloat ghostCutoff;

	// Maximum number of neighbors per atom required for coordination pattern analysis.
	int maxPatternNeighbors;

	/// Dislocation tracer parameters.
	int maxBurgersCircuitSize;
	int maxExtendedBurgersCircuitSize;

	/// Output options.
	CAFloat linePointInterval;
	int lineSmoothingLevel;
	int surfaceSmoothingLevel;

	/// Output files.
	std::string atomsOutputFile;
	std::string cellOutputFile;
	std::string meshOutputFile;
	std::string surfaceOutputFile;
	std::string surfaceCapOutputFile;
	std::string dislocationsOutputFile;
	std::string analysisOutputFile;

	/// Output streams.
	std::ofstream atomsOutputStream;
	std::ofstream cellOutputStream;
	std::ofstream meshOutputStream;
	std::ofstream surfaceOutputStream;
	std::ofstream surfaceCapOutputStream;
	std::ofstream dislocationsOutputStream;
	FILE* analysisOutputStream;
};

#endif // __CRYSTALANALYSIS_FIX_LAMMPS_H

#endif // FIX_CLASS
