///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "ElasticMapping.h"
#include "../atomic_structure/GhostAtomCommunicationHelper.h"
#include "CrystalPathFinder.h"

using namespace std;

namespace CALib {

// List of vertices that bound the six edges of a tetrahedron.
static const int edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

//#define DEBUG_EDGES
//#define DEBUG_EDGES_V1 5908853
//#define DEBUG_EDGES_V2 5909578

/******************************************************************************
* Computes the elastic mapping.
* If onlyLocalTetrahedra==true, the elastic deformation field is
* only computed for tetrahedra that overlap with the local processor domain.
* If onlyLocalTetrahedra==false, it is also computed for ghost tetrahedra.
******************************************************************************/
void ElasticMapping::generate(bool onlyLocalTetrahedra, int crystalPathSteps, bool dontReconstructIdealEdgeVectors)
{
	// Build list of edges in the tessellation.
	generateTessellationEdges(onlyLocalTetrahedra);

	// Assign each vertex to a cluster.
	assignVerticesToClusters();

	// Determine the ideal vector corresponding to each edge of the tessellation.
	assignIdealVectorsToEdges(crystalPathSteps, dontReconstructIdealEdgeVectors);
}

/******************************************************************************
* Builds the list of edges in the tetrahedral tessellation.
******************************************************************************/
void ElasticMapping::generateTessellationEdges(bool onlyLocalTetrahedra)
{
	CALIB_ASSERT(structure().neighborMode() == AtomicStructure::GHOST_ATOMS);
	CALIB_ASSERT(_vertexEdges.empty());
	CALIB_ASSERT(_edgeCount == 0);

#ifdef DEBUG_EDGES
	cout << "PROC " << parallel().processor() << " Location=" << parallel().processorLocationPoint() << endl;
#endif

	// Initialize linked list heads for edges.
	_vertexEdges.resize(tessellation().structure().numLocalAtoms() + tessellation().structure().numGhostAtoms(), NULL);

	// Generate list of tessellation edges.
	for(DelaunayTessellation::CellIterator cell = tessellation().begin_cells(); cell != tessellation().end_cells(); ++cell) {

		// Skip invalid cells (those not connecting four physical atoms).
		if(tessellation().isValidCell(cell) == false) continue;

		if(onlyLocalTetrahedra) {
			// Is this tetrahedron not completely on the side of another processor domain?
			if(computeCellOutcodes(cell).first != 0) {
#ifdef DEBUG_EDGES
				for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
					int atom1 = cell->vertex(edgeVertices[edgeIndex][0])->point().atomIndex();
					int atom2 = cell->vertex(edgeVertices[edgeIndex][1])->point().atomIndex();
					CALIB_ASSERT(atom1 >= 0 && atom2 >= 0);
					AtomInteger tag1 = structure().atomTag(atom1);
					AtomInteger tag2 = structure().atomTag(atom2);
					if(tag1 > tag2) {
						swap(tag1, tag2);
						swap(atom1, atom2);
					}
					if(tag1 == DEBUG_EDGES_V1 && tag2 == DEBUG_EDGES_V2) {
						cout << "SKIPPED EDGE ON PROC " << parallel().processor() << "  coord=" <<
								structure().absoluteToReduced(structure().atomPosition(atom1)) << "-" <<
								structure().absoluteToReduced(structure().atomPosition(atom2)) <<
								" AndOutcode=" << computeCellOutcodes(cell).first <<
								" OrOutcode=" << computeCellOutcodes(cell).second << endl;
						for(int v = 0; v < 4; v++) {
							int atomIndex = cell->vertex(v)->point().atomIndex();
							const Point3& x = structure().atomPosition(atomIndex);
							Point3 rp = structure().absoluteToReduced(x);
							const Point3I& p = structure().atomProcCoord(atomIndex);

							string code;

							Point3I l = parallel().processorLocationPoint();
							if(p.X == l.X + 1) code += " X_POSITIVE";
							else if(p.X == l.X - 1) code += " X_NEGATIVE";

							if(p.Y == l.Y + 1) code += " Y_POSITIVE";
							else if(p.Y == l.Y - 1) code += " Y_NEGATIVE";

							if(p.Z == l.Z + 1) code += " Z_POSITIVE";
							else if(p.Z == l.Z - 1) code += " Z_NEGATIVE";

							cout << "  Vertex " << v << ": " << rp << " tag=" << structure().atomTag(atomIndex) << " outcode=" << code << endl;
						}
					}
				}
#endif
				continue;
			}
		}

		for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
			int vertex1 = cell->vertex(edgeVertices[edgeIndex][0])->info();
			int vertex2 = cell->vertex(edgeVertices[edgeIndex][1])->info();
			CALIB_ASSERT(vertex1 >= 0 && vertex2 >= 0);
			TessellationEdge* edge = findEdge(vertex1, vertex2);
			if(edge == NULL) {
				// Create a new pair of edges.
				TessellationEdge* edge12 = _edgePool.construct(vertex1, vertex2);
				TessellationEdge* edge21 = _edgePool.construct(vertex2, vertex1);
				edge12->next = _vertexEdges[vertex1];
				_vertexEdges[vertex1] = edge12;
				edge21->next = _vertexEdges[vertex2];
				_vertexEdges[vertex2] = edge21;
				edge12->reverse = edge21;
				edge21->reverse = edge12;
				_edgeCount++;
#ifdef DEBUG_EDGES
				edge = edge12;
				AtomInteger tag1 = structure().atomTag(vertex1);
				AtomInteger tag2 = structure().atomTag(vertex2);
				if(tag1 > tag2) {
					swap(tag1, tag2);
					swap(vertex1, vertex2);
				}
				if(tag1 == DEBUG_EDGES_V1 && tag2 == DEBUG_EDGES_V2) {
					cout << "CREATED EDGE ON PROC " << parallel().processor() << "  coord=" <<
							structure().absoluteToReduced(structure().atomPosition(vertex1)) << "-" <<
							structure().absoluteToReduced(structure().atomPosition(vertex2)) <<
								" AndOutcode=" << computeCellOutcodes(cell).first <<
								" OrOutcode=" << computeCellOutcodes(cell).second << endl;
					for(int v = 0; v < 4; v++) {
						int atomIndex = cell->vertex(v)->point().atomIndex();
						const Point3& x = structure().atomPosition(atomIndex);
						Point3 rp = structure().absoluteToReduced(x);
						const Point3I& p = structure().atomProcCoord(atomIndex);

						string code;

						Point3I l = parallel().processorLocationPoint();
						if(p.X == l.X + 1) code += " X_POSITIVE";
						else if(p.X == l.X - 1) code += " X_NEGATIVE";

						if(p.Y == l.Y + 1) code += " Y_POSITIVE";
						else if(p.Y == l.Y - 1) code += " Y_NEGATIVE";

						if(p.Z == l.Z + 1) code += " Z_POSITIVE";
						else if(p.Z == l.Z - 1) code += " Z_NEGATIVE";

						cout << "  Vertex " << v << ": " << rp << " tag=" << structure().atomTag(atomIndex) << " outcode=" << code << endl;
					}
				}
#endif
			}
		}
	}
}

/******************************************************************************
* Determines the overlap of a tetrahedron with the domains of neighboring processors.
******************************************************************************/
std::pair<int,int> ElasticMapping::computeCellOutcodes(DelaunayTessellation::CellHandle cell) const
{
	int codeAnd = -1;
	int codeOr = 0;
	const Vector3I l = parallel().processorLocationPoint();

	for(int v = 0; v < 4; v++) {
		int atomIndex = vertexToAtom(cell->vertex(v));
		CALIB_ASSERT(atomIndex >= 0);
		const Vector3I& p = structure().atomProcCoord(atomIndex);

		int code = 0;

		if(p.x() == l.x() + 1) code |= X_POSITIVE;
		else if(p.x() == l.x() - 1) code |= X_NEGATIVE;

		if(p.y() == l.y() + 1) code |= Y_POSITIVE;
		else if(p.y() == l.y() - 1) code |= Y_NEGATIVE;

		if(p.z() == l.z() + 1) code |= Z_POSITIVE;
		else if(p.z() == l.z() - 1) code |= Z_NEGATIVE;

		codeOr |= code;
		codeAnd &= code;
	}

	return make_pair(codeAnd, codeOr);
}

/******************************************************************************
* Assigns each tessellation vertex to a cluster.
******************************************************************************/
void ElasticMapping::assignVerticesToClusters()
{
	CALIB_ASSERT(_vertexClusters.empty());

	// Assign a cluster to each vertex of the tessellation, which will be used to express
	// all reference vectors assigned to the edges leaving the vertex.
	// Make sure the assignment of clusters to the vertices is consistent across processors.
	_vertexClusters.resize(structure().numLocalAtoms() + structure().numGhostAtoms(), NULL);

	// If an atoms is part of an atomic cluster (as an outcome of the super pattern analysis),
	// then this directly determines the cluster assigned to the corresponding tessellation vertex.
	for(int atomIndex = 0; atomIndex < _vertexClusters.size(); atomIndex++) {
		Cluster* c = patternAnalysis().atomCluster(atomIndex);
		_vertexClusters[atomIndex] = c;
	}

	// Now try to assign a cluster to those vertices of the tessellation whose corresponding atom
	// is not part of a cluster. This is performed by repeatedly copying the cluster assignment
	// from an already assigned vertex to all its unassigned neighbors.
	bool notDone;
	do {
		notDone = false;
		for(vector<TessellationEdge*>::const_iterator vertexEdgeList = _vertexEdges.begin(); vertexEdgeList != _vertexEdges.end(); ++vertexEdgeList) {
			TessellationEdge* firstEdge = *vertexEdgeList;
			if(firstEdge == NULL) continue;
			int atom1 = vertexToAtom(firstEdge->vertex1);
			if(atom1 != -1 && _vertexClusters[atom1] == NULL) {
				for(TessellationEdge* e = firstEdge; e != NULL; e = e->next) {
					int atom2 = vertexToAtom(e->vertex2);
					if(atom2 != -1 && _vertexClusters[atom2] != NULL) {
						_vertexClusters[atom1] = _vertexClusters[atom2];
						notDone = true;
						break;
					}
				}
			}
		}
	}
	while(notDone);

	// Transfer local cluster assignments between neighboring processors.
	GhostAtomCommunicationHelper<ClusterId> commHelper(structure());
	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;
		for(int dir = 0; dir <= 1; dir++) {

			// Exchange the clusters assigned to local vertices.
			commHelper.prepare(dim, dir);
			while(commHelper.nextSendItem()) {
				Cluster* cluster = _vertexClusters[commHelper.sendAtomIndex()];
				commHelper.sendItem() = (cluster != NULL) ? cluster->id : ClusterId(-1,-1);
			}
			commHelper.communicate(dim, dir);

			// Unpack cluster IDs from receive buffer.
			while(commHelper.nextReceiveItem()) {
				// Look up cluster and assign it to ghost tessellation vertex.
				_vertexClusters[commHelper.receiveAtomIndex()] = clusterGraph().findCluster(commHelper.receiveItem());
			}
		}
	}
}

//#define CALIB_DETECT_INCOMPATIBLE_EDGES
//#define CALIB_AVOID_LATTICE_LATTICE_EDGES

/******************************************************************************
* Determines the ideal vector corresponding to each edge of the tessellation.
******************************************************************************/
void ElasticMapping::assignIdealVectorsToEdges(int crystalPathSteps, bool dontReconstructIdealEdgeVectors)
{
	CrystalPathFinder pathFinder(_patternAnalysis, crystalPathSteps);

	// Try to assign a reference vector to the tessellation edges.
	vector<TessellationEdge*>::const_iterator vertexEdgeList = _vertexEdges.begin();
	for(int vertexIndex = 0; vertexIndex < _vertexEdges.size(); vertexIndex++, ++vertexEdgeList) {
		Cluster* cluster1 = clusterOfVertex(vertexIndex);
		if(cluster1 == NULL) continue;
		for(TessellationEdge* edge = *vertexEdgeList; edge != NULL; edge = edge->next) {
#ifndef CALIB_DETECT_INCOMPATIBLE_EDGES
			// Check if the reference vector of this edge has already been determined.
			if(edge->hasClusterVector()) continue;
#else
			if(edge->isIncompatible) continue;
#endif

			Cluster* cluster2 = clusterOfVertex(edge->vertex2);
			if(cluster2 == NULL) continue;

#ifdef CALIB_AVOID_LATTICE_LATTICE_EDGES
			if(cluster2->parentCluster() != cluster1->parentCluster() && cluster1->pattern->isLattice() && cluster2->pattern->isLattice())
				continue;
#endif

			// Determine the ideal vector connecting the two atoms.
			boost::optional<ClusterVector> idealVector = pathFinder.findPath(vertexToAtom(vertexIndex), vertexToAtom(edge->vertex2));
			if(!idealVector)
				continue;

			// Translate vector to the frame of the vertex cluster.
			Vector3 localVec;
			if(idealVector->cluster() == cluster1)
				localVec = idealVector->localVec();
			else {
				ClusterTransition* transition = clusterGraph().determineClusterTransition(idealVector->cluster(), cluster1);
				if(!transition) continue;
				localVec = transition->transform(idealVector->localVec());
			}

			// Assign the cluster transition to the edge.
			ClusterTransition* transition = clusterGraph().determineClusterTransition(cluster1, cluster2);
			// The two clusters may be part of two disconnected components of the cluster graph.
			if(!transition)
				continue;

#ifdef CALIB_DETECT_INCOMPATIBLE_EDGES
			if(transition == edge->clusterTransition && !(localVec - edge->clusterVector).isZero(CA_LATTICE_VECTOR_EPSILON)) {
				edge->isIncompatible = true;
				edge->reverse->isIncompatible = true;
				edge->clearClusterVector();
				continue;
			}
#endif
			// Assign cluster vector to edge and its reverse edge.
			edge->assignClusterVector(localVec, transition);
		}
	}

	if(!dontReconstructIdealEdgeVectors) {
		// Some tetrahedron edges might not connect two neighbor atoms. Then they could not be assigned a lattice vector above.
		// For these edges, try to infer their lattice vector by summing the lattice vectors of two adjacent edges.
		reconstructIdealEdgeVectors();
	}

#if 0
	context().msgLogger() << "PROC " << parallel().processor() << ": nclusters=" << clusterGraph().clusters().size() <<
			" ntransitions=" << clusterGraph().clusterTransitions().size() << endl;

	clusterGraph().printSearchStatistics();
#endif
}

/******************************************************************************
* Tries to compute the ideal vector of tessellation edges, which are in the
* disordered region of the crystal.
******************************************************************************/
void ElasticMapping::reconstructIdealEdgeVectors()
{
	// Some of the tessellation edges do not connect two neighbor atoms.
	// Then they were not assigned a lattice vector so far.
	// For these edges, try to infer their lattice vector by summing the lattice vectors of two adjacent edges.
	bool notDone;
	boost::dynamic_bitset<> deadVertices(_vertexEdges.size());
	boost::dynamic_bitset<> changedVertices(_vertexEdges.size());
	changedVertices.set();
	boost::dynamic_bitset<> newlyChangedVertices(_vertexEdges.size());
	do {
		notDone = false;

		vector<TessellationEdge*>::const_iterator vertexEdgeList = _vertexEdges.begin();
		for(int vertexIndex = 0; vertexIndex < _vertexEdges.size(); vertexIndex++, ++vertexEdgeList) {
			if(deadVertices.test(vertexIndex)) continue;
			Cluster* cluster1 = clusterOfVertex(vertexIndex);
			deadVertices.set(vertexIndex);
			if(cluster1 == NULL) continue;
			for(TessellationEdge* edge = *vertexEdgeList; edge != NULL; edge = edge->next) {
#ifndef CALIB_DETECT_INCOMPATIBLE_EDGES
				if(edge->hasClusterVector()) continue;
#else
				if(edge->isIncompatible) continue;
#endif

				Cluster* cluster2 = clusterOfVertex(edge->vertex2);
				if(!cluster2) continue;

#ifdef CALIB_AVOID_LATTICE_LATTICE_EDGES
				if(cluster2->parentCluster() != cluster1->parentCluster() && cluster1->pattern->isLattice() && cluster2->pattern->isLattice()) {
					deadVertices.reset(vertexIndex);
					continue;
				}
#endif

				if(!changedVertices.test(edge->vertex1) && !changedVertices.test(edge->vertex2)) {
					deadVertices.reset(vertexIndex);
					continue;
				}
				for(TessellationEdge* e1 = *vertexEdgeList; e1 != NULL && !edge->hasClusterVector(); e1 = e1->next) {
					if(e1->hasClusterVector() == false) continue;
					CALIB_ASSERT(e1 != edge);
					for(TessellationEdge* e2 = _vertexEdges[e1->vertex2]; e2 != NULL; e2 = e2->next) {
						if(e2->hasClusterVector() == false) continue;
						if(e2->vertex2 == edge->vertex2) {
							CALIB_ASSERT(e1->clusterTransition->cluster2 == e2->clusterTransition->cluster1);
							ClusterTransition* transition = clusterGraph().concatenateClusterTransitions(e1->clusterTransition, e2->clusterTransition);
							CALIB_ASSERT(transition != NULL);
							Vector3 clusterVector = e1->clusterVector + e1->clusterTransition->reverseTransform(e2->clusterVector);
#ifdef CALIB_DETECT_INCOMPATIBLE_EDGES
							if(transition == edge->clusterTransition && !(clusterVector - edge->clusterVector).isZero(CA_LATTICE_VECTOR_EPSILON)) {
								edge->isIncompatible = true;
								edge->reverse->isIncompatible = true;
								edge->clearClusterVector();
								break;
							}
#endif
							edge->assignClusterVector(clusterVector, transition);
							CALIB_ASSERT(edge->clusterTransition->cluster1 == clusterOfVertex(e1->vertex1));
							CALIB_ASSERT(edge->clusterTransition->cluster2 == clusterOfVertex(e2->vertex2));
							newlyChangedVertices.set(edge->vertex1);
							newlyChangedVertices.set(edge->vertex2);
							changedVertices.set(edge->vertex1);
							changedVertices.set(edge->vertex2);
							notDone = true;
							break;
						}
					}
				}
				if(!edge->hasClusterVector())
					deadVertices.reset(vertexIndex);
			}
		}
		changedVertices.swap(newlyChangedVertices);
		if(notDone) newlyChangedVertices.reset();
	}
	while(notDone);
}


/******************************************************************************
* Determines whether the elastic mapping from the actual physical configuration
* of the crystal to the imaginary, stress-free configuration is compatible
* inside the given tessellation cell. Returns false if the mapping is incompatible
* or cannot be determined at all.
******************************************************************************/
bool ElasticMapping::isElasticMappingCompatible(DelaunayTessellation::CellHandle cell) const
{
	// Must be valid tessellation cell to determine the mapping.
	if(tessellation().isValidCell(cell) == false)
		return false;

	// Retrieve the six edges of the tetrahedron.
	// All must have a cluster vector assigned to them.
	TessellationEdge* edges[6];
	for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
		int vertex1 = cell->vertex(edgeVertices[edgeIndex][0])->info();
		int vertex2 = cell->vertex(edgeVertices[edgeIndex][1])->info();
		CALIB_ASSERT(vertex1 != -1 && vertex2 != -1);
		edges[edgeIndex] = findEdge(vertex1, vertex2);
		if(edges[edgeIndex] == NULL || edges[edgeIndex]->hasClusterVector() == false)
			return false;
	}

	static const int circuits[4][3] = { {0,4,2}, {1,5,2}, {0,3,1}, {3,5,4} };

	// Perform the Burgers circuit test on each of the four faces of the tetrahedron.
	for(int face = 0; face < 4; face++) {
		Vector3 burgersVector = edges[circuits[face][0]]->clusterVector;
		burgersVector += edges[circuits[face][0]]->clusterTransition->reverseTransform(edges[circuits[face][1]]->clusterVector);
		burgersVector -= edges[circuits[face][2]]->clusterVector;
		if(!burgersVector.isZero(CA_LATTICE_VECTOR_EPSILON))
			return false;
	}

	// Perform disclination test on each of the four faces.
	for(int face = 0; face < 4; face++) {
		ClusterTransition* t1 = edges[circuits[face][0]]->clusterTransition;
		ClusterTransition* t2 = edges[circuits[face][1]]->clusterTransition;
		ClusterTransition* t3 = edges[circuits[face][2]]->clusterTransition;
		if(!t1->isSelfTransition() || !t2->isSelfTransition() || !t3->isSelfTransition()) {
			Matrix3 frankRotation = t3->reverse->tm * t2->tm * t1->tm;
			if(!(frankRotation - Matrix3::Identity()).isZero(CA_TRANSITION_MATRIX_EPSILON))
				return false;
		}
	}

	return true;
}

/******************************************************************************
* Computes the elastic deformation gradient within the given cell.
* Return NULL if the gradient could not be determined. Otherwise returns
* the atomic cluster whose frame of reference served as the relaxed configuration
* to compute F_elastic.
******************************************************************************/
Cluster* ElasticMapping::computeElasticMapping(DelaunayTessellation::CellHandle cell, Matrix3& F_elastic) const
{
	CALIB_ASSERT(structure().neighborMode() == AtomicStructure::GHOST_ATOMS);

	Cluster* cluster;
	Matrix3 tet_x;
	Matrix3 tet_X;
	for(int eindex = 0; eindex < 3; eindex++) {
		int v1 = cell->vertex(edgeVertices[eindex][0])->info();
		int v2 = cell->vertex(edgeVertices[eindex][1])->info();
		if(v1 == -1 || v2 == -1) return NULL;
		TessellationEdge* edge = findEdge(v1, v2);
		if(edge == NULL || edge->hasClusterVector() == false)
			return NULL;
		tet_X.col(eindex) = edge->clusterVector;
		if(eindex == 0) cluster = edge->clusterTransition->cluster1;
		else CALIB_ASSERT(cluster == edge->clusterTransition->cluster1);
		tet_x.col(eindex) = structure().atomPosition(vertexToAtom(v2)) - structure().atomPosition(vertexToAtom(v1));
	}
	if(fabs(tet_X.determinant()) <= CAFLOAT_EPSILON)
		return NULL;

	F_elastic = tet_x * tet_X.inverse();
	return cluster;
}

/// \brief Defines a strict weak ordering of Vector3 objects.
inline bool vector3Compare(const Vector3I& p1, const Vector3I& p2) {
	if(p1.z() != p2.z()) return p1.z() < p2.z();
	else if(p1.y() != p2.y()) return p1.y() < p2.y();
	else return p1.x() < p2.x();
}

/**
 * This structure is used to transmit data of tessellation edges between processors.
 */
struct EdgeSenderComm {
	EdgeSenderComm() {}
	EdgeSenderComm(AtomInteger v1, AtomInteger v2, const Vector3I& pcoord, const Vector3& vec, bool hasVector, const Matrix3& _transitionMatrix, int _transitionDistance
#ifdef DEBUG_EDGES
			, const Point3& _rp
#endif
			) :
		vertexTag1(v1), vertexTag2(v2), procCoord(pcoord), clusterVector(vec), hasClusterVector(hasVector), transitionMatrix(_transitionMatrix), transitionDistance(_transitionDistance)
#ifdef DEBUG_EDGES
	, rp(_rp)
#endif
	{}
	AtomInteger vertexTag1;
	AtomInteger vertexTag2;
	Vector3I procCoord;
	Vector3 clusterVector;
	bool hasClusterVector;
	Matrix3 transitionMatrix;
	int transitionDistance;

#ifdef DEBUG_EDGES
	Vector3 rp;
#endif

	// Defines strict weak ordering of edges.
	bool operator<(const EdgeSenderComm& other) const {
		if(vertexTag1 != other.vertexTag1)
			return vertexTag1 < other.vertexTag1;
		else if(vertexTag2 != other.vertexTag2)
			return vertexTag2 < other.vertexTag2;
		else
			return vector3Compare(procCoord, other.procCoord);
	}
};

/**
 * This structure is used to transmit data of tessellation edges between processors.
 */
struct EdgeReceiverComm {
	EdgeReceiverComm(AtomInteger v1, AtomInteger v2, const Vector3I& pcoord, ElasticMapping::TessellationEdge* edge
#ifdef DEBUG_EDGES
			, const Point3& _rp
#endif
			) :
		vertexTag1(v1), vertexTag2(v2), procCoord(pcoord), original(edge)
#ifdef DEBUG_EDGES
	, rp(_rp)
#endif
	{}
	AtomInteger vertexTag1;
	AtomInteger vertexTag2;
	Vector3I procCoord;
	ElasticMapping::TessellationEdge* original;

#ifdef DEBUG_EDGES
	Vector3 rp;
#endif

	// Defines strict weak ordering of edges.
	bool operator<(const EdgeReceiverComm& other) const {
		if(vertexTag1 != other.vertexTag1)
			return vertexTag1 < other.vertexTag1;
		else if(vertexTag2 != other.vertexTag2)
			return vertexTag2 < other.vertexTag2;
		else
			return vector3Compare(procCoord, other.procCoord);
	}
};

/******************************************************************************
* Exchanges the ideal vectors assigned to tessellation edges between processors.
* The synchronization is performed only for edges of
* tetrahedra that overlap with more than one processor domain.
******************************************************************************/
void ElasticMapping::synchronize()
{
	CALIB_ASSERT(parallel().isParallelMode());
	CALIB_ASSERT(!hasVertexToAtomMapping());

	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

		// The outcode for the current positive direction.
		int posOutcode = 1 << (dim*2);
		// The outcode for the current negative direction.
		int negOutcode = posOutcode << 1;
		// The outcode for both positive and negative direction.
		int posOrNegOutcode = posOutcode | negOutcode;

		// Generate lists of tessellation edges that need to be exchanged with the two neighboring processors.
		set<EdgeSenderComm> toBeSentEdges[2];
		set<EdgeReceiverComm> toBeReceivedEdges[2];
		for(DelaunayTessellation::CellIterator cell = tessellation().begin_cells(); cell != tessellation().end_cells(); ++cell) {

			// Skip invalid cells (those not connecting four physical atoms).
			if(tessellation().isValidCell(cell) == false)
				continue;

			// Compute outcodes for the current tetrahedron.
			pair<int,int> outCodes = computeCellOutcodes(cell);

			// Skip tetrahedra that are completely on one side of the processor domain.
			if(outCodes.first != 0)
				continue;

			// Don't send/receive tetrahedron if it does not overlap with any neighboring processor domains.
			if((outCodes.second & posOrNegOutcode) == 0)
				continue;

			// Check if both positive and negative outcode bits are set at the same time.
			if((outCodes.second & posOrNegOutcode) == posOrNegOutcode)
				context().raiseErrorOne("Detected invalid tessellation. Tetrahedron overlaps with more than two processor domains in a row.");

			// Gather vertex tags.
			int vertexIndices[4];
			AtomInteger vertexTags[4];
			for(int v = 0; v < 4; v++) {
				vertexIndices[v] = cell->vertex(v)->info();
				vertexTags[v] = structure().atomTag(vertexIndices[v]);
			}

			// Add edges of the shared tetrahedron to the send lists.
			for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
				int vertex1 = vertexIndices[edgeVertices[edgeIndex][0]];
				int vertex2 = vertexIndices[edgeVertices[edgeIndex][1]];
				AtomInteger tag1 = vertexTags[edgeVertices[edgeIndex][0]];
				AtomInteger tag2 = vertexTags[edgeVertices[edgeIndex][1]];
				TessellationEdge* edge = findEdge(vertex1, vertex2);
				CALIB_ASSERT(edge != NULL);

				if(tag1 > tag2) {
					swap(tag1, tag2);
					edge = edge->reverse;
				}

				const Vector3I& procCoord = structure().atomProcCoord(edge->vertex1);
				if(procCoord[dim] == parallel().processorLocation(dim)) {
					// We are sending this edge.
					if(outCodes.second & negOutcode) {
						toBeSentEdges[0].insert(EdgeSenderComm(tag1, tag2, procCoord, edge->clusterVector, edge->hasClusterVector(),
							edge->hasClusterVector() ? edge->clusterTransition->tm : Matrix3::Identity(),
									edge->hasClusterVector() ? edge->clusterTransition->distance : 0
#ifdef DEBUG_EDGES
							, structure().absoluteToReduced(structure().atomPosition(edge->vertex1))
#endif
											));
					}
					else {
						CALIB_ASSERT(outCodes.second & posOutcode);
						toBeSentEdges[1].insert(EdgeSenderComm(tag1, tag2, procCoord, edge->clusterVector, edge->hasClusterVector(),
							edge->hasClusterVector() ? edge->clusterTransition->tm : Matrix3::Identity(),
									edge->hasClusterVector() ? edge->clusterTransition->distance : 0
#ifdef DEBUG_EDGES
							, structure().absoluteToReduced(structure().atomPosition(edge->vertex1))
#endif
											));
					}
				}
				else  {
					// We are receiving the edge.
					if(procCoord[dim] == parallel().processorLocation(dim) - 1) {
						CALIB_ASSERT(outCodes.second & negOutcode);
						toBeReceivedEdges[0].insert(EdgeReceiverComm(tag1, tag2, procCoord, edge
#ifdef DEBUG_EDGES
								, structure().absoluteToReduced(structure().atomPosition(edge->vertex1))
#endif
								));
					}
					else {
						CALIB_ASSERT(outCodes.second & posOutcode);
						CALIB_ASSERT(procCoord[dim] == parallel().processorLocation(dim) + 1);
						toBeReceivedEdges[1].insert(EdgeReceiverComm(tag1, tag2, procCoord, edge
#ifdef DEBUG_EDGES
								, structure().absoluteToReduced(structure().atomPosition(edge->vertex1))
#endif
								));
					}
				}
			}
		}

		for(int dir = 0; dir <= 1; dir++) {
			int oppositeDir = dir ? 0 : 1;

			// Copy list of edges to be sent into an MPI-compatible linear memory buffer.
			vector<EdgeSenderComm> sendBuffer(toBeSentEdges[dir].begin(), toBeSentEdges[dir].end());

#ifdef DEBUG_EDGES
			BOOST_FOREACH(const EdgeSenderComm& edge, sendBuffer) {
				if(edge.vertexTag1 == DEBUG_EDGES_V1 && edge.vertexTag2 == DEBUG_EDGES_V2) {
					if(edge.hasClusterVector)
						cout << "SENDING DIM " << dim << " DIR " << dir << " PROC " << parallel().processor() << " **Sending edge " << edge.vertexTag1 << "-" << edge.vertexTag2 << " to proc " << parallel().processorNeighbor(dim, dir) << " proccoord=" << edge.procCoord <<
						" pos=" << edge.rp << " vec=" <<
								edge.clusterVector << " transition:" << endl << edge.transitionMatrix;
					else
						cout << "SENDING DIM " << dim << " DIR " << dir << " PROC " << parallel().processor() << " **Sending edge " << edge.vertexTag1 << "-" << edge.vertexTag2 << " to proc " << parallel().processorNeighbor(dim, dir) << " proccoord=" << edge.procCoord <<
						" pos=" << edge.rp << " vec=NONE" << endl;
				}
			}
#endif

			// Send/receive edge list.
			vector<EdgeSenderComm> receiveBuffer;
			parallel().exchangeWithSize(dim, dir, structure().pbc(dim), sendBuffer, receiveBuffer);

#ifdef DEBUG_EDGES
			parallel().barrier();
#endif

			if(parallel().shouldSendOrReceive(dim, oppositeDir, structure().pbc(dim))) {
				if(receiveBuffer.size() != toBeReceivedEdges[oppositeDir].size())
					context().raiseErrorOne("Distributed tessellation is invalid. Overlapping tetrahedra are not consistent. Expected %i edges but received %i edges from proc %i (dim=%i).", toBeReceivedEdges[oppositeDir].size(), receiveBuffer.size(), parallel().processorNeighbor(dim, oppositeDir), dim);

				set<EdgeReceiverComm>::const_iterator toBeReceivedEdge = toBeReceivedEdges[oppositeDir].begin();
				vector<EdgeSenderComm>::const_iterator receivedEdge = receiveBuffer.begin();
				for(; receivedEdge != receiveBuffer.end(); ++receivedEdge, ++toBeReceivedEdge) {
					if(receivedEdge->vertexTag1 != toBeReceivedEdge->vertexTag1 ||
							receivedEdge->vertexTag2 != toBeReceivedEdge->vertexTag2) {
						context().raiseErrorOne("Distributed tessellation is invalid. Overlapping tetrahedra are not consistent. Edge vertices do not match.");
					}
#ifdef DEBUG_EDGES
					if(receivedEdge->vertexTag1 == DEBUG_EDGES_V1 && receivedEdge->vertexTag2 == DEBUG_EDGES_V2) {
						if(receivedEdge->hasClusterVector)
							cout << "RECV DIM " << dim << " DIR " << dir << " PROC " << parallel().processor() << " ** Received edge " << receivedEdge->vertexTag1 << "-" << receivedEdge->vertexTag2 << " from proc " <<
								parallel().processorNeighbor(dim, oppositeDir) << " proccoord=" <<
								receivedEdge->procCoord << "->" << toBeReceivedEdge->procCoord << " pos=" << receivedEdge->rp << "->" << toBeReceivedEdge->rp << " vec=" <<
								receivedEdge->clusterVector << " transition:" << endl << receivedEdge->transitionMatrix;
						else
							cout << "RECV DIM " << dim << " DIR " << dir << " PROC " << parallel().processor() << " **Received edge " << receivedEdge->vertexTag1 << "-" << receivedEdge->vertexTag2 <<
								" from proc " << parallel().processorNeighbor(dim, oppositeDir) << " proccoord=" << receivedEdge->procCoord << "->" <<
								toBeReceivedEdge->procCoord << " pos=" << receivedEdge->rp << "->" << toBeReceivedEdge->rp << " vec=NONE" << endl;
					}
#endif

					if(receivedEdge->hasClusterVector) {
						Cluster* cluster1 = clusterOfVertex(toBeReceivedEdge->original->vertex1);
						Cluster* cluster2 = clusterOfVertex(toBeReceivedEdge->original->vertex2);
						CALIB_ASSERT(cluster1 && cluster2);
						CALIB_ASSERT(receivedEdge->transitionDistance >= 1 || cluster1 == cluster2);
						ClusterTransition* transition = clusterGraph().createClusterTransition(cluster1, cluster2, receivedEdge->transitionMatrix, receivedEdge->transitionDistance);
						toBeReceivedEdge->original->assignClusterVector(receivedEdge->clusterVector, transition);
					}
					else {
						toBeReceivedEdge->original->clearClusterVector();
						CALIB_ASSERT(toBeReceivedEdge->original->hasClusterVector() == false);
					}
				}
			}
		}
	}

#if defined(DEBUG_CRYSTAL_ANALYSIS) and !defined(NDEBUG)
	performConsistencyCheck();
#endif
}

/// Structure used to exchange tessellation tetrahedra between processors.
struct TetrahedronComm {
	AtomInteger vertexTags[4];
	int isGood;
	Vector3 positions[4];
	Vector3 edgeVectors[6];
	AtomInteger edgeVertices[6][2];
	ClusterId cluster1[6];
	ClusterId cluster2[6];
	Matrix3 clusterTransitions[6];
	Vector3 edgeVerticesPos[6][2];

	bool operator<(const TetrahedronComm& other) const {
		for(int v = 0; v < 4; v++) {
			if(vertexTags[v] < other.vertexTags[v])
				return true;
			else if(vertexTags[v] > other.vertexTags[v])
				return false;
		}
		return false;
	}
};

struct TetCommEdgeOrder {
	TetCommEdgeOrder(const TetrahedronComm& _t) : t(_t) {}
	const TetrahedronComm& t;
	bool operator()(int i, int j) const {
		if(t.edgeVertices[i][0] != t.edgeVertices[j][0])
			return t.edgeVertices[i][0] < t.edgeVertices[j][0];
		else
			return t.edgeVertices[i][1] < t.edgeVertices[j][1];
	}
};

/******************************************************************************
* After synchronizing the elastic mapping across processors,
* this method performs an additional comparison to check whether
* the data on all processors is consistent.
******************************************************************************/
void ElasticMapping::performConsistencyCheck()
{
	CALIB_ASSERT(!hasVertexToAtomMapping());

	context().msgLogger() << "Performing consistency check." << endl;

	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

		// Build list of tetrahedra that must be sent to the neighboring processors.
		// We exchange tetrahedra that at least one vertex inside the local domain and one vertex
		// in the neighboring processor's domain.
		vector<TetrahedronComm> sendBuffers[2];

		// Determine the grid coordinates of the two neighboring processors in the current spatial dimension.
		Vector3I neighborProcCoords[2];
		neighborProcCoords[0] = parallel().processorLocationPoint();
		neighborProcCoords[1] = parallel().processorLocationPoint();
		neighborProcCoords[0][dim] -= 1;
		neighborProcCoords[1][dim] += 1;

		for(DelaunayTessellation::CellIterator cell = tessellation().begin_cells(); cell != tessellation().end_cells(); ++cell) {

			// Iterate over the four vertices of the tetrahedron cell.
			bool hasInsideVertex = false;
			bool hasInfiniteVertex = false;
			bool hasOutsideVertex[2] = { false, false };
			bool hasHelperVertex = false;
			TetrahedronComm tetComm;
			for(int v = 0; v < 4; v++) {

				// Skip the infinite vertex.
				if(tessellation().isInfiniteVertex(cell->vertex(v))) {
					hasInfiniteVertex = true;
					continue;
				}

				// Determine the processor domain the vertex is located in.
				int atomIndex = cell->vertex(v)->info();
				if(atomIndex == -1) {
					hasHelperVertex = true;
					continue;
				}

				const Vector3I& procCoord = structure().atomProcCoord(atomIndex);
				if(procCoord == parallel().processorLocationPoint())
					hasInsideVertex = true;
				else if(procCoord == neighborProcCoords[0])
					hasOutsideVertex[0] = true;
				else if(procCoord == neighborProcCoords[1])
					hasOutsideVertex[1] = true;

				tetComm.vertexTags[v] = structure().atomTag(atomIndex);
				tetComm.positions[v] = structure().atomPosition(atomIndex);
			}
			for(int e = 0; e < 6; e++) {
				tetComm.edgeVectors[e] = Vector3(1e10,1e10,1e10);
				tetComm.edgeVertices[e][0] = tetComm.edgeVertices[e][1] = -1;
				tetComm.cluster1[e] = ClusterId(-1,-1);
				tetComm.cluster2[e] = ClusterId(-1,-1);
				tetComm.clusterTransitions[e].setIdentity();
				int atom1 = cell->vertex(edgeVertices[e][0])->info();
				int atom2 = cell->vertex(edgeVertices[e][1])->info();
				if(atom1 == -1 || atom2 == -1) continue;
				tetComm.edgeVertices[e][0] = structure().atomTag(atom1);
				tetComm.edgeVertices[e][1] = structure().atomTag(atom2);
				tetComm.edgeVerticesPos[e][0] = structure().absoluteToReducedPoint(structure().atomPosition(atom1));
				tetComm.edgeVerticesPos[e][1] = structure().absoluteToReducedPoint(structure().atomPosition(atom2));
				TessellationEdge* edge = findEdge(atom1, atom2);
				if(edge == NULL) continue;
				if(structure().atomTag(atom1) > structure().atomTag(atom2)) {
					swap(atom1, atom2);
					edge = edge->reverse;
				}
				tetComm.edgeVertices[e][0] = structure().atomTag(atom1);
				tetComm.edgeVertices[e][1] = structure().atomTag(atom2);
				tetComm.edgeVerticesPos[e][0] = structure().absoluteToReducedPoint(structure().atomPosition(atom1));
				tetComm.edgeVerticesPos[e][1] = structure().absoluteToReducedPoint(structure().atomPosition(atom2));
				if(edge->hasClusterVector()) {
					tetComm.edgeVectors[e] = edge->clusterVector;
					tetComm.cluster1[e] = edge->clusterTransition->cluster1->id;
					tetComm.cluster2[e] = edge->clusterTransition->cluster2->id;
					tetComm.clusterTransitions[e] = edge->clusterTransition->tm;
				}
			}
			if(hasInsideVertex && hasInfiniteVertex) {
				context().raiseErrorOne("Detected invalid tessellation. Infinite tetrahedron intersects processor domain.");
			}

			if(hasOutsideVertex[0] && hasOutsideVertex[1])
				context().raiseErrorOne("Detected invalid tessellation. Tetrahedron overlaps with more than two processor domains in one dimension.");

			if(hasHelperVertex == false) {
				if(hasInsideVertex && hasOutsideVertex[0]) {
					tetComm.isGood = isElasticMappingCompatible(cell);
					sort(tetComm.vertexTags, tetComm.vertexTags + 4);
					sendBuffers[0].push_back(tetComm);
				}
				else if(hasInsideVertex && hasOutsideVertex[1]) {
					tetComm.isGood = isElasticMappingCompatible(cell);
					sort(tetComm.vertexTags, tetComm.vertexTags + 4);
					sendBuffers[1].push_back(tetComm);
				}
			}
		}

		sort(sendBuffers[0].begin(), sendBuffers[0].end());
		sort(sendBuffers[1].begin(), sendBuffers[1].end());

		for(int dir = 0; dir <= 1; dir++) {
			const int oppositeDir = dir ? 0 : 1;

			// Communicate number of tetrahedra to be exchanged with neighbor processor.
			int tetReceiveBufferSize = 0;
			parallel().exchange(dim, dir, structure().pbc(dim), sendBuffers[dir].size(), tetReceiveBufferSize);

			if(parallel().shouldSendOrReceive(dim, oppositeDir, structure().pbc(dim))) {
				// Number of tetrahedra must match.
				if(tetReceiveBufferSize != sendBuffers[oppositeDir].size()) {
					//context().msgLogger() << "Processor " << parallel().processor() << " dim=" << dim << " dir=" << oppositeDir << "   Neighbor processors " << context.processorNeighbor(dim, oppositeDir) << " dir=" << dir << endl;
					//context().msgLogger() << "tetReceiveBufferSize=" << tetReceiveBufferSize << "  sendBuffers[oppositeDir].size()=" << sendBuffers[oppositeDir].size() << endl;
					context().raiseErrorOne("Detected invalid tessellation. Numbers of overlapping tetrahedra differ.");
				}
			}

			// Exchange overlap tetrahedra.
			vector<TetrahedronComm> tetReceiveBuffer(tetReceiveBufferSize);
			parallel().exchange(dim, dir, structure().pbc(dim), sendBuffers[dir], tetReceiveBuffer);

			// Check if all received tetrahedra are identical to our own tetrahedra.
			vector<TetrahedronComm>::const_iterator localTet = sendBuffers[oppositeDir].begin();
			for(vector<TetrahedronComm>::const_iterator remoteTet = tetReceiveBuffer.begin(); remoteTet != tetReceiveBuffer.end(); ++remoteTet, ++localTet) {

				for(int v = 0; v < 4; v++) {
					if(localTet->vertexTags[v] != remoteTet->vertexTags[v]) {
						//context().msgLogger() << "Processor " << parallel().processor() << " dim=" << dim << " dir=" << oppositeDir << "   Neighbor processors " << context.processorNeighbor(dim, oppositeDir) << " dir=" << dir << endl;
						//context().msgLogger() << "tetReceiveBufferSize=" << tetReceiveBufferSize << "  sendBuffers[oppositeDir].size()=" << sendBuffers[oppositeDir].size() << endl;
						context().raiseErrorOne("Detected invalid tessellation. Overlapping tetrahedra are not consistent.");
					}
				}

				if(localTet->isGood != remoteTet->isGood) {
					cout << endl;
					cout << "PROC " << parallel().processor() << ": localTet->isGood=" << localTet->isGood << "  remoteTet->isGood=" << remoteTet->isGood << endl;
					int localOrder[6] = {0,1,2,3,4,5};
					std::sort(localOrder, localOrder+6, TetCommEdgeOrder(*localTet));
					for(int e = 0; e < 6; e++) {
						int i = localOrder[e];
						cout << "PROC " << parallel().processor() << ": Edge " << e << " local(" << localTet->edgeVertices[i][0] << "," << localTet->edgeVertices[i][1] << ") " <<
								"Transition: " << localTet->cluster1[i] << "->" << localTet->cluster2[i] << " " <<
								"ClusterVec: " << localTet->edgeVectors[i] <<
								" " << localTet->edgeVerticesPos[i][0] << "->" << localTet->edgeVerticesPos[i][1] << endl;
					}
					int remoteOrder[6] = {0,1,2,3,4,5};
					std::sort(remoteOrder, remoteOrder+6, TetCommEdgeOrder(*remoteTet));
					for(int e = 0; e < 6; e++) {
						int i = remoteOrder[e];
						cout << "REMOTE PROC " << parallel().processorNeighbor(dim, dir ? 0 : 1) << ": Edge " << e << " remote(" << remoteTet->edgeVertices[i][0] << "," <<
								remoteTet->edgeVertices[i][1] << ") " <<
								"Transition: " << remoteTet->cluster1[i] << "->" << remoteTet->cluster2[i] << " " <<
								"ClusterVec: " << remoteTet->edgeVectors[i] <<
								" " << remoteTet->edgeVerticesPos[i][0] << "->" << remoteTet->edgeVerticesPos[i][1] << endl;
					}
					cout << "Combined transitions:" << endl;
					for(int e = 0; e < 6; e++) {
						Matrix3 tm = localTet->clusterTransitions[localOrder[e]]*remoteTet->clusterTransitions[remoteOrder[e]].inverse();
						cout << "Transition " << e << ": " << endl << tm;
					}
					cout << "Local transitions:" << endl;
					for(int e = 0; e < 6; e++) {
						Matrix3 tm = localTet->clusterTransitions[localOrder[e]];
						cout << "Transition " << e << ": " << endl << tm;
					}
					cout << "Remote transitions:" << endl;
					for(int e = 0; e < 6; e++) {
						Matrix3 tm = remoteTet->clusterTransitions[remoteOrder[e]];
						cout << "Transition " << e << ": " << endl << tm;
					}
					cout << "PROC " << parallel().processor() << ": dim=" << dim << " dir=" << oppositeDir << "   Neighbor processor " << parallel().processorNeighbor(dim, oppositeDir) << " dir=" << dir << endl;
					context().raiseErrorOne("Detected invalid tessellation. Overlapping tetrahedra are not in the same region.");
				}
			}
		}
	}
}


}; // End of namespace
