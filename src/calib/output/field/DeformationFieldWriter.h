///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_DEFORM_FIELD_WRITER_H
#define __CA_DEFORM_FIELD_WRITER_H

#include "../../CALib.h"
#include "FieldWriter.h"
#include "../../context/CAContext.h"
#include "../../deformation/DeformationField.h"

namespace CALib {

/**
 * Writes the computed deformation field to an output file for visualization.
 */
class DeformationFieldWriter : public FieldWriter
{
public:

	enum FieldComponents {
		CELL_VOLUME,
		CELL_ASPECT_RATIO,
		F_11, F_12, F_13,
		F_21, F_22, F_23,
		F_31, F_32, F_33,
		CELL_HAS_PLASTIC_F,
		F_plastic_11, F_plastic_12, F_plastic_13,
		F_plastic_21, F_plastic_22, F_plastic_23,
		F_plastic_31, F_plastic_32, F_plastic_33,
		PLASTIC_SHEAR_STRAIN,
		NUM_COMPONENTS
	};

public:

	/// Constructor.
	DeformationFieldWriter(const DelaunayTessellation& tessellation) : FieldWriter(tessellation) {}

	/// Outputs the given components of the deformation field to a VTK file.
	void writeVTKFile(std::ostream& stream, const DeformationField& field, const std::bitset<NUM_COMPONENTS>& components);
};

}; // End of namespace

#endif // __CA_DEFORM_FIELD_WRITER_H
