///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_FLOATTYPE_H
#define __CRYSTAL_ANALYSIS_FLOATTYPE_H

#include "../CALib.h"

namespace CALib {

// Define the number type used for numerical computations.
#ifndef CALIB_USE_DOUBLE_PRECISION_FP

	// Use 32-bit float as default floating point type.
	typedef float CAFloat;
	#define FLOATTYPE_FLOAT		// This tells the program that we're using 32-bit floating point.

	// These are the format strings used with the sscanf() parsing function.
	// We have to obey the floating point precision used to compile the program,
	// because the destination variables are either single or double precision variables.
	#define CAFLOAT_SCANF_STRING_1   "%g"
	#define CAFLOAT_SCANF_STRING_2   "%g %g"
	#define CAFLOAT_SCANF_STRING_3   "%g %g %g"

	// The data type used for message passing of CAFloat.
	#define MPI_FLOATTYPE MPI_FLOAT

#else

	// Use 64-bit double as default floating point type.
	typedef double CAFloat;
	#define FLOATTYPE_DOUBLE	// This tells the program that we're using 64-bit floating point.

	// These are the format strings used with the sscanf() parsing function.
	// We have to obey the floating point precision used to compile the program,
	// because the destination variables are either single or double precision variables.
	#define CAFLOAT_SCANF_STRING_1   "%lg"
	#define CAFLOAT_SCANF_STRING_2   "%lg %lg"
	#define CAFLOAT_SCANF_STRING_3   "%lg %lg %lg"

	// The data type used for message passing of CAFloat.
	#define MPI_FLOATTYPE MPI_DOUBLE

#endif

/// A small epsilon value for the CAFloat.
#define CAFLOAT_EPSILON	((CAFloat)1e-6)

/// Computes the square of a number.
template<typename T> inline T square(const T& f) { return f*f; }

/// Clamps a value to the given [low,high] interval and returns the clamped value.
template<typename T> T clamped(const T& value, const T& low, const T& high) {
	return value < low ? low : (value > high ? high : value);
}

/// Clamps a value in-place to be in the given [low,high] interval.
template<typename T> void clamp(T& value, const T& low, const T& high) {
	if(value < low) value = low;
	else if(value > high) value = high;
}

/// The maximum value for floating point variables.
#define CAFLOAT_MAX	std::numeric_limits<CAFloat>::max()

// The Pi constant
#define CAFLOAT_PI boost::math::constants::pi<CAFloat>()

}; // End of namespace

#endif	// __CRYSTAL_ANALYSIS_FLOATTYPE_H
