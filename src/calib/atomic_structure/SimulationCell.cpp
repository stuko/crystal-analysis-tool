///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "SimulationCell.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor.
******************************************************************************/
SimulationCell::SimulationCell(CAContext& context) : ContextReference(context)
{
	_timestep = 0;
	_pbcFlags[0] = _pbcFlags[1] = _pbcFlags[2] = true;
	_simulationCell = AffineTransformation::Identity();
}

/******************************************************************************
* Calculates the shift vector that must be subtracted from point B to bring it close to point A such that
* the vector (B-A) is not a wrapped vector.
******************************************************************************/
Vector3 SimulationCell::calculateShiftVector(const Vector3& a, const Vector3& b) const
{
	Vector3 d = absoluteToReducedVector(b - a);
	d.x() = _pbcFlags[0] ? floor(d.x() + CAFloat(0.5)) : CAFloat(0);
	d.y() = _pbcFlags[1] ? floor(d.y() + CAFloat(0.5)) : CAFloat(0);
	d.z() = _pbcFlags[2] ? floor(d.z() + CAFloat(0.5)) : CAFloat(0);
	return reducedToAbsoluteVector(d);
}

/******************************************************************************
 * Determines whether the given vector is so long such that it would
 * be wrapped at periodic boundaries (according to the minimum image convention).
******************************************************************************/
bool SimulationCell::isWrappedVector(const Vector3& v) const
{
	// Transform vector to reduced cell coordinates.
	Vector3 rv = _reciprocalSimulationCell.linear() * v;
	if(_pbcFlags[0] && fabs(rv.x()) > CAFloat(0.5)) return true;
	if(_pbcFlags[1] && fabs(rv.y()) > CAFloat(0.5)) return true;
	if(_pbcFlags[2] && fabs(rv.z()) > CAFloat(0.5)) return true;
	return false;
}

/******************************************************************************
 * Determines whether the given vector (in reduced coordinates) is so long such
 * that it would be wrapped at periodic boundaries (according to the minimum image convention).
******************************************************************************/
bool SimulationCell::isReducedWrappedVector(const Vector3& v) const
{
	// Transform vector to reduced cell coordinates.
	if(_pbcFlags[0] && fabs(v.x()) > CAFloat(0.5)) return true;
	if(_pbcFlags[1] && fabs(v.y()) > CAFloat(0.5)) return true;
	if(_pbcFlags[2] && fabs(v.z()) > CAFloat(0.5)) return true;
	return false;
}

/******************************************************************************
* Determines the periodic image of the simulation cell in which the given
* point is located.
******************************************************************************/
Vector3I SimulationCell::periodicImage(const Vector3& p) const
{
	Vector3 rp = absoluteToReducedPoint(p);
	return Vector3I(_pbcFlags[0] ? (int)floor(rp.x()) : 0,
					_pbcFlags[1] ? (int)floor(rp.y()) : 0,
					_pbcFlags[2] ? (int)floor(rp.z()) : 0);
}

/******************************************************************************
* Determines the processor domain that contains the given world-space point.
******************************************************************************/
Vector3I SimulationCell::absoluteToProcessorGrid(const Vector3& pos, bool clamp) const
{
	Vector3 procCoord = _absoluteToProcGridTM * pos;
	Vector3I procCoordi;
	for(int dim = 0; dim < 3; dim++) {
		procCoordi[dim] = (int)floor(procCoord[dim]);
		if(clamp) {
			if(procCoordi[dim] < 0) {
				CALIB_ASSERT(procCoordi[dim] == -1);
				procCoordi[dim] = 0;
			}
			else if(procCoordi[dim] >= context().processorGrid(dim)) {
				CALIB_ASSERT(procCoordi[dim] == context().processorGrid(dim));
				procCoordi[dim] = context().processorGrid(dim) - 1;
			}
		}
	}
	return procCoordi;
}

/******************************************************************************
* Converts a position in the processor grid to an absolute coordinate.
* No wrapping is performed.
******************************************************************************/
Vector3 SimulationCell::processorGridToAbsolute(const Vector3I& pos) const
{
	return _procGridToAbsoluteTM * pos.cast<CAFloat>();
}

/******************************************************************************
* Sets the simulation cell geometry and boundary conditions.
******************************************************************************/
void SimulationCell::setSimulationCell(const AffineTransformation& cellMatrix, const boost::array<bool,3>& pbc)
{
	_simulationCell = cellMatrix;

	// Detect degenerate simulation cells.
	if(cellVolume() <= CAFLOAT_EPSILON)
		context().raiseErrorAll("Degenerate simulation cell. Simulation cell volume must be positive.");

	_reciprocalSimulationCell = _simulationCell.inverse();
	_pbcFlags = pbc;
	_boundingBox = _simulationCell;
}

/******************************************************************************
* Sets up the simulation cell and domain decomposition.
******************************************************************************/
void SimulationCell::setupSimulationCellAndDecomposition(AffineTransformation simulationCell, boost::array<bool,3> pbc, bool broadcastInfo)
{
	if(broadcastInfo) {
		context().broadcast(simulationCell);
		context().broadcast(pbc);
	}

	setSimulationCell(simulationCell, pbc);

	// Set up processor grid.
	context().setupProcessorGrid(simulationCell.linear());

	// Set up matrix that transforms absolute coordinates to processor grid coordinates.
	_absoluteToProcGridTM = Eigen::DiagonalMatrix<CAFloat,3>(context().processorGrid(0), context().processorGrid(1), context().processorGrid(2)) *
			_reciprocalSimulationCell;

	// Also store inverse transformation.
	_procGridToAbsoluteTM = _absoluteToProcGridTM.inverse();

	// Compute geometry of the local processor's domain.
	_processorDomain = _simulationCell *
		Eigen::DiagonalMatrix<CAFloat,3>(
			CAFloat(1) / parallel().processorGrid(0),
			CAFloat(1) / parallel().processorGrid(1),
			CAFloat(1) / parallel().processorGrid(2)) *
		Eigen::Translation<CAFloat,3>(parallel().processorLocationPoint().cast<CAFloat>());
}

}; // End of namespace
