// Convert a LAMMPS binary restart file into a compact binary file
//
// Syntax: compact_restart restart-file output-file
//
// this serial code must be compiled on a platform that can read the binary
//   restart file since binary formats are not compatible across all platforms
// restart-file can have a '%' character to indicate a multiproc restart
//   file as written by LAMMPS

#include "math.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))

#define MAX_GROUP 32

// same as write_restart.cpp

enum{VERSION,UNITS,NTIMESTEP,DIMENSION,NPROCS,PROCGRID_0,PROCGRID_1,PROCGRID_2,
       NEWTON_PAIR,NEWTON_BOND,XPERIODIC,YPERIODIC,ZPERIODIC,
       BOUNDARY_00,BOUNDARY_01,BOUNDARY_10,BOUNDARY_11,BOUNDARY_20,BOUNDARY_21,
       ATOM_STYLE,NATOMS,NTYPES,
       NBONDS,NBONDTYPES,BOND_PER_ATOM,
       NANGLES,NANGLETYPES,ANGLE_PER_ATOM,
       NDIHEDRALS,NDIHEDRALTYPES,DIHEDRAL_PER_ATOM,
       NIMPROPERS,NIMPROPERTYPES,IMPROPER_PER_ATOM,
       BOXLO_0,BOXHI_0,BOXLO_1,BOXHI_1,BOXLO_2,BOXHI_2,
       SPECIAL_LJ_1,SPECIAL_LJ_2,SPECIAL_LJ_3,
       SPECIAL_COUL_1,SPECIAL_COUL_2,SPECIAL_COUL_3,
       XY,XZ,YZ};
enum{MASS,SHAPE,DIPOLE};
enum{PAIR,BOND,ANGLE,DIHEDRAL,IMPROPER};

static const char * const cg_type_list[] =
  {"none", "lj9_6", "lj12_4", "lj12_6"};

// ---------------------------------------------------------------------
// Data class to hold problem
// ---------------------------------------------------------------------

class Data {
 public:

	// global settings

	  char *version;
	  int ntimestep;
	  int nprocs;
	  char *unit_style;
	  int dimension;
	  int px,py,pz;
	  int newton_pair,newton_bond;
	  int xperiodic,yperiodic,zperiodic;
	  int boundary[3][2];

	  char *atom_style;
	  int style_angle,style_atomic,style_bond,style_charge,style_dipole;
	  int style_dpd,style_ellipsoid,style_full,style_granular;
	  int style_hybrid,style_molecular,style_peri;

	  int natoms,nbonds,nangles,ndihedrals,nimpropers;
	  int ntypes,nbondtypes,nangletypes,ndihedraltypes,nimpropertypes;
	  int bond_per_atom,angle_per_atom,dihedral_per_atom,improper_per_atom;
	  int triclinic;

	  double xlo,xhi,ylo,yhi,zlo,zhi,xy,xz,yz;
	  double special_lj[4],special_coul[4];

	  double cut_lj_global,cut_coul_global,kappa;
	  int offset_flag,mix_flag;

	  // force fields

	  char *pair_style,*bond_style,*angle_style,*dihedral_style,*improper_style;

	  double *pair_born_A,*pair_born_rho,*pair_born_sigma;
	  double *pair_born_C,*pair_born_D;
	  double *pair_buck_A,*pair_buck_rho,*pair_buck_C;
	  double *pair_colloid_A12,*pair_colloid_sigma;
	  double *pair_colloid_d1,*pair_colloid_d2;
	  double *pair_dipole_epsilon,*pair_dipole_sigma;
	  double *pair_dpd_a0,*pair_dpd_gamma;
	  double *pair_charmm_epsilon,*pair_charmm_sigma;
	  double *pair_charmm_eps14,*pair_charmm_sigma14;
	  double *pair_class2_epsilon,*pair_class2_sigma;
	  double *pair_gb_epsilon,*pair_gb_sigma;
	  double *pair_gb_epsa,*pair_gb_epsb,*pair_gb_epsc;
	  double *pair_lj_epsilon,*pair_lj_sigma;
	  double **pair_cg_epsilon,**pair_cg_sigma;
	  int **pair_cg_cmm_type, **pair_setflag;
	  double **pair_cut_coul, **pair_cut_lj;
	  double *pair_ljexpand_epsilon,*pair_ljexpand_sigma,*pair_ljexpand_shift;
	  double *pair_ljgromacs_epsilon,*pair_ljgromacs_sigma;
	  double *pair_ljsmooth_epsilon,*pair_ljsmooth_sigma;
	  double *pair_morse_d0,*pair_morse_alpha,*pair_morse_r0;
	  double *pair_soft_A;
	  double *pair_yukawa_A;

	  double *bond_class2_r0,*bond_class2_k2,*bond_class2_k3,*bond_class2_k4;
	  double *bond_fene_k,*bond_fene_r0,*bond_fene_epsilon,*bond_fene_sigma;
	  double *bond_feneexpand_k,*bond_feneexpand_r0;
	  double *bond_feneexpand_epsilon,*bond_feneexpand_sigma;
	  double *bond_feneexpand_shift;
	  double *bond_harmonic_k,*bond_harmonic_r0;
	  double *bond_morse_d0,*bond_morse_alpha,*bond_morse_r0;
	  double *bond_nonlinear_epsilon,*bond_nonlinear_r0,*bond_nonlinear_lamda;
	  double *bond_quartic_k,*bond_quartic_b1,*bond_quartic_b2;
	  double *bond_quartic_rc,*bond_quartic_u0;

	  double *angle_charmm_k,*angle_charmm_theta0;
	  double *angle_charmm_k_ub,*angle_charmm_r_ub;
	  double *angle_class2_theta0;
	  double *angle_class2_k2,*angle_class2_k3,*angle_class2_k4;
	  double *angle_class2_bb_k,*angle_class2_bb_r1,*angle_class2_bb_r2;
	  double *angle_class2_ba_k1,*angle_class2_ba_k2;
	  double *angle_class2_ba_r1,*angle_class2_ba_r2;
	  double *angle_cosine_k;
	  double *angle_cosine_squared_k,*angle_cosine_squared_theta0;
	  double *angle_harmonic_k,*angle_harmonic_theta0;
	  double *angle_cg_cmm_epsilon,*angle_cg_cmm_sigma;
	  int *angle_cg_cmm_type;

	  double *dihedral_charmm_k,*dihedral_charmm_weight;
	  int *dihedral_charmm_multiplicity,*dihedral_charmm_sign;
	  double *dihedral_class2_k1,*dihedral_class2_k2,*dihedral_class2_k3;
	  double *dihedral_class2_phi1,*dihedral_class2_phi2,*dihedral_class2_phi3;
	  double *dihedral_class2_mbt_f1,*dihedral_class2_mbt_f2;
	  double *dihedral_class2_mbt_f3,*dihedral_class2_mbt_r0;
	  double *dihedral_class2_ebt_f1_1,*dihedral_class2_ebt_f2_1;
	  double *dihedral_class2_ebt_f3_1,*dihedral_class2_ebt_r0_1;
	  double *dihedral_class2_ebt_f1_2,*dihedral_class2_ebt_f2_2;
	  double *dihedral_class2_ebt_f3_2,*dihedral_class2_ebt_r0_2;
	  double *dihedral_class2_at_f1_1,*dihedral_class2_at_f2_1;
	  double *dihedral_class2_at_f3_1,*dihedral_class2_at_theta0_1;
	  double *dihedral_class2_at_f1_2,*dihedral_class2_at_f2_2;
	  double *dihedral_class2_at_f3_2,*dihedral_class2_at_theta0_2;
	  double *dihedral_class2_aat_k;
	  double *dihedral_class2_aat_theta0_1,*dihedral_class2_aat_theta0_2;
	  double *dihedral_class2_bb13_k;
	  double *dihedral_class2_bb13_r10,*dihedral_class2_bb13_r30;
	  double *dihedral_harmonic_k;
	  int *dihedral_harmonic_multiplicity,*dihedral_harmonic_sign;
	  double *dihedral_helix_aphi,*dihedral_helix_bphi,*dihedral_helix_cphi;
	  double *dihedral_multi_a1,*dihedral_multi_a2,*dihedral_multi_a3;
	  double *dihedral_multi_a4,*dihedral_multi_a5;
	  double *dihedral_opls_k1,*dihedral_opls_k2;
	  double *dihedral_opls_k3,*dihedral_opls_k4;

	  double *improper_class2_k0,*improper_class2_chi0;
	  double *improper_class2_aa_k1,*improper_class2_aa_k2,*improper_class2_aa_k3;
	  double *improper_class2_aa_theta0_1,*improper_class2_aa_theta0_2;
	  double *improper_class2_aa_theta0_3;
	  double *improper_cvff_k;
	  int *improper_cvff_sign,*improper_cvff_multiplicity;
	  double *improper_harmonic_k,*improper_harmonic_chi;

	  // atom quantities

	  int iatoms,ibonds,iangles,idihedrals,iimpropers;

	  double *mass,*shape,*dipole;
	  double *x,*y,*z,*vx,*vy,*vz;
	  double *omegax,*omegay,*omegaz;
	  int *tag,*type,*mask,*image;
	  int *molecule;
	  double *q,*mux,*muy,*muz,*radius,*density,*vfrac,*rmass;
	  double *s0,*x0x,*x0y,*x0z;
	  double *quatw,*quati,*quatj,*quatk,*angmomx,*angmomy,*angmomz;
	  int *bond_type,*angle_type,*dihedral_type,*improper_type;
	  int *bond_atom1,*bond_atom2;
	  int *angle_atom1,*angle_atom2,*angle_atom3;
	  int *dihedral_atom1,*dihedral_atom2,*dihedral_atom3,*dihedral_atom4;
	  int *improper_atom1,*improper_atom2,*improper_atom3,*improper_atom4;

  // functions

  Data();
  void stats();
  void write_header(FILE *fp, FILE *fp2=NULL);
};

// ---------------------------------------------------------------------
// function prototypes
// ---------------------------------------------------------------------

void header(FILE *, Data &);
void set_style(char *, Data &, int);
void groups(FILE *);
void type_arrays(FILE *, Data &);
void force_fields(FILE *, Data &);
void modify(FILE *);
void pair(FILE *fp, Data &data, char *style, int flag);
int atom(double *, Data &data, FILE* fout);

void strip_suffix(char *);

int read_int(FILE *fp);
double read_double(FILE *fp);
char *read_char(FILE *fp);

// ---------------------------------------------------------------------
// main program
// ---------------------------------------------------------------------

int main (int narg, char **arg)
{
  // process command-line args

  int iarg = 1;
  if (narg < 3 || strcmp(arg[iarg],"-h") == 0) {
    printf("Syntax: restart2dump (switch) ... "
	   "restart-file dump-file\n");
    return 0;
  }

  char *restartfile = arg[iarg];
  char *datafile = arg[iarg+1];

  // if restart file contains '%', file = filename with % replaced by "base"
  // else file = single file
  // open single restart file or base file for multiproc case

  printf("Converting restart file ...\n");

  char *ptr;
  FILE *fp;
  FILE *fpout;

  int multiproc = 0;
  if ( (ptr = strchr(restartfile,'%')) ) {
    multiproc = 1;
    char *basefile = new char[strlen(restartfile) + 16];
    *ptr = '\0';
    sprintf(basefile,"%s%s%s",restartfile,"base",ptr+1);
    fp = fopen(basefile,"rb");
    if (fp == NULL) {
      printf("ERROR: Cannot open restart file %s\n",basefile);
      return 1;
    }
  } else {
    fp = fopen(restartfile,"rb");
    if (fp == NULL) {
      printf("ERROR: Cannot open restart file %s\n",restartfile);
      return 1;
    }
  }

  // read beginning of restart file

  Data data;

  header(fp,data);


  fpout = fopen(datafile,"wb");
  if (fpout == NULL) {
    printf("ERROR: Cannot open output file %s\n",datafile);
    return 1;
  }
  data.write_header(fpout);

  groups(fp);
  type_arrays(fp,data);
  force_fields(fp,data);
  modify(fp);


  // print out stats

  data.stats();
  // read atoms from single or multiple restart files

  double *buf = NULL;
  int n,m;
  int maxbuf = 0;
  data.iatoms = data.ibonds = data.iangles =
    data.idihedrals = data.iimpropers = 0;

  for (int iproc = 0; iproc < data.nprocs; iproc++) {
	  printf("Converting atoms of processor %i of %i\n",iproc,data.nprocs);
    if (multiproc) {
      fclose(fp);
      char *procfile;
      sprintf(procfile,"%s%d%s",restartfile,iproc,ptr+1);
      fp = fopen(procfile,"rb");
      if (fp == NULL) {
        printf("ERROR: Cannot open restart file %s\n",procfile);
        return 1;
      }
    }
    n = read_int(fp);

    if (n > maxbuf) {
      maxbuf = n;
      delete [] buf;
      buf = new double[maxbuf];
    }

    if(fread(buf,sizeof(double),n,fp) != n) {
        printf("ERROR: Cannot read from input file. Unexpected end of file.\n");
        return 1;
    }

    m = 0;
    while (m < n) m += atom(&buf[m],data,fpout);
    fflush(fpout);
  }

  fclose(fp);


  printf("Almost done - closing output file...\n");

    fclose(fpout);

  return 0;
}

// ---------------------------------------------------------------------
// read header of restart file
// ---------------------------------------------------------------------

void header(FILE *fp, Data &data)
{
	const char *version = "10 Sept 2010";

	  data.triclinic = 0;

	  int flag;
	  flag = read_int(fp);

	  while (flag >= 0) {

	    if (flag == VERSION) {
	      data.version = read_char(fp);
	      if (strcmp(version,data.version) != 0) {
		const char *str = "Restart file version does not match restart2data version";
		printf("WARNING %s\n",str);
		printf("  restart2data version = %s\n",version);
	      }
	    }
	    else if (flag == UNITS) data.unit_style = read_char(fp);
	    else if (flag == NTIMESTEP) data.ntimestep = read_int(fp);
	    else if (flag == DIMENSION) data.dimension = read_int(fp);
	    else if (flag == NPROCS) data.nprocs = read_int(fp);
	    else if (flag == PROCGRID_0) data.px = read_int(fp);
	    else if (flag == PROCGRID_1) data.py = read_int(fp);
	    else if (flag == PROCGRID_2) data.pz = read_int(fp);
	    else if (flag == NEWTON_PAIR) data.newton_pair = read_int(fp);
	    else if (flag == NEWTON_BOND) data.newton_bond = read_int(fp);
	    else if (flag == XPERIODIC) data.xperiodic = read_int(fp);
	    else if (flag == YPERIODIC) data.yperiodic = read_int(fp);
	    else if (flag == ZPERIODIC) data.zperiodic = read_int(fp);
	    else if (flag == BOUNDARY_00) data.boundary[0][0] = read_int(fp);
	    else if (flag == BOUNDARY_01) data.boundary[0][1] = read_int(fp);
	    else if (flag == BOUNDARY_10) data.boundary[1][0] = read_int(fp);
	    else if (flag == BOUNDARY_11) data.boundary[1][1] = read_int(fp);
	    else if (flag == BOUNDARY_20) data.boundary[2][0] = read_int(fp);
	    else if (flag == BOUNDARY_21) data.boundary[2][1] = read_int(fp);

	    // if atom_style = hybrid:
	    //   set data_style_hybrid to # of sub-styles
	    //   read additional sub-class arguments
	    //   set sub-styles to 1 to N

	    else if (flag == ATOM_STYLE) {
	      data.style_angle = data.style_atomic = data.style_bond =
		data.style_charge = data.style_dipole =	data.style_dpd =
		data.style_ellipsoid = data.style_full = data.style_granular =
		data.style_hybrid = data.style_molecular = data.style_peri = 0;

	      data.atom_style = read_char(fp);
	      set_style(data.atom_style,data,1);

	      if (strcmp(data.atom_style,"hybrid") == 0) {
		int nwords = read_int(fp);
		set_style(data.atom_style,data,nwords);
		char *substyle;
		for (int i = 1; i <= nwords; i++) {
		  substyle = read_char(fp);
		  set_style(substyle,data,i);
		}
	      }
	    }

	    else if (flag == NATOMS) data.natoms = static_cast<int> (read_double(fp));
	    else if (flag == NTYPES) data.ntypes = read_int(fp);
	    else if (flag == NBONDS) data.nbonds = read_int(fp);
	    else if (flag == NBONDTYPES) data.nbondtypes = read_int(fp);
	    else if (flag == BOND_PER_ATOM) data.bond_per_atom = read_int(fp);
	    else if (flag == NANGLES) data.nangles = read_int(fp);
	    else if (flag == NANGLETYPES) data.nangletypes = read_int(fp);
	    else if (flag == ANGLE_PER_ATOM) data.angle_per_atom = read_int(fp);
	    else if (flag == NDIHEDRALS) data.ndihedrals = read_int(fp);
	    else if (flag == NDIHEDRALTYPES) data.ndihedraltypes = read_int(fp);
	    else if (flag == DIHEDRAL_PER_ATOM) data.dihedral_per_atom = read_int(fp);
	    else if (flag == NIMPROPERS) data.nimpropers = read_int(fp);
	    else if (flag == NIMPROPERTYPES) data.nimpropertypes = read_int(fp);
	    else if (flag == IMPROPER_PER_ATOM) data.improper_per_atom = read_int(fp);
	    else if (flag == BOXLO_0) data.xlo = read_double(fp);
	    else if (flag == BOXHI_0) data.xhi = read_double(fp);
	    else if (flag == BOXLO_1) data.ylo = read_double(fp);
	    else if (flag == BOXHI_1) data.yhi = read_double(fp);
	    else if (flag == BOXLO_2) data.zlo = read_double(fp);
	    else if (flag == BOXHI_2) data.zhi = read_double(fp);
	    else if (flag == SPECIAL_LJ_1) data.special_lj[1] = read_double(fp);
	    else if (flag == SPECIAL_LJ_2) data.special_lj[2] = read_double(fp);
	    else if (flag == SPECIAL_LJ_3) data.special_lj[3] = read_double(fp);
	    else if (flag == SPECIAL_COUL_1) data.special_coul[1] = read_double(fp);
	    else if (flag == SPECIAL_COUL_2) data.special_coul[2] = read_double(fp);
	    else if (flag == SPECIAL_COUL_3) data.special_coul[3] = read_double(fp);
	    else if (flag == XY) {
	      data.triclinic = 1;
	      data.xy = read_double(fp);
	    } else if (flag == XZ) {
	      data.triclinic = 1;
	      data.xz = read_double(fp);
	    } else if (flag == YZ) {
	      data.triclinic = 1;
	      data.yz = read_double(fp);
	    } else {
	      printf("ERROR: Invalid flag in header section of restart file %d\n",
		     flag);
	      exit(1);
	    }

	    flag = read_int(fp);
	  }
}

// ---------------------------------------------------------------------
// set atom style to flag
// ---------------------------------------------------------------------

void set_style(char *name, Data &data, int flag)
{
  if (strcmp(name,"atomic") == 0) data.style_atomic = flag;
  else {
    printf("ERROR: Unknown atom style %s\n",name);
    exit(1);
  }
}

// ---------------------------------------------------------------------
// read group info from restart file, just ignore it
// ---------------------------------------------------------------------

void groups(FILE *fp)
{
	  int ngroup = read_int(fp);

	  int n;
	  char *name;

	  // use count to not change restart format with deleted groups
	  // remove this on next major release

	  int count = 0;
	  for (int i = 0; i < MAX_GROUP; i++) {
	    name = read_char(fp);
	    delete [] name;
	    count++;
	    if (count == ngroup) break;
	  }
}

// ---------------------------------------------------------------------
// read type arrays from restart file
// ---------------------------------------------------------------------

void type_arrays(FILE *fp, Data &data)
{
	data.mass = NULL;
	  data.shape = NULL;
	  data.dipole = NULL;

	  int flag;
	  flag = read_int(fp);

	  while (flag >= 0) {

	    if (flag == MASS) {
	      data.mass = new double[data.ntypes+1];
	      fread(&data.mass[1],sizeof(double),data.ntypes,fp);
	    } else if (flag == SHAPE) {
	      data.shape = new double[3*(data.ntypes+1)];
	      fread(&data.shape[3],sizeof(double),3*data.ntypes,fp);
	    } else if (flag == DIPOLE) {
	      data.dipole = new double[data.ntypes+1];
	      fread(&data.dipole[1],sizeof(double),data.ntypes,fp);
	    } else {
	      printf("ERROR: Invalid flag in type arrays section of restart file %d\n",
		     flag);
	      exit(1);
	    }

	    flag = read_int(fp);
	  }
}

// ---------------------------------------------------------------------
// read force-field info from restart file
// ---------------------------------------------------------------------

void force_fields(FILE *fp, Data &data)
{
	data.pair_style = data.bond_style = data.angle_style =
	    data.dihedral_style = data.improper_style = NULL;

	  int flag;
	  flag = read_int(fp);

	  while (flag >= 0) {

	    if (flag == PAIR) {
	      data.pair_style = read_char(fp);
	      pair(fp,data,data.pair_style,1);
	    } else {
	      printf("ERROR: Invalid flag in force fields section of restart file %d\n",
		     flag);
	      exit(1);
	    }

	    flag = read_int(fp);
	  }
}

// ---------------------------------------------------------------------
// read fix info from restart file, just ignore it
// ---------------------------------------------------------------------

void modify(FILE *fp)
{
	char *buf;
	  int n;

	  // nfix = # of fix entries with state

	  int nfix = read_int(fp);

	  // read each entry with id string, style string, chunk of data

	  for (int i = 0; i < nfix; i++) {
	    buf = read_char(fp); delete [] buf;
	    buf = read_char(fp); delete [] buf;
	    buf = read_char(fp); delete [] buf;
	  }

	  // nfix = # of fix entries with peratom info

	  int nfix_peratom = read_int(fp);

	  // read each entry with id string, style string, maxsize of one atom data

	  for (int i = 0; i < nfix_peratom; i++) {
	    buf = read_char(fp); delete [] buf;
	    buf = read_char(fp); delete [] buf;
	    n = read_int(fp);
	  }
}

// ---------------------------------------------------------------------
// read atom info from restart file and store in data struct
// ---------------------------------------------------------------------

int atom(double *buf, Data &data, FILE* fout)
{
  // read atom quantities from buf
  float xyz[3];
  xyz[0] = (float)buf[1];
  xyz[1] = (float)buf[2];
  xyz[2] = (float)buf[3];
  fwrite(xyz, sizeof(xyz[0]), 3, fout);

  data.iatoms++;
  return static_cast<int> (buf[0]);
}

// ---------------------------------------------------------------------
// pair coeffs
// one section for each pair style
// flag = 1, read all coeff info and allocation arrays
// flag = 0, just read global settings (when called recursively by hybrid)
// ---------------------------------------------------------------------

void pair(FILE *fp, Data &data, char *style, int flag)
{
  int i,j,m;
  int itmp;
  double rtmp;

  if (strcmp(style,"none") == 0) {

  } else if (strcmp(style,"airebo") == 0) {

  } else if (strcmp(style,"born/coul/long") == 0) {
    double cut_lj_global = read_double(fp);
    double cut_coul = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_born_A = new double[data.ntypes+1];
    data.pair_born_rho = new double[data.ntypes+1];
    data.pair_born_sigma = new double[data.ntypes+1];
    data.pair_born_C = new double[data.ntypes+1];
    data.pair_born_D = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_born_A[i] = read_double(fp);
	    data.pair_born_rho[i] = read_double(fp);
	    data.pair_born_sigma[i] = read_double(fp);
	    data.pair_born_C[i] = read_double(fp);
	    data.pair_born_D[i] = read_double(fp);
	    double cut_lj = read_double(fp);
	  } else {
	    double born_A = read_double(fp);
	    double born_rho = read_double(fp);
	    double born_sigma = read_double(fp);
	    double born_C = read_double(fp);
	    double born_D = read_double(fp);
	    double cut_lj = read_double(fp);
	  }
	}
      }

  } else if ((strcmp(style,"buck") == 0)  ||
	   (strcmp(style,"buck/coul/cut") == 0) ||
	   (strcmp(style,"buck/coul/long") == 0) ||
	   (strcmp(style,"buck/coul") == 0)) {

    if (strcmp(style,"buck") == 0) {
      m = 0;
      double cut_lj_global = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"buck/coul/cut") == 0) {
      m = 1;
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"buck/coul/long") == 0) {
      m = 0;
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"buck/coul") == 0) {
      m = 0;
      double cut_buck_global = read_double(fp);
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
      int ewald_order = read_int(fp);
    }

    if (!flag) return;

    data.pair_buck_A = new double[data.ntypes+1];
    data.pair_buck_rho = new double[data.ntypes+1];
    data.pair_buck_C = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_buck_A[i] = read_double(fp);
	    data.pair_buck_rho[i] = read_double(fp);
	    data.pair_buck_C[i] = read_double(fp);
	    double cut_lj = read_double(fp);
	    if (m) double cut_coul = read_double(fp);
	  } else {
	    double buck_A = read_double(fp);
	    double buck_rho = read_double(fp);
	    double buck_C = read_double(fp);
	    double cut_lj = read_double(fp);
	    if (m) double cut_coul = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"colloid") == 0) {

    double cut_global = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_colloid_A12 = new double[data.ntypes+1];
    data.pair_colloid_sigma = new double[data.ntypes+1];
    data.pair_colloid_d1 = new double[data.ntypes+1];
    data.pair_colloid_d2 = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_colloid_A12[i] = read_double(fp);
	    data.pair_colloid_sigma[i] = read_double(fp);
	    data.pair_colloid_d1[i] = read_double(fp);
	    data.pair_colloid_d2[i] = read_double(fp);
	    double cut_lj = read_double(fp);
	  } else {
	    double colloid_A12 = read_double(fp);
	    double colloid_sigma = read_double(fp);
	    double colloid_d1 = read_double(fp);
	    double colloid_d2 = read_double(fp);
	    double cut_lj = read_double(fp);
	  }
	}
      }

  } else if ((strcmp(style,"coul/cut") == 0) ||
	     (strcmp(style,"coul/debye") == 0) ||
	     (strcmp(style,"coul/long") == 0)) {

    if (strcmp(style,"coul/cut") == 0) {
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"coul/debye") == 0) {
      m = 1;
      double cut_coul = read_double(fp);
      double kappa = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"coul/long") == 0) {
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    }

    if (!flag) return;

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    double cut_coul = read_double(fp);
	  } else {
	    double cut_coul = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"dipole/cut") == 0) {

    double cut_lj_global = read_double(fp);
    double cut_coul_global = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_dipole_epsilon = new double[data.ntypes+1];
    data.pair_dipole_sigma = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_dipole_epsilon[i] = read_double(fp);
	    data.pair_dipole_sigma[i] = read_double(fp);
	    double cut_lj = read_double(fp);
	    double cut_coul = read_double(fp);
	  } else {
	    double dipole_epsilon = read_double(fp);
	    double dipole_sigma = read_double(fp);
	    double cut_lj = read_double(fp);
	    double cut_coul = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"dpd") == 0) {

    double temperature = read_double(fp);
    double cut_global = read_double(fp);
    int seed = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_dpd_a0 = new double[data.ntypes+1];
    data.pair_dpd_gamma = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_dpd_a0[i] = read_double(fp);
	    data.pair_dpd_gamma[i] = read_double(fp);
	    double cut = read_double(fp);
	  } else {
	    double dpd_a0 = read_double(fp);
	    double dpd_gamma = read_double(fp);
	    double cut = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"eam") == 0) {
  } else if (strcmp(style,"eam/opt") == 0) {
  } else if (strcmp(style,"eam/alloy") == 0) {
  } else if (strcmp(style,"eam/alloy/opt") == 0) {
  } else if (strcmp(style,"eam/fs") == 0) {
  } else if (strcmp(style,"eam/fs/opt") == 0) {
  } else if (strcmp(style,"eim") == 0) {

  } else if (strcmp(style,"eff/cut") == 0) {

    double cut_coul = read_double(fp);
    int limit_size_flag = read_int(fp);
    int flexible_pressure_flag = read_int(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    double cut = read_double(fp);
	  } else {
	    double cut = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"gayberne") == 0) {

    double gamma = read_double(fp);
    double upsilon = read_double(fp);
    double mu = read_double(fp);
    double cut_global = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_gb_epsilon = new double[data.ntypes+1];
    data.pair_gb_sigma = new double[data.ntypes+1];
    data.pair_gb_epsa = new double[data.ntypes+1];
    data.pair_gb_epsb = new double[data.ntypes+1];
    data.pair_gb_epsc = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++) {
      itmp = read_int(fp);
      if (itmp) {
	data.pair_gb_epsa[i] = read_double(fp);
	data.pair_gb_epsb[i] = read_double(fp);
	data.pair_gb_epsc[i] = read_double(fp);
	data.pair_gb_epsa[i] = pow(data.pair_gb_epsa[i],-mu);
	data.pair_gb_epsb[i] = pow(data.pair_gb_epsb[i],-mu);
	data.pair_gb_epsc[i] = pow(data.pair_gb_epsc[i],-mu);
      }

      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_gb_epsilon[i] = read_double(fp);
	    data.pair_gb_sigma[i] = read_double(fp);
	    double cut = read_double(fp);
	  } else {
	    double gb_epsilon = read_double(fp);
	    double gb_sigma = read_double(fp);
	    double cut = read_double(fp);
	  }
	}
      }
    }

  } else if ((strcmp(style,"gran/hooke") == 0) ||
	   (strcmp(style,"gran/hooke/history") == 0) ||
	   (strcmp(style,"gran/hertz/history") == 0)) {

    double kn = read_double(fp);
    double kt = read_double(fp);
    double gamman = read_double(fp);
    double gammat = read_double(fp);
    double xmu = read_double(fp);
    int dampflag = read_int(fp);

  } else if ((strcmp(style,"lj/charmm/coul/charmm") == 0) ||
	   (strcmp(style,"lj/charmm/coul/charmm/implicit") == 0) ||
	   (strcmp(style,"lj/charmm/coul/long") == 0) ||
	   (strcmp(style,"lj/charmm/coul/long/opt") == 0)) {

    if (strcmp(style,"lj/charmm/coul/charmm") == 0) {
      double cut_lj_inner = read_double(fp);
      double cut_lj = read_double(fp);
      double cut_coul_inner = read_double(fp);
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/charmm/coul/charmm/implicit") == 0) {
      double cut_lj_inner = read_double(fp);
      double cut_lj = read_double(fp);
      double cut_coul_inner = read_double(fp);
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if ((strcmp(style,"lj/charmm/coul/long") == 0) ||
	       (strcmp(style,"lj/charmm/coul/long/opt") == 0)) {
      double cut_lj_inner = read_double(fp);
      double cut_lj = read_double(fp);
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    }

    if (!flag) return;

    data.pair_charmm_epsilon = new double[data.ntypes+1];
    data.pair_charmm_sigma = new double[data.ntypes+1];
    data.pair_charmm_eps14 = new double[data.ntypes+1];
    data.pair_charmm_sigma14 = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_charmm_epsilon[i] = read_double(fp);
	    data.pair_charmm_sigma[i] = read_double(fp);
	    data.pair_charmm_eps14[i] = read_double(fp);
	    data.pair_charmm_sigma14[i] = read_double(fp);
	  } else {
	    double charmm_epsilon = read_double(fp);
	    double charmm_sigma = read_double(fp);
	    double charmm_eps14 = read_double(fp);
	    double charmm_sigma14 = read_double(fp);
	  }
	}
      }

  } else if ((strcmp(style,"lj/class2") == 0) ||
	   (strcmp(style,"lj/class2/coul/cut") == 0) ||
	   (strcmp(style,"lj/class2/coul/long") == 0)) {

    if (strcmp(style,"lj/class2") == 0) {
      m = 0;
      double cut_lj_global = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/class2/coul/cut") == 0) {
      m = 1;
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/class2/coul/long") == 0) {
      m = 0;
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    }

    if (!flag) return;

    data.pair_class2_epsilon = new double[data.ntypes+1];
    data.pair_class2_sigma = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_class2_epsilon[i] = read_double(fp);
	    data.pair_class2_sigma[i] = read_double(fp);
	    double cut_lj = read_double(fp);
	    if (m) double cut_coul = read_double(fp);
	  } else {
	    double class2_epsilon = read_double(fp);
	    double class2_sigma = read_double(fp);
	    double cut_lj = read_double(fp);
	    if (m) double cut_coul = read_double(fp);
	  }
	}
      }

  } else if ((strcmp(style,"lj/cut") == 0) ||
	   (strcmp(style,"lj/cut/opt") == 0) ||
	   (strcmp(style,"lj/cut/coul/cut") == 0) ||
	   (strcmp(style,"lj/cut/coul/debye") == 0) ||
	   (strcmp(style,"lj/cut/coul/long") == 0) ||
	   (strcmp(style,"lj/cut/coul/long/tip4p") == 0) ||
	   (strcmp(style,"lj/coul") == 0)) {

    if ((strcmp(style,"lj/cut") == 0) || (strcmp(style,"lj/cut/opt") == 0)) {
      m = 0;
      double cut_lj_global = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/cut/coul/cut") == 0) {
      m = 1;
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/cut/coul/debye") == 0) {
      m = 1;
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      double kappa = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/cut/coul/long") == 0) {
      m = 0;
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/cut/coul/long/tip4p") == 0) {
      m = 0;
      int typeO = read_int(fp);
      int typeH = read_int(fp);
      int typeB = read_int(fp);
      int typeA = read_int(fp);
      double qdist = read_double(fp);
      double cut_lj_global = read_double(fp);
      double cut_lj_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/coul") == 0) {
      m = 0;
      double cut_lj_global = read_double(fp);
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
      int ewald_order = read_int(fp);
    }

    if (!flag) return;

    data.pair_lj_epsilon = new double[data.ntypes+1];
    data.pair_lj_sigma = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_lj_epsilon[i] = read_double(fp);
	    data.pair_lj_sigma[i] = read_double(fp);
	    double cut_lj = read_double(fp);
	    if (m) double cut_coul = read_double(fp);
	  } else {
	    double lj_epsilon = read_double(fp);
	    double lj_sigma = read_double(fp);
	    double cut_lj = read_double(fp);
	    if (m) double cut_coul = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"lj/expand") == 0) {

    double cut_global = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_ljexpand_epsilon = new double[data.ntypes+1];
    data.pair_ljexpand_sigma = new double[data.ntypes+1];
    data.pair_ljexpand_shift = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_ljexpand_epsilon[i] = read_double(fp);
	    data.pair_ljexpand_sigma[i] = read_double(fp);
	    data.pair_ljexpand_shift[i] = read_double(fp);
	    double cut_lj = read_double(fp);
	  } else {
	    double ljexpand_epsilon = read_double(fp);
	    double ljexpand_sigma = read_double(fp);
	    double ljexpand_shift = read_double(fp);
	    double cut_lj = read_double(fp);
	  }
	}
      }

  } else if ((strcmp(style,"lj/gromacs") == 0) ||
	     (strcmp(style,"lj/gromacs/coul/gromacs") == 0)) {

    if (strcmp(style,"lj/gromacs") == 0) {
      m = 1;
      double cut_inner_global = read_double(fp);
      double cut_global = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    } else if (strcmp(style,"lj/gromacs/coul/gromacs") == 0) {
      m = 0;
      double cut_lj_inner_global = read_double(fp);
      double cut_lj = read_double(fp);
      double cut_coul_inner_global = read_double(fp);
      double cut_coul = read_double(fp);
      int offset_flag = read_int(fp);
      int mix_flag = read_int(fp);
    }

    if (!flag) return;

    data.pair_ljgromacs_epsilon = new double[data.ntypes+1];
    data.pair_ljgromacs_sigma = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_ljgromacs_epsilon[i] = read_double(fp);
	    data.pair_ljgromacs_sigma[i] = read_double(fp);
	    if (m) {
	      double cut_inner = read_double(fp);
	      double cut = read_double(fp);
	    }
	    } else {
	    double ljgromacs_epsilon = read_double(fp);
	    double ljgromacs_sigma = read_double(fp);
	    if (m) {
	      double cut_inner = read_double(fp);
	      double cut = read_double(fp);
	    }
	  }
	}
      }

  } else if (strcmp(style,"lj/smooth") == 0) {

    double cut_inner_global = read_double(fp);
    double cut_global = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_ljsmooth_epsilon = new double[data.ntypes+1];
    data.pair_ljsmooth_sigma = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_ljsmooth_epsilon[i] = read_double(fp);
	    data.pair_ljsmooth_sigma[i] = read_double(fp);
	    double cut_inner = read_double(fp);
	    double cut = read_double(fp);
	  } else {
	    double ljsmooth_epsilon = read_double(fp);
	    double ljsmooth_sigma = read_double(fp);
	    double cut_inner = read_double(fp);
	    double cut = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"meam") == 0) {

  } else if ((strcmp(style,"morse") == 0) ||
	     (strcmp(style,"morse/opt") == 0)) {

    double cut_global = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_morse_d0 = new double[data.ntypes+1];
    data.pair_morse_alpha = new double[data.ntypes+1];
    data.pair_morse_r0 = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_morse_d0[i] = read_double(fp);
	    data.pair_morse_alpha[i] = read_double(fp);
	    data.pair_morse_r0[i] = read_double(fp);
	    double cut = read_double(fp);
	  } else {
	    double morse_d0 = read_double(fp);
	    double morse_alpha = read_double(fp);
	    double morse_r0 = read_double(fp);
	    double cut = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"reax") == 0) {

  } else if (strcmp(style,"soft") == 0) {

    double cut_global = read_double(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_soft_A = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_soft_A[i] = read_double(fp);
	    double cut = read_double(fp);
	  } else {
	    double soft_A = read_double(fp);
	    double cut = read_double(fp);
	  }
	}
      }

  } else if (strcmp(style,"sw") == 0) {

  } else if (strcmp(style,"table") == 0) {

    int tabstyle = read_int(fp);
    int n = read_int(fp);

  } else if (strcmp(style,"tersoff") == 0) {
  } else if (strcmp(style,"tersoff/zbl") == 0) {

  } else if (strcmp(style,"yukawa") == 0) {

    double kappa = read_double(fp);
    double cut_global = read_double(fp);
    int offset_flag = read_int(fp);
    int mix_flag = read_int(fp);

    if (!flag) return;

    data.pair_yukawa_A = new double[data.ntypes+1];

    for (i = 1; i <= data.ntypes; i++)
      for (j = i; j <= data.ntypes; j++) {
	itmp = read_int(fp);
	if (i == j && itmp == 0) {
	  printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
	  exit(1);
	}
	if (itmp) {
	  if (i == j) {
	    data.pair_yukawa_A[i] = read_double(fp);
	    double cut = read_double(fp);
	  } else {
	    double yukawa_A = read_double(fp);
	    double cut = read_double(fp);
	  }
	}
      }

  } else if ((strcmp(style,"cg/cmm") == 0) ||
             (strcmp(style,"cg/cmm/coul/cut") == 0) ||
             (strcmp(style,"cg/cmm/coul/long") == 0) ) {
    m = 0;
    data.cut_lj_global = read_double(fp);
    data.cut_coul_global = read_double(fp);
    data.kappa = read_double(fp);
    data.offset_flag = read_int(fp);
    data.mix_flag = read_int(fp);

    if (!flag) return;

    const int numtyp=data.ntypes+1;
    data.pair_cg_cmm_type = new int*[numtyp];
    data.pair_setflag = new int*[numtyp];
    data.pair_cg_epsilon = new double*[numtyp];
    data.pair_cg_sigma = new double*[numtyp];
    data.pair_cut_lj = new double*[numtyp];
    if  ((strcmp(style,"cg/cmm/coul/cut") == 0) ||
             (strcmp(style,"cg/cmm/coul/long") == 0) ) {
      data.pair_cut_coul = new double*[numtyp];
      m=1;
    } else {
      data.pair_cut_coul = NULL;
      m=0;
    }

    for (i = 1; i <= data.ntypes; i++) {
      data.pair_cg_cmm_type[i] = new int[numtyp];
      data.pair_setflag[i] = new int[numtyp];
      data.pair_cg_epsilon[i] = new double[numtyp];
      data.pair_cg_sigma[i] = new double[numtyp];
      data.pair_cut_lj[i] = new double[numtyp];
      if ((strcmp(style,"cg/cmm/coul/cut") == 0) ||
          (strcmp(style,"cg/cmm/coul/long") == 0) ) {
        data.pair_cut_coul[i] = new double[numtyp];
      }

      for (j = i; j <= data.ntypes; j++) {
        itmp = read_int(fp);
        data.pair_setflag[i][j] = itmp;
        if (i == j && itmp == 0) {
          printf("ERROR: Pair coeff %d,%d is not in restart file\n",i,j);
          exit(1);
        }
        if (itmp) {
          data.pair_cg_cmm_type[i][j] = read_int(fp);
          data.pair_cg_epsilon[i][j] = read_double(fp);
          data.pair_cg_sigma[i][j] = read_double(fp);
          data.pair_cut_lj[i][j] = read_double(fp);
          if (m) {
	    data.pair_cut_lj[i][j] = read_double(fp);
	    data.pair_cut_coul[i][j] = read_double(fp);
	  }
        }
      }
    }

  } else if ((strcmp(style,"hybrid") == 0) ||
	     (strcmp(style,"hybrid/overlay") == 0)) {

    // for each substyle of hybrid,
    //   read its settings by calling pair() recursively with flag = 0
    //   so that coeff array allocation is skipped

    int nstyles = read_int(fp);
    for (int i = 0; i < nstyles; i++) {
      char *substyle = read_char(fp);
      pair(fp,data,substyle,0);
    }

  } else {
    printf("ERROR: Unknown pair style %s\n",style);
    exit(1);
  }
}

// ---------------------------------------------------------------------
// initialize Data
// ---------------------------------------------------------------------

Data::Data()
{
}

// ---------------------------------------------------------------------
// print out stats on problem
// ---------------------------------------------------------------------

void Data::stats()
{
	  printf("  Restart file version = %s\n",version);
	  printf("  Ntimestep = %d\n",ntimestep);
	  printf("  Nprocs = %d\n",nprocs);
	  printf("  Natoms = %d\n",natoms);
	  printf("  Nbonds = %d\n",nbonds);
	  printf("  Nangles = %d\n",nangles);
	  printf("  Ndihedrals = %d\n",ndihedrals);
	  printf("  Nimpropers = %d\n",nimpropers);
	  printf("  Unit style = %s\n",unit_style);
	  printf("  Atom style = %s\n",atom_style);
	  printf("  Pair style = %s\n",pair_style);
	  printf("  Bond style = %s\n",bond_style);
	  printf("  Angle style = %s\n",angle_style);
	  printf("  Dihedral style = %s\n",dihedral_style);
	  printf("  Improper style = %s\n",improper_style);
	  printf("  Xlo xhi = %g %g\n",xlo,xhi);
	  printf("  Ylo yhi = %g %g\n",ylo,yhi);
	  printf("  Zlo zhi = %g %g\n",zlo,zhi);
	  if (triclinic) printf("  Xy xz yz = %g %g %g\n",xy,xz,yz);
	  printf("  Periodicity = %d %d %d\n",xperiodic,yperiodic,zperiodic);
	  printf("  Boundary = %d %d, %d %d, %d %d\n",boundary[0][0],boundary[0][1],
		 boundary[1][0],boundary[1][1],boundary[2][0],boundary[2][1]);
}

// ---------------------------------------------------------------------
// write the data file and input file
// ---------------------------------------------------------------------

void Data::write_header(FILE *fp, FILE *fp2)
{
  unsigned int magicCode = 0x87BD21;
  fwrite(&magicCode, sizeof(magicCode), 1, fp);
  fwrite(&ntimestep, sizeof(ntimestep), 1, fp);
  fwrite(&xlo, sizeof(xlo), 1, fp);
  fwrite(&xhi, sizeof(xhi), 1, fp);
  fwrite(&ylo, sizeof(ylo), 1, fp);
  fwrite(&yhi, sizeof(yhi), 1, fp);
  fwrite(&zlo, sizeof(zlo), 1, fp);
  fwrite(&zhi, sizeof(zhi), 1, fp);
  fwrite(&xy, sizeof(xy), 1, fp);
  fwrite(&xz, sizeof(xz), 1, fp);
  fwrite(&yz, sizeof(yz), 1, fp);
  fwrite(&xperiodic, sizeof(xperiodic), 1, fp);
  fwrite(&yperiodic, sizeof(yperiodic), 1, fp);
  fwrite(&zperiodic, sizeof(zperiodic), 1, fp);
  fwrite(&natoms, sizeof(natoms), 1, fp);
}

// ---------------------------------------------------------------------
// strip known accelerator suffixes from style name
// ---------------------------------------------------------------------

void strip_suffix(char *style)
{
  char *slash = strrchr(style,'/');
  if (slash == NULL) return;

  int i=0;

  const char *suffix_list[] = {	"/opt", "/gpu", "/cuda", "/omp", NULL };
  const char *suffix = suffix_list[0];
  while (suffix != NULL) {
    if (strcmp(slash,suffix) == 0) {
      *slash = '\0';
      return;
    }
    ++i;
    suffix = suffix_list[i];
  }
}

// ---------------------------------------------------------------------
// binary reads from restart file
// ---------------------------------------------------------------------

int read_int(FILE *fp)
{
  int value;
  fread(&value,sizeof(int),1,fp);
  return value;
}

double read_double(FILE *fp)
{
  double value;
  fread(&value,sizeof(double),1,fp);
  return value;
}

char *read_char(FILE *fp)
{
  int n;
  fread(&n,sizeof(int),1,fp);
  if (n == 0) return NULL;
  char *value = new char[n];
  fread(value,sizeof(char),n,fp);
  return value;
}

