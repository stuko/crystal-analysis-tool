#!/usr/bin/env python

# Creates a VRML file for rendering defect surfaces.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input dislocations file and an output VRML file.")

def skipToKeyword(stream, keyword):
    while True:
        line = stream.readline()
        if line == "": raise IOError("Unexpected end of surface file.")
        if line.startswith(keyword):
            return line

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")

if instream.readline() != "# vtk DataFile Version 3.0\n": raise IOError("Invalid VTK file.")
infoLine = instream.readline()
if infoLine.startswith("# Defect surface") == False and infoLine.startswith("# Stacking faults") == False: raise IOError("Invalid surface file.")    
dxaVersionString = infoLine.strip()
if instream.readline() != "ASCII\n" or instream.readline() != "DATASET UNSTRUCTURED_GRID\n": raise IOError("Invalid surface file.")

# Open the output file.
outstream = stdout
if args[1] != '-': outstream = open(args[1], "w")

outstream.write("#VRML V2.0 utf8\n")
outstream.write("Group {\n")
outstream.write("  children [\n")
outstream.write("  Shape {\n")
outstream.write("appearance Appearance {\n")
outstream.write("material Material {\n")
outstream.write("ambientIntensity .075\n")
outstream.write("diffuseColor 1 1 1\n")
outstream.write("specularColor 1 1 1\n")
outstream.write("shininess .02\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("IndexedFaceSet {\n")

# Read in point coordinates
tokens = instream.readline().split()
if tokens[0] != "POINTS": raise IOError("Invalid surface file.")
numPoints = int(tokens[1])

outstream.write("coord Coordinate {\n")
outstream.write("point [\n")

for i in range(numPoints):
    tokens = instream.readline().split()
    outstream.write("%s %s %s,\n" % (tokens[0], tokens[1], tokens[2]))
outstream.write("]\n")
outstream.write("}\n")

outstream.write("coordIndex [\n")

numFacets = int(skipToKeyword(instream, "CELLS").split()[1])

# Read in facets.
for i in range(numFacets):
    tokens = instream.readline().split()
    numFacetPoints = int(tokens[0])
    assert(numFacetPoints == 3)
    outstream.write("%s %s %s -1,\n" % (tokens[1], tokens[2], tokens[3]))   

outstream.write("]\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("]\n")
outstream.write("}\n")
outstream.close()
