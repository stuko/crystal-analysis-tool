///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CoordinationPatternAnalysis.h"
#include "CoordinationPatternMatcher.h"
#include "../catalog/PatternCatalog.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Searches the atomic structure for the given coordination patterns.
******************************************************************************/
void CoordinationPatternAnalysis::searchCoordinationPatterns(const vector<CoordinationPattern const*>& patternsToSearchFor, CAFloat cutoffRadius)
{
	// Reset output array.
	_coordinationMatchCounts.resize(structure().numLocalAtoms() + structure().numGhostAtoms());
	_coordinationMatchesHeads.resize(_coordinationMatchCounts.size());
	_coordinationPatternMatches.clear();

	if(patternsToSearchFor.empty()) {
		boost::fill(_coordinationMatchCounts, 0);
		return;
	}

	// Create pattern matcher objects.
	boost::ptr_vector<CoordinationPatternMatcher> matchers;
	BOOST_REVERSE_FOREACH(CoordinationPattern const* pattern, patternsToSearchFor) {
		matchers.push_back(CoordinationPatternMatcher::createPatternMatcher(structure(), neighborList(), *pattern));
		matchers.back().setCutoff(cutoffRadius);
	}

	// Match local atoms to coordination patterns.
	vector<int>::iterator matchCount = _coordinationMatchCounts.begin();
	vector<int>::iterator matchHead = _coordinationMatchesHeads.begin();
	for(int atomIndex = 0; atomIndex < structure().numLocalAtoms(); atomIndex++, ++matchCount, ++matchHead) {
		*matchCount = 0;
		*matchHead = _coordinationPatternMatches.size();
		BOOST_FOREACH(CoordinationPatternMatcher& matcher, matchers) {
			if(matcher.findNextPermutation(atomIndex)) {
				// Create a record for this match.
				coordmatch record;
				record.pattern = &matcher.pattern();
				boost::copy(matcher.permutation(), record.mapping);
				_coordinationPatternMatches.push_back(record);
				(*matchCount)++;
			}
		}
	}

	// Transfer match records of local atoms to neighboring processors.
	if(structure().neighborMode() == AtomicStructure::GHOST_ATOMS)
		exchangeCoordinationPatternMatches();

	BOOST_FOREACH(const CoordinationPatternMatcher& matcher, matchers) {
		AtomInteger totalNumberOfMatches = parallel().reduce_sum(matcher.numberOfMatches());
		if(parallel().isMaster())
			context().msgLogger() <<  "Coordination pattern '[" << matcher.pattern().index << "]" << matcher.pattern().name << "': " << totalNumberOfMatches << " matching atoms (" << (totalNumberOfMatches*100/structure().numTotalAtoms()) << "%)" << endl;
	}
}

/**
 * This is the serialized version of the CoordinationPatternAnalysis::coordmatch structure.
 * It is used to transfer a match record between processors.
 */
struct CoordMatchComm
{
	int patternIndex;
	int mapping[CA_MAX_PATTERN_NEIGHBORS];
};

/******************************************************************************
* Exchange coordination pattern matches with neighboring processors.
******************************************************************************/
void CoordinationPatternAnalysis::exchangeCoordinationPatternMatches()
{
	CALIB_ASSERT(structure().neighborMode() == AtomicStructure::GHOST_ATOMS);

	vector<int>::iterator ghostMatchCount = _coordinationMatchCounts.begin() + structure().numLocalAtoms();
	vector<int>::iterator ghostMappingHead = _coordinationMatchesHeads.begin() + structure().numLocalAtoms();

	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false && parallel().processorGrid(dim) == 1) continue;

		for(int dir = 0; dir <= 1; dir++) {
			const vector<int>& sendIndices = structure().ghostCommunicationList(dim, dir);

			// First count and send the number of match records to be transferred for each atom.
			size_t sendCount = 0;
			vector<int> countSendBuffer(sendIndices.size());
			vector<int>::iterator countSendItem = countSendBuffer.begin();
			BOOST_FOREACH(int index, sendIndices) {
				*countSendItem++ = coordinationPatternMatchCount(index);
				sendCount += coordinationPatternMatchCount(index);
			}

			// Allocate receive buffer and send packed record counts to neighbor processor.
			vector<int> countReceiveBuffer(structure().ghostReceiveCount(dim, dir));
			parallel().exchange(dim, dir, structure().pbc(dim), countSendBuffer, countReceiveBuffer);

			// Pack match records of local atoms into a send buffer.
			vector<CoordMatchComm> sendBuffer(sendCount);
			vector<CoordMatchComm>::iterator sendItem = sendBuffer.begin();
			BOOST_FOREACH(int index, sendIndices) {
				BOOST_FOREACH(const coordmatch& matchRecord, coordinationPatternMatches(index)) {
					sendItem->patternIndex = matchRecord.pattern->index;
					memcpy(sendItem->mapping, matchRecord.mapping, sizeof(sendItem->mapping));
					++sendItem;
				}
			}
			CALIB_ASSERT(sendItem == sendBuffer.end());

			// Allocate receive buffer and send packed match records to neighbor processor.
			int receiveCount = boost::accumulate(countReceiveBuffer, 0);
			vector<CoordMatchComm> receiveBuffer(receiveCount);
			parallel().exchange(dim, dir, structure().pbc(dim), sendBuffer, receiveBuffer);

			// Unpack received match records of ghost atoms.
			_coordinationPatternMatches.reserve(_coordinationPatternMatches.size() + receiveCount);
			vector<CoordMatchComm>::const_iterator receivedMatch = receiveBuffer.begin();
			for(vector<int>::const_iterator receivedMatchCount = countReceiveBuffer.begin(); receivedMatchCount != countReceiveBuffer.end(); ++receivedMatchCount, ++ghostMappingHead, ++ghostMatchCount) {
				*ghostMatchCount = *receivedMatchCount;
				*ghostMappingHead = _coordinationPatternMatches.size();
				for(int i = 0; i < *receivedMatchCount; i++, ++receivedMatch) {
					coordmatch record;
					record.pattern = &_catalog.coordinationPatternByIndex(receivedMatch->patternIndex);
					memcpy(record.mapping, receivedMatch->mapping, sizeof(record.mapping));
					_coordinationPatternMatches.push_back(record);
				}
			}
			CALIB_ASSERT(receivedMatch == receiveBuffer.end());
		}
	}
	CALIB_ASSERT(ghostMatchCount == _coordinationMatchCounts.end());
	CALIB_ASSERT(ghostMappingHead == _coordinationMatchesHeads.end());
}


/******************************************************************************
* Determines which coordination pattern best matches an atom.
******************************************************************************/
CoordinationPatternAnalysis::coordmatch const* CoordinationPatternAnalysis::bestMatchingCoordinationPattern(int atomIndex) const
{
	CAFloat minDeviation = CAFLOAT_MAX;
	CoordinationPatternAnalysis::coordmatch const* bestMatch = NULL;
	BOOST_FOREACH(const CoordinationPatternAnalysis::coordmatch& match, coordinationPatternMatches(atomIndex)) {
		CAFloat dev = calculateDeviationFromCoordinationPattern(match, atomIndex);
		if(dev < minDeviation) {
			minDeviation = dev;
			bestMatch = &match;
		}
	}
	return bestMatch;
}

/******************************************************************************
* Quantifies the deviation of an atom's coordination structure from the given
* ideal pattern.
******************************************************************************/
CAFloat CoordinationPatternAnalysis::calculateDeviationFromCoordinationPattern(const CoordinationPatternAnalysis::coordmatch& mapping, int centerAtomIndex) const
{
	const CoordinationPattern& pattern = *mapping.pattern;
	CAFloat minDeviation = CAFLOAT_MAX;

	// Try every symmetry permutation of the pattern, because we might not have found the optimal
	// one in the first place.
	for(int p = 0; p < pattern.permutations.size(); p++) {

		// Calculate transformation matrix using a least-square fit.
		FrameTransformation ftm;
		for(int patternNeighborIndex = 0; patternNeighborIndex < pattern.numNeighbors(); patternNeighborIndex++) {
			const Vector3& nv = neighborList().neighborVector(centerAtomIndex, mapping.mapping[patternNeighborIndex]);
			const Vector3& lv = pattern.neighbors[pattern.permutations[p][patternNeighborIndex]].vec;
			ftm.addVector(nv, lv);
		}
		// Calculate transformation matrix.
		Matrix3 orientation = ftm.computeAverageTransformation();

		// Compute deviation magnitudes for every neighbor.
		// Normalize by nearest-neighbor distance.
		CAFloat maxDeviation = 0;
		for(int patternNeighborIndex = 0; patternNeighborIndex < pattern.numNeighbors(); patternNeighborIndex++) {
			const Vector3& nv = neighborList().neighborVector(centerAtomIndex, mapping.mapping[patternNeighborIndex]);
			const Vector3& lv = pattern.neighbors[pattern.permutations[p][patternNeighborIndex]].vec;
			CAFloat deviation = (lv - orientation * nv).squaredNorm() / pattern.neighbors[0].vec.squaredNorm();
			if(deviation > maxDeviation)
				maxDeviation = deviation;
		}

		if(maxDeviation < minDeviation)
			minDeviation = maxDeviation;
	}

	return sqrt(minDeviation);
}

}; // End of namespace
