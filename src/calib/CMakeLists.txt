####################################################################
#
# This a CMake project file that controls which source files must
# be built and which third-party libraries must be linked.
#
# Use the cmake utility to create a Makefile from this project file.
# See http://www.cmake.org/ for details.
#
####################################################################

####################################################################
# List of source files of the CrystalAnalysis library.
####################################################################
SET(CALibSourceFiles
	util/Debug.cpp
	context/CAContext.cpp
	atomic_structure/SimulationCell.cpp
	atomic_structure/AtomicStructure.cpp
	atomic_structure/neighbor_list/NeighborList.cpp
	pattern/catalog/PatternCatalog.cpp
	pattern/coordinationpattern/CoordinationPatternAnalysis.cpp
	pattern/coordinationpattern/CoordinationPatternMatcher.cpp
	pattern/coordinationpattern/cna/CNACoordinationPatternMatcher.cpp
	pattern/coordinationpattern/acna/AdaptiveCNACoordinationPatternMatcher.cpp
	pattern/coordinationpattern/cna/CommonNeighborAnalysis.cpp
	pattern/coordinationpattern/nda/NDACoordinationPatternMatcher.cpp
	pattern/coordinationpattern/diamond/DiamondCoordinationPatternMatcher.cpp
	pattern/superpattern/SuperPatternMatcher.cpp
	pattern/superpattern/SuperPatternAnalysis.cpp
	cluster_graph/ClusterGraph.cpp
	cluster_graph/ClusterGraphParallel.cpp
	tessellation/DelaunayTessellation.cpp
	tessellation/ElasticMapping.cpp
	tessellation/CrystalPathFinder.cpp
	dislocations/InterfaceMesh.cpp
	dislocations/DislocationNetwork.cpp
	dislocations/DislocationTracer.cpp
	trianglemesh/TriangleMesh.cpp
	trianglemesh/TriangleMeshFromInterfaceMesh.cpp
	trianglemesh/TriangleMeshCap.cpp
	deformation/DisplacementVectors.cpp
	deformation/Field.cpp
	deformation/DeformationField.cpp
	deformation/ElasticField.cpp
	input/atoms/AtomicStructureParser.cpp
	input/atoms/LAMMPSParser.cpp
	input/atoms/IMDParser.cpp
	input/atoms/POSCARParser.cpp
	input/atoms/DDCMDParser.cpp
	input/atoms/CompactFileParser.cpp
	input/patterns/PatternCatalogReader.cpp
	output/atoms/AtomicStructureWriter.cpp
	output/interface_mesh/InterfaceMeshWriter.cpp
	output/dislocations/DislocationsWriter.cpp
	output/dislocations/AnalysisResultsWriter.cpp
	output/triangle_mesh/TriangleMeshWriter.cpp
	output/cell/SimulationCellWriter.cpp
	output/cluster_graph/ClusterGraphWriter.cpp
	output/patterns/PatternCatalogWriter.cpp
	output/field/FieldWriter.cpp
	output/field/DeformationFieldWriter.cpp
	output/field/ElasticFieldWriter.cpp
	hardcoded/HardcodedCoordinationPatternAnalysis.cpp
	hardcoded/HardcodedSuperPatternAnalysis.cpp
	grains/GrainIdentification.cpp
	surface/FreeSurfaceIdentification.cpp
)

####################################################################
# List of source files of the GLU library used for polygon tessellation.
####################################################################
SET(GLUSourceFiles
	trianglemesh/polytess/dict.c
	trianglemesh/polytess/geom.c
	trianglemesh/polytess/memalloc.c
	trianglemesh/polytess/mesh.c
	trianglemesh/polytess/normal.c
	trianglemesh/polytess/priorityq.c
	trianglemesh/polytess/render.c
	trianglemesh/polytess/sweep.c
	trianglemesh/polytess/tess.c
	trianglemesh/polytess/tessmono.c
)

###################################################################
# Build CA library.
###################################################################
ADD_LIBRARY(CrystalAnalysisLibrary STATIC ${CALibSourceFiles} ${GLUSourceFiles})
TARGET_LINK_LIBRARIES(CrystalAnalysisLibrary ${Boost_LIBRARIES})
IF(ENABLE_MPI)
	TARGET_LINK_LIBRARIES(CrystalAnalysisLibrary ${MPI_LIBRARIES})
ENDIF()
SET_TARGET_PROPERTIES(CrystalAnalysisLibrary PROPERTIES COMPILE_DEFINITIONS "${ANALYSIS_COMPILE_DEFINITIONS}")
SET_TARGET_PROPERTIES(CrystalAnalysisLibrary PROPERTIES ARCHIVE_OUTPUT_NAME "crystalanalysis")
SET_TARGET_PROPERTIES(CrystalAnalysisLibrary PROPERTIES OUTPUT_NAME "crystalanalysis")

IF(ENABLE_ZLIB_SUPPORT)
    IF(APPLE OR CYGWIN OR WIN32 OR Boost_NO_SYSTEM_PATHS)
    	# On some systems, we must explicitly link in the Boost zlib library.
    	FIND_LIBRARY(BOOST_ZLIB_LIBRARY NAMES "boost_zlib" PATHS ${Boost_LIBRARY_DIRS})
    	IF(NOT BOOST_ZLIB_LIBRARY)
    		MESSAGE(FATAL_ERROR "ZLib library (boost_zlib) not found in Boost library directory ${Boost_LIBRARY_DIRS}.")
    	ENDIF()
    	TARGET_LINK_LIBRARIES(CrystalAnalysisLibrary ${BOOST_ZLIB_LIBRARY})
    ENDIF()
ENDIF()

IF(UNIX AND NOT CYGWIN AND Boost_NO_SYSTEM_PATHS)
	# On some systems, we must explicitly link in the Posix realtime library.
	FIND_LIBRARY(BOOST_POSIXRT_LIBRARY NAMES rt DOC "The POSIX Realtime Extensions library used by Boost.Thread")
	IF(BOOST_POSIXRT_LIBRARY)
		TARGET_LINK_LIBRARIES(CrystalAnalysisLibrary ${BOOST_POSIXRT_LIBRARY})
	ENDIF()
ENDIF()
