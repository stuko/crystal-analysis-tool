///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DeformationField.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

// List of vertices that bound the six edges of a tetrahedron.
static const int edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

/******************************************************************************
* Constructor,
******************************************************************************/
DeformationField::DeformationField(CAContext& context, AtomicStructure& structure1, AtomicStructure& structure2, DelaunayTessellation& tessellation, const PatternCatalog& catalog) :
	Field(context, structure1, tessellation), _structure2(structure2),
	_displacements(context, structure1, structure2), _bridgedClusterGraph(structure1, catalog)
{
	CALIB_ASSERT(&structure1 == &tessellation.structure());
}

/******************************************************************************
* Calculates the total deformation tensor field.
******************************************************************************/
void DeformationField::calculateTotalDeformation()
{
	CALIB_ASSERT(localCells().size() != 0);

	// Build two-way map between atoms in the initial configuration and the final configuration.
	_displacements.prepare();

	// Allocate output array.
	_deformationGradients.resize(localCells().size());

	vector<Matrix3>::iterator F = _deformationGradients.begin();
	BOOST_FOREACH(DelaunayTessellation::CellHandle cell, localCells()) {
		// Determine corner atoms of the tetrahedron in the initial and final configuration.
		int initialAtoms[4];
		int finalAtoms[4];
		for(int v = 0; v < 4; v++) {
			int vertexIndex = cell->vertex(v)->info();
			CALIB_ASSERT(vertexIndex >= 0);
			initialAtoms[v] = vertexIndex;
			finalAtoms[v] = _displacements.mapInitialToFinal(vertexIndex);
			if(finalAtoms[v] == -1)
				context().raiseErrorOne("Some atoms moved too far in the final configuration and have migrated to a different processor. Try increasing the ghost cutoff to avoid this error.");
			CALIB_ASSERT(structure1().atomOriginalTag(initialAtoms[v]) == structure2().atomOriginalTag(finalAtoms[v]));
		}

		// Calculate total deformation gradient from the displacements of atoms.
		Matrix3 tet_initial, tet_final;
		const Vector3& a1 = structure1().atomPosition(initialAtoms[3]);
		const Vector3& a2 = structure2().atomPosition(finalAtoms[3]);
		for(int i = 0; i < 3; i++) {
			const Vector3& b1 = structure1().atomPosition(initialAtoms[i]);
			const Vector3& b2 = structure2().atomPosition(finalAtoms[i]);
			CALIB_ASSERT(!structure1().isWrappedVector(b1 - a1));
			if(structure2().isWrappedVector(b2 - a2)) {
				context().raiseErrorOne("Initially close-by atoms %1 and %2 have moved apart very far in the deformed configuration. "
						"Their final distance exceeds half the periodic box size. "
						"Under these circumstances, we cannot perform the deformation field analysis, "
						"because the calculation of displacement vectors is ambiguous.", structure2().atomTag(finalAtoms[i]), structure2().atomTag(finalAtoms[3]));
			}
			tet_initial.col(i) = b1 - a1;
			tet_final.col(i) = b2 - a2;
		}
		*F++ = tet_final * tet_initial.inverse();
	}
}

/******************************************************************************
* Calculates the incremental elastic and plastic deformation tensor fields.
******************************************************************************/
void DeformationField::calculateElasticPlasticDeformation(const ElasticMapping& initialMapping, const ElasticMapping& finalMapping)
{
	CALIB_ASSERT(deformationGradients().size() == localCells().size());

	// Build a mapping between clusters in the initial configuration and clusters in the final configuration.
	mapClusters(initialMapping, finalMapping);

	// Allocate output arrays.
	_plasticDeformationGradients.resize(localCells().size());
	_slipDeformationGradients.resize(localCells().size());

	int ninvalid_no_initial_mapping = 0;
	int ninvalid_no_final_mapping = 0;
	int ninvalid_singular_initial = 0;
	int ninvalid_singular_final = 0;
	int ninvalid_no_transition = 0;

	vector<Matrix3>::iterator F_plastic = _plasticDeformationGradients.begin();
	vector<Matrix3>::iterator F_slip = _slipDeformationGradients.begin();
	vector<Matrix3>::const_iterator F = deformationGradients().begin();
	for(vector<DelaunayTessellation::CellHandle>::const_iterator cell = localCells().begin(); cell != localCells().end(); ++cell, ++F_plastic, ++F_slip, ++F) {

		F_plastic->setZero();
		F_slip->setZero();

		// Determine elastic deformation gradient in initial configuration.
		Matrix3 F_elastic_initial;
		Cluster* cluster_initial = initialMapping.computeElasticMapping(*cell, F_elastic_initial);
		if(!cluster_initial) {
			ninvalid_no_initial_mapping++;
			continue;	// Elastic mapping could not be determined.
		}
		if(fabs(F_elastic_initial.determinant()) <= CAFLOAT_EPSILON) {
			ninvalid_singular_initial++;
			continue;	// Elastic mapping is singular.
		}

		// Determine elastic deformation gradient in final configuration.
		Matrix3 F_elastic_final;
		Cluster* cluster_final = finalMapping.computeElasticMapping(*cell, F_elastic_final);
		if(!cluster_final) {
			ninvalid_no_final_mapping++;
			continue;	// Elastic mapping could not be determined.
		}
		if(fabs(F_elastic_final.determinant()) <= CAFLOAT_EPSILON) {
			ninvalid_singular_final++;
			continue;	// Elastic mapping is singular.
		}

		ClusterTransition* finalToInitialTransition = _bridgedClusterGraph.determineClusterTransition(cluster_final, cluster_initial);
		if(!finalToInitialTransition) {
			ninvalid_no_transition++;
			continue;	// Couldn't determine the crystallographic relation between lattice in initial and final configuration.
		}

		Matrix3 F_temp = finalToInitialTransition->tm * F_elastic_final.inverse() * (*F);

		// Calculate plastic slip deformation gradient.
		*F_slip = F_temp * F_elastic_initial;

		// Calculate incremental plastic deformation gradient.
		*F_plastic = F_elastic_initial * F_temp;
	}

	//context().msgLogger() << "ninvalid_no_initial_mapping = " << parallel().reduce_sum((AtomInteger)ninvalid_no_initial_mapping) << endl;
	//context().msgLogger() << "ninvalid_no_final_mapping = " << parallel().reduce_sum((AtomInteger)ninvalid_no_final_mapping) << endl;
	//context().msgLogger() << "ninvalid_singular_initial = " << parallel().reduce_sum((AtomInteger)ninvalid_singular_initial) << endl;
	//context().msgLogger() << "ninvalid_singular_final = " << parallel().reduce_sum((AtomInteger)ninvalid_singular_final) << endl;
	//context().msgLogger() << "ninvalid_no_transition = " << parallel().reduce_sum((AtomInteger)ninvalid_no_transition) << endl;
}

/// This structure is used to exchange mapping information between clusters in the initial and final configuration.
struct MappingComm
{
	ClusterId clusterInitial;		// The ID of the super cluster in the initial configuration.
	ClusterId clusterFinal;			// The ID of the super cluster in the final configuration.
	Matrix3 tm;						// The relative lattice rotation of the two clusters.
};

/******************************************************************************
* Builds a mapping between clusters in the initial configuration and clusters
* in the final configuration.
******************************************************************************/
void DeformationField::mapClusters(const ElasticMapping& initialMapping, const ElasticMapping& finalMapping)
{
	CALIB_ASSERT(_bridgedClusterGraph.clusterTransitions().empty());

	// Build list of inter-configuration cluster transitions.
	for(int atom_initial = 0; atom_initial < structure1().numLocalAtoms(); atom_initial++) {
		Cluster* initialCluster = initialMapping.patternAnalysis().atomCluster(atom_initial);
		if(initialCluster == NULL) continue;

		// Look up the corresponding atom in the final configuration.
		int atom_final = _displacements.mapInitialToFinal(atom_initial);
		if(atom_final == -1) continue;

		Matrix3 tm;
		if(determineClusterRotation(initialMapping, finalMapping, atom_initial, atom_final, tm)) {
			Cluster* finalCluster = finalMapping.patternAnalysis().atomCluster(atom_final);
			CALIB_ASSERT(finalCluster != NULL);

			// Create cluster transition.
			_bridgedClusterGraph.createClusterTransition(initialCluster, finalCluster, tm);
		}
	}

	const ClusterGraph& initialGraph = initialMapping.patternAnalysis().clusterGraph();
	const ClusterGraph& finalGraph   = finalMapping.patternAnalysis().clusterGraph();

	// Send all newly created cluster transitions to the master processor to check for
	// inconsistent transitions and to redistribute it back to all other processors.
	if(parallel().isMaster() == false) {
		vector<MappingComm> sendBuffer(_bridgedClusterGraph.clusterTransitions().size());
		vector<MappingComm>::iterator sendItem = sendBuffer.begin();
		BOOST_FOREACH(const ClusterTransition* t, _bridgedClusterGraph.clusterTransitions()) {
			sendItem->clusterInitial = t->cluster1->id;
			sendItem->clusterFinal = t->cluster2->id;
			sendItem->tm = t->tm;
			++sendItem;
		}
		parallel().sendWithSize(sendBuffer, 0);
	}
	else {
		// On the master processor, receive data packets from all other processors.
		for(int proc = 1; proc < parallel().processorCount(); proc++) {

			// Receive data.
			vector<MappingComm> receiveBuffer;
			parallel().receiveWithSize(receiveBuffer, proc);

			// Unpack data.
			BOOST_FOREACH(const MappingComm& receivedItem, receiveBuffer) {
				Cluster* cluster_initial = initialGraph.findCluster(receivedItem.clusterInitial);
				Cluster* cluster_final = finalGraph.findCluster(receivedItem.clusterFinal);
				CALIB_ASSERT(cluster_initial != NULL);
				CALIB_ASSERT(cluster_final != NULL);
				_bridgedClusterGraph.createClusterTransition(cluster_initial, cluster_final, receivedItem.tm);
			}
		}
	}

	// Broadcast final transition list back to other processors.
	vector<MappingComm> mappingBuffer;
	if(parallel().isMaster()) {
		mappingBuffer.resize(_bridgedClusterGraph.clusterTransitions().size());
		vector<MappingComm>::iterator sendItem = mappingBuffer.begin();
		BOOST_FOREACH(const ClusterTransition* t, _bridgedClusterGraph.clusterTransitions()) {
			sendItem->clusterInitial = t->cluster1->id;
			sendItem->clusterFinal = t->cluster2->id;
			sendItem->tm = t->tm;
			++sendItem;
		}
	}
	parallel().broadcastWithSize(mappingBuffer);
	if(parallel().isMaster() == false) {
		// Unpack data received from master processor.
		BOOST_FOREACH(const MappingComm& receivedItem, mappingBuffer) {
			Cluster* cluster_initial = initialGraph.findCluster(receivedItem.clusterInitial);
			Cluster* cluster_final = finalGraph.findCluster(receivedItem.clusterFinal);
			CALIB_ASSERT(cluster_initial != NULL);
			CALIB_ASSERT(cluster_final != NULL);
			_bridgedClusterGraph.createClusterTransition(cluster_initial, cluster_final, receivedItem.tm);
		}
	}
}

/******************************************************************************
* Determines the rigid body rotation of the crystal at an atom.
******************************************************************************/
bool DeformationField::determineClusterRotation(const ElasticMapping& initialMapping, const ElasticMapping& finalMapping, int atom_initial, int atom_final, Matrix3& tm) const
{
	CALIB_ASSERT(atom_initial != -1 && atom_final != -1);
	const SuperPatternAnalysis& initialPatternAnalysis = initialMapping.patternAnalysis();
	const SuperPatternAnalysis& finalPatternAnalysis = finalMapping.patternAnalysis();

	Cluster* cluster_final   = finalPatternAnalysis.atomCluster(atom_final);
	if(cluster_final == NULL) return false;

	Cluster* cluster_initial = initialPatternAnalysis.atomCluster(atom_initial);
	CALIB_ASSERT(cluster_initial != NULL);

	CALIB_ASSERT(cluster_initial->pattern != NULL);
	CALIB_ASSERT(cluster_final->pattern != NULL);
	const SuperPattern& superPattern_initial = *cluster_initial->pattern;
	const SuperPattern& superPattern_final = *cluster_final->pattern;

	CALIB_ASSERT(cluster_initial->atomCount != 0);
	CALIB_ASSERT(cluster_final->atomCount != 0);

	CALIB_ASSERT(initialPatternAnalysis.atomPatternNodeIndex(atom_initial) < superPattern_initial.numCoreNodes);
	CALIB_ASSERT(finalPatternAnalysis.atomPatternNodeIndex(atom_final) < superPattern_final.numCoreNodes);

	const SuperPatternNode& node_initial = initialPatternAnalysis.atomPatternNode(atom_initial);
	const SuperPatternNode& node_final = finalPatternAnalysis.atomPatternNode(atom_final);
	if(node_initial.coordinationPattern != node_final.coordinationPattern) return false;
	const CoordinationPattern* const coordPattern = node_initial.coordinationPattern;

	FrameTransformation ftm;
	for(int n = 0; n < coordPattern->numNeighbors(); n++) {

		int neighbor_index_initial = initialPatternAnalysis.nodeNeighborToAtomNeighbor(atom_initial, n);
		int neighbor_initial = initialPatternAnalysis.neighborList().neighbor(atom_initial, neighbor_index_initial);
		int neighbor_final = _displacements.mapInitialToFinal(neighbor_initial);
		if(neighbor_final == -1) return false;

		bool found = false;
		for(int n2 = 0; n2 < coordPattern->numNeighbors(); n2++) {
			int neighbor_index_final = finalPatternAnalysis.nodeNeighborToAtomNeighbor(atom_final, n2);
			if(finalPatternAnalysis.neighborList().neighbor(atom_final, neighbor_index_final) == neighbor_final) {
				const Vector3& initial_vector = node_initial.neighbors[n].referenceVector;
				const Vector3& final_vector = node_final.neighbors[n2].referenceVector;
				ftm.addVector(initial_vector, final_vector);
				found = true;
				break;
			}
		}
		if(!found) return false;
	}
	tm = ftm.computeTransformation();

	// It should be a pure rotation/reflection matrix.
	if(isRotationReflectionMatrix(tm, CA_TRANSITION_MATRIX_EPSILON) == false)
		return false;

	return true;
}

}; // End of namespace
