///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_GRAIN_IDENTIFICATION_H
#define __CA_GRAIN_IDENTIFICATION_H

#include "../CALib.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../deformation/ElasticAtomTensor.h"
#include "../pattern/superpattern/SuperPatternAnalysis.h"
#include "../context/CAContext.h"

namespace CALib {

/**
 * This class decomposes a polycrystal into individual grains.
 */
class GrainIdentification : public ContextReference
{
public:

	/**
	 * This class stores information about one grain of the polycrystal.
	 */
	struct Grain {

		/// Total number of atoms that belong to the grain.
		AtomInteger atomCount;

		/// Number of atoms that belong to the grain and which a local orientation tensor could be computed.
		AtomInteger latticeAtomCount;

		/// The (average) lattice orientation tensor of the grain.
		Matrix3 orientation;

		/// This is the cluster that is used to define the grain's lattice orientation.
		Cluster* cluster;

		/// The unique ID assigned to the grain.
		int id;

		/// This field is used by the disjoint-set forest algorithm using union-by-rank and path compression.
		size_t rank;

		/// Pointer to the parent element. This field is used by the disjoint-set algorithm.
		Grain* parent;

		/// Returns true if this is a root grain in the disjoint set structure.
		inline bool isRoot() const { return parent == this; }

		/// Initializes the fields of this structure.
		void init() {
			atomCount = 1;
			latticeAtomCount = 0;
			cluster = NULL;
			rank = 0;
			parent = this;
		}

		/// Merges another grain into this one.
		void join(Grain& grainB, const Matrix3& alignmentTM = Matrix3::Zero()) {
			grainB.parent = this;
			if(grainB.cluster != NULL) {
				CALIB_ASSERT(cluster != NULL);
				CAFloat weight = CAFloat(latticeAtomCount) / (latticeAtomCount + grainB.latticeAtomCount);
				orientation = weight * orientation + (1.0 - weight) * (grainB.orientation * alignmentTM);
			}
			atomCount += grainB.atomCount;
			latticeAtomCount += grainB.latticeAtomCount;
		}
	};

	/// Predicate object that determines whether it is an actual grain.
	struct IsActualGrain {
		bool operator()(const GrainIdentification::Grain& grain) const { return grain.id != 0; }
	};

public:

	/// Constructor,
	GrainIdentification(SuperPatternAnalysis& patternAnalysis);

	/// Sets The minimum misorientation angle between adjacent grains.
	void setMisorientationThreshold(CAFloat angle) { _misorientationThreshold = angle; }

	/// Controls the amount of noise allowed inside a grain.
	void setFluctuationTolerance(CAFloat tolerance) { _fluctuationTolerance = tolerance; }

	/// Sets the minimum number of crystalline atoms per grain.
	void setMinGrainAtomCount(AtomInteger count) { _minGrainAtomCount = count; }

	/// Returns the atomic structure.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns the object that holds the results of the atomic structure identification.
	SuperPatternAnalysis& patternAnalysis() { return _patternAnalysis; }

	/// Returns the atom neighbor list.
	const NeighborList& neighborList() const { return _patternAnalysis.neighborList(); }

	/// Builds a list of grains and assigns atoms to the grains.
	/// Returns the number of identified grains.
	AtomInteger findGrains();

	/// Returns the ID of the grain to which the given atom belongs.
	/// A return value of 0 indicates that the belong doesn't belong to any grain.
	int atomGrainId(int atomIndex) const {
		return const_cast<GrainIdentification*>(this)->parentGrain(atomIndex).id;
	}

	/// Returns the number of identified grains.
	AtomInteger grainCount() const { return _grainCount; }

	/// Returns an iterator interval that enumerates all identified grains.
	std::pair<
	boost::filter_iterator<IsActualGrain, std::vector<Grain>::const_iterator>,
	boost::filter_iterator<IsActualGrain, std::vector<Grain>::const_iterator> >
	grains() const {
		return make_pair(
				boost::filter_iterator<IsActualGrain, std::vector<Grain>::const_iterator>(_grains.begin(), _grains.end()),
				boost::filter_iterator<IsActualGrain, std::vector<Grain>::const_iterator>(_grains.end(), _grains.end())
				);
	}


private:

	/// Returns the parent grain of another grain.
	Grain& parentGrain(const Grain& grain) const {
		Grain* parent = grain.parent;
		while(!parent->isRoot()) parent = parent->parent;
		return *parent;
	}

	/// Returns the parent grain of an atom.
	Grain& parentGrain(int atomIndex) {
		Grain& parent = parentGrain(_grains[atomIndex]);
		// Perform path compression:
		_grains[atomIndex].parent = &parent;
		return parent;
	}

	/// Determines the reverse mapping from ghost atoms to local atoms (assuming serial mode).
	void generateGhostToLocalAtomMap();

	/// Maps an atom index to the corresponding local atom index in case it is a ghost atom.
	int mapToLocalAtomIndex(int atomIndex) const {
		// If atom is a ghost atom, map index back to range of local atoms.
		if(structure().isGhostAtom(atomIndex)) {
			CALIB_ASSERT(atomIndex >= (int)structure().numLocalAtoms());
			CALIB_ASSERT(atomIndex < (int)_ghostToLocalMap.size() + (int)structure().numLocalAtoms());
			return _ghostToLocalMap[atomIndex - structure().numLocalAtoms()];
		}
		return atomIndex;
	}

	/// Computes the local orientation tensor for all atoms whose structure has been identified.
	void computeAtomicOrientationTensors();

	/// Tests if two grain should be merged and merges them if deemed necessary.
	bool mergeTest(Grain& grainA, Grain& grainB, bool allowForFluctuations = true);

	/// Calculates the misorientation angle between two lattice orientations.
	CAFloat calculateMisorientation(const Grain& grainA, const Grain& grainB, Matrix3* alignmentTM = NULL);

	/// Computes the angle of rotation from a rotation matrix.
	static CAFloat angleFromMatrix(const Matrix3& tm);

	/// Assigns contiguous IDs to all parent grains.
	AtomInteger assignIdsToGrains();

	/// The atomic structure.
	const AtomicStructure& _structure;

	/// The results of the local structure identification.
	SuperPatternAnalysis& _patternAnalysis;

	/// Maps ghost atom indices to local atom indices.
	std::vector<int> _ghostToLocalMap;

	/// The working list of grains (contains one entry per atom).
	std::vector<Grain> _grains;

	/// The minimum misorientation angle between adjacent grains.
	CAFloat _misorientationThreshold;

	/// Controls the amount of noise allowed inside a grain.
	CAFloat _fluctuationTolerance;

	/// The minimum number of crystalline atoms per grain.
	AtomInteger _minGrainAtomCount;

	/// The final number of grains.
	AtomInteger _grainCount;
};

}; // End of namespace

#endif // __CA_GRAIN_IDENTIFICATION_H

