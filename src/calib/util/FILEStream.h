///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_STDIO_FILE_STREAM_H
#define __CRYSTAL_ANALYSIS_STDIO_FILE_STREAM_H

#include "../CALib.h"

namespace CALib {

/**
 *  Helper class that is used by the stdio_ostream class defined below.
 */
class stdio_out_syncbuf : public std::streambuf {
public:
	stdio_out_syncbuf() : std::streambuf(), fptr(NULL) {}
	void attach(FILE* f) { fptr = f; }
protected:
	virtual int overflow(int c = EOF) {
		if(c == EOF) return EOF;
		if(fptr) return fputc(c, fptr);
		return EOF;
	}
	virtual std::streamsize xsputn(const char* s, std::streamsize n) {
		if(fptr)
			return fwrite(s, 1, n, fptr);
		return 0;
	}
	virtual int underflow() {
		int c = getc(fptr);
		if(c != EOF)
			ungetc(c, fptr);
		return c;
	}
	virtual int uflow() { return getc(fptr); }
	virtual int pbackfail(int c = EOF) { return c != EOF ? ungetc(c, fptr) : EOF; }
	virtual int sync() { if(fptr) return fflush(fptr); return 0; }
private:
	FILE* fptr;
};

/**
 *  A STL stream class that sends its data to a FILE* handle.
 */
class stdio_ostream : public std::ostream {
public:
	stdio_ostream() : std::ostream(NULL) { init(&buf); }
	stdio_out_syncbuf* rdbuf() { return &buf; }
	void attach(FILE* fptr) { buf.attach(fptr); }
private:
	stdio_out_syncbuf buf;
};

/**
 *  Helper class that is used by the stdio_dual_ostream class defined below.
 */
class stdio_in_syncbuf : public std::streambuf {
public:
	stdio_in_syncbuf(FILE* f) : std::streambuf(), fptr(f) {}
	int get_fd() const {
		if(!fptr) return -1;
		return fileno(fptr);
	}
	void attach(FILE* f) {
		CALIB_ASSERT(fptr == NULL);
		fptr = f;
	}
protected:
	virtual int_type underflow() {
		if(gptr() < egptr()) return traits_type::to_int_type(*gptr());

		int putback_count = 0;
		// Copy the character in bump position (if any) to putback position.
		if(gptr() - eback()) {
			*putback_buffer = *(gptr() - 1);
			putback_count = 1;
		}

		// Now insert a character into the bump position.
		int result;
		do {
			result = std::fgetc(fptr);
		} while (result == EOF && errno == EINTR);

		if (result != EOF) *(putback_buffer + 1) = result;
		else {
			return traits_type::eof();
		}

		// Reset buffer pointers.
		setg(putback_buffer + (1 - putback_count),
				putback_buffer + 1,
				putback_buffer + 2);

		// Return character in bump/peek position.
		return result;
	}

	virtual std::streamsize xsgetn(char* dest, std::streamsize num) {
		std::streamsize chars_read = 0;

		// Available would normally be 0, but could be up to 2 if there
		// have been putbacks or a peek and a putback.
		std::streamsize available = egptr() - gptr();

		// If num is less than or equal to the characters already in the
		// putback buffer, extract from buffer.
		if(num <= available) {
			traits_type::copy(dest, gptr(), num);
			gbump(num);
			chars_read = num;
		}
		else {
			// First copy out putback buffer.
			if(available) {
				traits_type::copy(dest, gptr(), available);
				chars_read = available;
			}

			std::streamsize remaining = num - chars_read;
			std::streamsize result;
			do {
				result = std::fread(dest + chars_read, sizeof(char), remaining, fptr);
				chars_read += result;
				remaining -= result;
			}
			while(remaining && (result || errno == EINTR));

			if(remaining) {
				return chars_read;
			}

			if(chars_read) {
				// Now mimic extraction of all characters by sbumpc() by putting
				// two characters into the putback buffer (if available) and resetting the
				// putback buffer pointers.
				int putback_count;
				if(chars_read >= 2) {
					*putback_buffer = *(dest + (chars_read - 2));
					putback_count = 2;
				}
				else {  // If we have reached here then we have only fetched
						// one character and it must have been read with
						// fread() and not taken from the putback
						// buffer - otherwise we would have ended
						// at the first if block in this method
						// - and this also means that gptr() == egptr()
					if(gptr() - eback()) {
						*putback_buffer = *(gptr() - 1);
						putback_count = 2;
					}
					else putback_count = 1;
				}
				*(putback_buffer + 1) = *(dest + (chars_read - 1));

				// Reset putback buffer pointers.
				setg(putback_buffer + (2 - putback_count),
						putback_buffer + 2,
						putback_buffer + 2);
			}
		}
		return chars_read;
	}
private:
	FILE* fptr;

	// Putback_buffer does not do any buffering: it reserves one character
	// for putback and one character for a peek() and/or for bumping
	// with sbumpc/uflow() - buffering is done by the underlying C stream.
	char_type putback_buffer[2];
};

/**
 *  A STL stream class that reads its data from a classic C FILE* handle.
 */
class stdio_istream : public std::istream {
public:
	stdio_istream(FILE* f = NULL) : std::istream(NULL), buf(f) { init(&buf); }
	void attach(FILE* fptr) { buf.attach(fptr); }
	stdio_in_syncbuf* rdbuf() { return &buf; }
private:
	stdio_in_syncbuf buf;
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_STDIO_FILE_STREAM_H

