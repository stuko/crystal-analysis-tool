///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_SUPER_PATTERN_ANALYSIS_H
#define __CA_SUPER_PATTERN_ANALYSIS_H

#include "../../CALib.h"
#include "../../util/MemoryPool.h"
#include "../../context/CAContext.h"
#include "../coordinationpattern/CoordinationPatternAnalysis.h"
#include "../../cluster_graph/ClusterGraph.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "SuperPatternMatcher.h"

namespace CALib {

struct SuperPattern;
class PatternCatalog;

/**
 * Searches for occurrences of super patterns in an AtomicStructure and
 * decomposes the atomic structure into atomic clusters.
 */
class SuperPatternAnalysis : public ContextReference
{
public:

	/// Used to return global information about a pattern matching pass.
	struct AnalysisResultsRecord {

		/// The structure pattern.
		const SuperPattern* pattern;

		/// Number of atoms that match to the given pattern.
		AtomInteger numMatchingAtoms;

		/// The number of unit cells (= number of matching atoms / number of core nodes of the pattern).
		AtomInteger numMatchingUnits;

		/// Number of clusters found that contain disclinations.
		AtomInteger numDisclinations;
	};

public:

	/// Constructor. Takes a reference to the CoordinationPatternAnalysis object, which serves as basis for the super pattern analysis.
	SuperPatternAnalysis(const CoordinationPatternAnalysis& coordAnalysis) :
		ContextReference(coordAnalysis.context()), _coordAnalysis(coordAnalysis),
		_structure(coordAnalysis.structure()), _catalog(coordAnalysis.catalog()),
		_clusterGraph(coordAnalysis.structure(), coordAnalysis.catalog())
	{
		reset();
	}

	/// Returns a const-reference to the atomic structure in which patterns are searched.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns a reference to the coordination pattern analysis results.
	const CoordinationPatternAnalysis& coordAnalysis() const { return _coordAnalysis; }

	/// Searches the structure for the given super patterns.
	std::vector<SuperPatternAnalysis::AnalysisResultsRecord> searchSuperPatterns(const std::vector<SuperPattern const*>& patternsToSearchFor);

	/// Returns a reference to the cluster graph.
	ClusterGraph& clusterGraph() { return _clusterGraph; }

	/// Returns a const-reference to the cluster graph.
	const ClusterGraph& clusterGraph() const { return _clusterGraph; }

	/// Returns a const-reference to the neighbor list used for pattern matching.
	const NeighborList& neighborList() const { return _coordAnalysis.neighborList(); }

	/// After the pattern search phase, every processor has generated a local
	/// cluster graph that contains only the local cluster nodes. This function will
	/// establish graph edges between nodes from different processors by exchanging
	/// information about the cluster assignments of ghost atoms between neighboring
	/// processors.
	void connectDistributedClusterGraph();

	/// After the full distributed cluster graph has been generated, this will
	/// synchronize the assignment of clusters to atoms of all processors.
	/// That is, ghost atoms will become part of remote clusters.
	void synchronizeClusterAssignments();

	/// Determines whether the given atom (belonging to a cluster) is part of an
	/// anti-phase boundary.
	bool isAntiPhaseBoundaryAtom(int atomIndex) const;

	/// After the pattern analysis has been performed, returns the cluster an
	/// atom belongs to; or NULL if it does not belong to any cluster.
	Cluster* atomCluster(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_clusterAtoms.size());
		return _clusterAtoms[atomIndex].cluster;
	}

	/// After the pattern analysis has been performed, returns the super pattern
	/// an atom has been mapped to.
	const SuperPattern& atomPattern(int atomIndex) const {
		CALIB_ASSERT(atomCluster(atomIndex) != NULL);
		return _clusterAtoms[atomIndex].pattern();
	}

	/// After the pattern analysis has been performed, returns the index of the super pattern
	/// node an atom has been mapped to.
	int atomPatternNodeIndex(int atomIndex) const {
		CALIB_ASSERT(atomCluster(atomIndex) != NULL);
		return _clusterAtoms[atomIndex].nodeIndex;
	}

	/// After the pattern analysis has been performed, returns the super pattern
	/// node an atom has been mapped to.
	const SuperPatternNode& atomPatternNode(int atomIndex) const {
		CALIB_ASSERT(atomCluster(atomIndex) != NULL);
		return _clusterAtoms[atomIndex].patternNode();
	}

	/// After the pattern analysis has been performed, returns the coordination pattern
	/// assigned to the given atom.
	/// Requirement: The atom must belong to a cluster.
	const CoordinationPattern& atomCoordinationPattern(int atomIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_clusterAtoms.size());
		CALIB_ASSERT(_clusterAtoms[atomIndex].coordination != NULL);
		return *_clusterAtoms[atomIndex].coordination->pattern;
	}

	/// Given a neighbor bond of the super pattern node assigned to an atom, returns the
	/// corresponding entry in the atom's neighbor list.
	/// Requirement: The atom must belong to a cluster.
	int nodeNeighborToAtomNeighbor(int atomIndex, int nodeNeighborIndex) const {
		CALIB_ASSERT(atomIndex >= 0 && atomIndex < (int)_clusterAtoms.size());
		return nodeNeighborToAtomNeighbor(_clusterAtoms[atomIndex], nodeNeighborIndex);
	}

protected:

	/// Holds information about an atom that is part of a cluster.
	struct ClusterAtom {

		/// The cluster the atom is part of (or NULL if not part of a cluster).
		Cluster* cluster;

		/// The super pattern node to which the atom has been mapped.
		int nodeIndex;

		/// The index of the coordination pattern symmetry permutation that maps
		/// neighbor indices of the super pattern node to neighbors in the coordination pattern.
		int permutationIndex;

		/// The coordination pattern match record of the input atom.
		CoordinationPatternAnalysis::coordmatch const* coordination;

		/// Returns the super pattern of the cluster this atom is part of.
		const SuperPattern& pattern() const {
			CALIB_ASSERT(cluster != NULL && cluster->pattern != NULL);
			return *cluster->pattern;
		}

		/// Returns the super pattern node to which this cluster atom has been mapped.
		const SuperPatternNode& patternNode() const {
			CALIB_ASSERT(cluster != NULL && cluster->pattern != NULL);
			CALIB_ASSERT(nodeIndex >= 0 && nodeIndex < cluster->pattern->numCoreNodes);
			return cluster->pattern->nodes[nodeIndex];
		}
	};

	/// Searches the structure for a single super pattern.
	AnalysisResultsRecord searchSuperPattern(const SuperPattern& pattern);

	/// Resets the analysis results.
	void reset() {
		_clusterAtoms.resize(coordAnalysis().structure().numLocalAtoms() + coordAnalysis().structure().numGhostAtoms());
		ClusterAtom initStruct = { NULL, -1, -1, NULL };
		boost::fill(_clusterAtoms, initStruct);
	}

	/// Given a neighbor bond of the super pattern node assigned to an atom, returns the
	/// corresponding entry in the atom's neighbor list.
	/// Requirement: The atom must belong to a cluster.
	int nodeNeighborToAtomNeighbor(const ClusterAtom& atom, int nodeNeighborIndex) const {
		CALIB_ASSERT(atom.cluster != NULL);
		CALIB_ASSERT(atom.coordination != NULL);
		CALIB_ASSERT(nodeNeighborIndex >= 0 && nodeNeighborIndex < atom.coordination->pattern->numNeighbors());
		CALIB_ASSERT(atom.permutationIndex >= 0 && atom.permutationIndex < (int)atom.coordination->pattern->permutations.size());
		int coordPatternNeighborIndex = atom.coordination->pattern->permutations[atom.permutationIndex][nodeNeighborIndex];
		return atom.coordination->mapping[coordPatternNeighborIndex];
	}

	/// Given an atom where two clusters overlap, computes the crystallographic
	/// orientation relationship between the two.
	Matrix3 computeTransformationFromOverlapAtom(const SuperPatternMatcher::MatchedAtom& atom, const SuperPatternNode& node, const ClusterAtom& otherAtom);

	/// The results of the coordination pattern analysis.
	const CoordinationPatternAnalysis& _coordAnalysis;

	/// The atomic structure in which patterns are searched.
	const AtomicStructure& _structure;

	/// Catalog of patterns.
	const PatternCatalog& _catalog;

	/// The graph of clusters generated from the atomic structure as a result of the pattern search.
	ClusterGraph _clusterGraph;

	/// Stores the assigned cluster for each atom of the input structure.
	std::vector<ClusterAtom> _clusterAtoms;
};

}; // End of namespace

#endif // __CA_SUPER_PATTERN_ANALYSIS_H
